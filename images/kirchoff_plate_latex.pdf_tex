%% Creator: Inkscape inkscape 0.91, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'kirchoff_plate_latex.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{380.25600586bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.66529584)%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{kirchoff_plate_latex.pdf}}%
    \put(0.25854532,0.58386882){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$Z$}}}%
    \put(0.93104532,0.02867161){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$X$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=2]{kirchoff_plate_latex.pdf}}%
    \put(0.65231133,0.28065976){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$w$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=3]{kirchoff_plate_latex.pdf}}%
    \put(0.41043921,0.43293639){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$u$}}}}}%
    \put(0.30689373,0.25811058){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$A$}}}}}%
    \put(0.71060811,0.63833037){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$a$}}}}}%
    \put(0.3097703,0.18587563){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$P$}}}}}%
    \put(0.29507619,0.0045544){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$B$}}}}}%
    \put(0.70522529,0.55943436){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$p$}}}}}%
    \put(0.81202949,0.40852567){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$b$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=4]{kirchoff_plate_latex.pdf}}%
    \put(0.41818382,0.57210854){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$s_X$}}}%
    \put(0.16708885,0.12651341){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$Y$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=5]{kirchoff_plate_latex.pdf}}%
    \put(0.30764511,0.11149343){\color[rgb]{1,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$Q$}}}}}%
    \put(0.78176379,0.49527733){\color[rgb]{1,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$q$}}}}}%
    \put(0.80262003,0.3296579){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$\varphi$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=6]{kirchoff_plate_latex.pdf}}%
    \put(0.66950041,0.48817156){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$Z$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=7]{kirchoff_plate_latex.pdf}}%
    \put(0.80427529,0.17743809){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textit{$Z\sin\varphi$}}}}%
    \put(0.44976816,0.44454019){\color[rgb]{1,0,0}\makebox(0,0)[lt]{\begin{minipage}{0.16530213\unitlength}\raggedright reference\\ plane\end{minipage}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=8]{kirchoff_plate_latex.pdf}}%
    \put(0.07128101,0.32596153){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$s_Z$}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=9]{kirchoff_plate_latex.pdf}}%
    \put(0.93926221,0.49096486){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{\textit{$\varphi$}}}}}%
  \end{picture}%
\endgroup%
