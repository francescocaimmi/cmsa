"""
Genrates the data needed to plot the stress and stain distribution in the
CLT worked out example
"""
plot_flag = True
import numpy as np
A = np.array([[1739 , 388.4 , 56.63],
              [388.4 ,  453.3 , -114.1],
              [56.63 , -114.1 , 452.5]])

B = np.array([ [-3129 , 985.5 , -1072],
                [-985.5 , 1158 , -1072],
                [-1072, -1072 , 985.5]])
D = np. array([[33430 , 6461 , -5240],
               [6461 , 9320 , -5596],
               [-5240 , -5596 , 7663]])
#layer stiffnedd matrces converted to MPa
#converted to MPa
Q_0 = 1e3 * np.array([[181.8 , 2.897 , 0],
           [ 2.897 , 10.35 , 0],
            [0 , 0 , 7.17]])

Q_30 = 1e3 * np.array([[109.4 , 32.46, 54.19],
           [ 34.46 , 23.65, 20.05],
            [54.19 , 20.05 , 36.74]])

Q_45 = 1e3 * np.array([[56.66 , 43.32, -42.87],
           [ 43.32, 56.66, -42.87],
            [-42.87, -42.87, 46.59]])

ABD = np.empty((6,6))

ABD[:3,:3]=A
ABD[:3,3:]=B
ABD[3:,:3]=B
ABD[3:,3:]=D

gen_der = np.array([3.123e-7,3.492e-6,-7.598e-7,2.971e-8,-3.285e-7, 4.101e-7])
eta = gen_der[:3]
chi = gen_der[3:]
N = np.array([1,1,0,0,0,0])

Z = np.linspace(-7.5,-2.5,20)

for point in [[-2.5,2.5],[2.5,7.5]]:
    data = np.linspace(point[0],point[1],20)
    Z=np.append(Z, data)

sigma = np.zeros((len(Z),3))
epsilon = np.zeros((len(Z),3))

#calc the deformations
for z in xrange(len(Z)):
    epsilon[z] = eta+Z[z]*chi
    if z<=19:
        sigma[z] = np.dot(Q_0,epsilon[z])
    elif z<=39:
        sigma[z] = np.dot(Q_30,epsilon[z])
    elif z<=len(Z):
        sigma[z] = np.dot(Q_45,epsilon[z])



np.savetxt('clt_epsilon.txt',np.column_stack((Z,epsilon)), delimiter = ',',
           header = 'Z [mm], \epsilon_x,\epsilon_y,\gamma_{xy}')
np.savetxt('clt_sigma.txt',np.column_stack((Z,sigma)), delimiter = ',',
           header = 'Z [mm], \sigma_x,[MPa],\sigma_y [MPa],\tau_{xy} ,[MPa]')

if plot_flag:
    import matplotlib.pylab as plt

    plt.plot(epsilon[:,0],Z)
    plt.plot(epsilon[:,1],Z)
    plt.plot(epsilon[:,2],Z)
    plt.ticklabel_format(style='sci', scilimits=(-2,2))
    plt.show()
    plt.plot(sigma[:,0],Z)
    plt.plot(sigma[:,1],Z)
    plt.plot(sigma[:,2],Z)
    plt.ticklabel_format(style='sci', scilimits=(-2,2))
    plt.show()
