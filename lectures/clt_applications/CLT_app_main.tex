%kate: default-dictionary en_GB;
\usepackage{eulervm} %
\usepackage{fontspec}
\usepackage{polyglossia}
\setmainlanguage{english}
%math
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{bm}
\usepackage{xfrac}
\usepackage{empheq}


%beamer options 
\usepackage{beamerthemebars}
\usetheme{CambridgeUS}
\setbeamertemplate{footline}[page number]{}

%graphics
\usepackage{xcolor}
\usepackage{graphicx}
\graphicspath{ {../../images/} }
\usepackage{tikz}
\usetikzlibrary{positioning,3d,intersections}
\usetikzlibrary{arrows,shapes,backgrounds}
\pgfdeclarelayer{low}  % declare background layer
\pgfdeclarelayer{up}   %declare upper layer
\pgfsetlayers{low,main,up}  % set the order of the layers (main is the 
                        %standard layer)
% For every picture that defines or uses external nodes, you'll have to
% apply the 'remember picture' style. To avoid some typing, we'll apply
% the style to all pictures.
\tikzstyle{every picture}+=[remember picture]
% By default all math in TikZ nodes are set in inline mode. Change this to
% displaystyle so that we don't get small fractions.
\tikzset{execute at begin node={\everymath{\displaystyle}}} 
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{minibox}
%captions 
\usepackage[compatibility=false]{caption}
\captionsetup{font=scriptsize}

%bibliography
\mode<presentation>{
\usepackage[hyperref=true,style=alphabetic, url=false]{biblatex}
}
\mode<article>{
\usepackage[hyperref=true,style=numeric, url=false]{biblatex}
}
\addbibresource{../../corso_compositi.bib}


\makeatletter
\tikzoption{canvas is xy plane at z}[]{%
\def\tikz@plane@origin{\pgfpointxyz{0}{0}{#1}}%
\def\tikz@plane@x{\pgfpointxyz{1}{0}{#1}}%
\def\tikz@plane@y{\pgfpointxyz{0}{1}{#1}}%
\tikz@canvas@is@plane
}
\makeatother  


%Preamble
\author{Francesco Caimmi}
\date{}
\title{Composite Materials For Structural Applications}
\subtitle{Classical Lamination Theory - Applications and Design}
\institute{Politecnico di Milano - CMIC Department}
\begin{document}

\mode<presentation>{
\begin{frame}[plain]
    \titlepage
\end{frame}}

\mode<article>{
\begin{enumerate}
 \item nothing
}

\mode<presentation>{
\begin{frame}[plain]
    \tableofcontents
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
  \AtBeginSection[]
    {
    \begin{frame}<beamer>
    \tableofcontents[currentsection]
    \end{frame}
    }
}

\section{Composite Beams}

\mode<presentation>{
\begin{frame}
\frametitle{From plates to beams}   
\begin{columns}[onlytextwidth,T]
    \begin{column}{0.5\textwidth}
    Consider laminates with a rectangular cross section.\\
    Assume that~\cite{reddy2003mechanics}:
    \begin{itemize}
        \item $b$ is small w.r.t to the length along $X$
        \item the stacking sequence is such that the displacements do not depend
        on $Y$ 
        \item the loads depend only on $X$ 
        \item $M_Y=M_{XY}=0$
    \end{itemize}
    $\Rightarrow$ the laminate can be treated as if it were a beam:\\
    $\bm{s}=\bm{s}(X)$ and $\bm{U}=\bm{U}(X)$
    \end{column}
    \begin{column}{0.5\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.8,y={(-1cm,0)},x={(1cm,1cm)}, z={(0cm,1cm)}]
         \input{../../images/composite_beam.tex}   
        \end{tikzpicture}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item When the width $b$ (length along the y-axis) of a laminated plate is very 
small compared to the length along the x-axis and the lamination scheme, and 
loading is such that the displacements are functions of x only, the 
laminate is treated as a beam~\cite{reddy2003mechanics}.

In general, the maximum stress does not occur at the top or bottom of a 
laminated beam. The maximum stress location through the beam thickness depends 
on the lamination scheme~\cite{reddy2003mechanics}. 
}

\mode<presentation>{
\begin{frame}
\frametitle{Limits to the hypotheses}
The previous hypotheses can be satisfied only in an approximate way: from 
    \[
    \bm{\chi}=\left\lbrace \chi_X,\chi_Y,\chi_{XY} \right\rbrace =  
    \left\lbrace \frac{\partial^2 w}{\partial X^{\,2}}, 
    \frac{\partial^2 w}{\partial Y^{\,2}},
    2 \frac{\partial^2 w}{\partial X \partial Y}\right\rbrace,
    \]
the assumptions and 
\[
    \bm{M}=\bm{D}.\bm{\chi}
\]

$\Rightarrow$

\[
\frac{\partial^2 w}{\partial X\,^2} = D_{11}^* M_X , \frac{\partial^2 
w}{\partial Y\,^2} = D_{12}^* M_X, \frac{\partial^2 w}{\partial X \partial Y}= 
D_{16}^* M_X
\]  
where $\bm{D}^*=\bm{D}^{-1}$. Due to Poisson ($D_{12}^*$) and shear coupling 
coefficients ($D_{16}^*$) it cannot be assumed that $w\equiv w(X)$.\\
Only for long laminates it can be assumed that $w\equiv w(X)$: how long?\\
Depends on the width $b$ and the \textcolor{red}{material constants}.
\end{frame}

}

\mode<article>{
\item It can be shown that the effect of Poisson ratio and shear coupling can 
be neglected only for very long laminates~\cite{reddy2003mechanics}.
Only for long laminates it can be assumed that $w \equiv w(X)$: how long?
Depends on the material constants, but in general one must expect that in order 
to assume that the beam approximation holds the length-to-width ratio should be 
much larger than for isotropic cases.
}

\mode<presentation>{
\begin{frame}
\frametitle{Bending equations of motion}
\begin{columns}[onlytextwidth,T]
    \begin{column}{0.55\textwidth}
        Neglecting membrane loads, define:
        \begin{align*}
           &M = b M_{XX}, V = b V_X, E_{XX}=\frac{12}{h^3 D_{11}^*},\\
           &I_{YY}=\frac{b h^3}{12}, \hat{q}= b q 
        \end{align*}
    \end{column}
     \begin{column}{0.45\textwidth}
        \begin{figure}
            \centering
            \def\svgwidth{0.9\textwidth}
            \input{../../images/beam_bending.pdf_tex}
            \caption*{A beam model}
            \label{afig:clt_forces}
            \end{figure}
      \end{column}
\end{columns}
    and recover the Euler-Bernoulli equation for beam bending
    \[
        E_{XX}\,I_{YY}\frac{\mathrm{d}^4 w}{\mathrm{d}X\,^4}=\hat{q} \text{ or }
        \frac{\mathrm{d}^2 w}{\mathrm{d}X\,^2} = \frac{M}{I_{YY} \, E_{XX}}
    \]
    Under the previous hypotheses, one can use the \textcolor{red}{usual 
    solution techniques}.
    The effect of the material and of the \textcolor{red}{lamination sequence 
    effect is completely condensed in a single constant}, the bending 
    stiffness $E_{XX}$.
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{More on beams}
\begin{block}{Stress}
    As for standard laminates using 
    $\bm{\sigma_k}(X,Y,Z)=\bm{Q^{'}_k}.\bm{\epsilon}$ and 
    $\bm{M}=\left\lbrace M/b,0,0\right\rbrace$ :
    \[
       \bm{\sigma_k}=\frac{Z}{b} \bm{Q^{'}_k}.\bm{D^*} .\bm{M} =
       \frac{M Z}{b} 
        \begin{bmatrix}
            Q'^k_{11} D^*_{11}+Q'^k_{12} D^*_{12}+Q'^k_{16} D^*_{16}\\
            Q'^k_{12} D^*_{11}+Q'^k_{22} D^*_{12}+Q'^k_{26} D^*_{16}\\
            Q'^k_{16} D^*_{11}+Q'^k_{26} D^*_{12}+Q'^k_{66} D^*_{16}\\
        \end{bmatrix}
       \text{ for each layer }k
    \]
    Maximum stress: does not occur at the cross section top or bottom. Location 
    depends on stacking. 0° layers (w.r.t. $X$) take the most axial 
    stress.
\end{block}
\begin{columns}[onlytextwidth,T]
 \begin{column}{0.6\textwidth}
    \begin{block}{Thin walled sections}
        Beams with thin walled sections can be treated by assembling the wall 
        constitutive equations  in a global reference system to calculate the 
        section parameters (e.g shear rigidity)~\cite{barbero2011introduction}.
    \end{block}
 \end{column}
 \begin{column}{0.3\textwidth}
    \begin{figure}
     \centering   
    \begin{tikzpicture}[scale=0.45]
        \centering
        \scriptsize
        \input{../../images/walls.tex}
    \end{tikzpicture}
    \end{figure}
 \end{column} 
\end{columns}

\end{frame}

}

\mode<article>{
\item As to the stresses in composite beams they are evaluated exactly in the 
same way as with plates.For each layer one uses the constitutive equation. Under 
the assumption that the only acting force is the bending moment so that there 
are no normal deformation, the strain is given only by the curvature directed 
perpendicular to the beam axis, so that for each layer the stresses are given 
by this expression.
In general, the maximum stress does not occur at the top or bottom of a 
laminated beam. The maximum stress location through the beam thickness depends 
on the lamination scheme. The 0° layers (w.r.t.$X$) take the most axial 
stress~\cite{reddy2003mechanics}.
A rather common case of beams employed in engineering structures is that of 
beams with a thin walled cross section, that are mainly subject to torsion and  
bending moments and to shear and axial forces~\cite{barbero2011introduction}.
These beams are usually made with several walls, as in the case of the I shaped 
cross section in the figure, which is made with three different walls, two 
flanges and a web.
Each wall is usually a complex laminate, optimised to maximise the performance 
of the beam. So webs will probably contain $\pm45^\circ$ cross-ply layers to 
resist shear, and flanges $0^\circ$ layers to resist bending.
The middle surface of the cross section is represented by a network of nodes 
and segments, along which some curvilinear reference system ($s$ and $r$).
Using this technique one can at first do the integration through the thickness 
($r$) to calculate the wall properties, and then perform the usual integration 
along direction $s$ for each wall in order to establish the properties of the 
whole section, such as shear rigidity. The interested reader is referred to the 
book by Barbero~\cite{barbero2011introduction}.
}

\section{Sandwich plates}
\input{sandwich.tex}

\section{Optimal composite structures}
\input{optimal.tex}

% \section{Failure of a pressure vessels}
% \input{cylinder.tex}
% \section*{References}

\mode<presentation>{
\begin{frame}[allowframebreaks]
\frametitle{References}
\printbibliography
\end{frame}
}

\mode<article>{
\end{enumerate}
\printbibliography

}

\end{document}




