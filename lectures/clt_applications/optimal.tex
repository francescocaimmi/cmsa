%kate: default-dictionary en_GB;
\subsection{The general case}
\mode<presentation>{
\begin{frame}
\frametitle{Remarks on  the design problem for composite structures}   
Given 
\begin{itemize}
    \item a \textcolor{red}{design objective $f$}  (e.g. minimum cost or
        weight, maximum performance index)
    \item a set of \textcolor{blue}{design constraints $\bm{g}$} (e.g. resist 
        to given loads, do not deform more than a given limit, do not cost 
        more than a given amount)
\end{itemize}

for a composite structure, solving a design problem means to 
\textcolor{violet}{find the lamination sequence} 
$\textcolor{violet}{\bm{x}}=\left\lbrace{L,\bm{m},\bm{\theta},\bm{h}}
\right\rbrace$ , i.e. 
\begin{itemize}
    \item number of layers $L$
    \item sequence of materials $\bm{m}$
    \item sequence of angles $\bm{\theta}$
    \item sequence of layer thicknesses $\bm{h}$
\end{itemize}
varying in an admissible space $\mathcal{A}$ such that
\begin{empheq}[box=\fbox]{alignat*=2}
    %line 1
    \textcolor{violet}{\bm{x}}=
    \underset{\textcolor{violet}{\bm{x}} \in \mathcal{A}}
    {\operatorname{arg\,min}} 
    \quad &      \textcolor{red}{f}\,(\textcolor{violet}{\bm{x}}) \\
    %line 2
    \text{s.t.} \quad &  
    \textcolor{blue}{\bm{g}}\,(\textcolor{violet}{\bm{x}})\leq \bm{0}
\end{empheq}
\end{frame}
}

\mode<article>{
\item The admissible space defines bounds for the design variables, for 
instance the angles can vary only between $0^\circ$ and $90^\circ.$
Here the constraints are expressed as inequalities; they can be equalities as 
well.
}

\mode<presentation>{
\begin{frame}
\frametitle{Optimal composite structures}
\begin{empheq}[box=\fbox]{alignat*=2}
    %line 1
    \bm{x}=\underset{\bm{x}\in\mathcal{A}}{\operatorname{arg\,min}} \quad &  
    f\,(\bm{x}) \\
    %line 2
    \text{s.t.} \quad &  \bm{g}\,(\bm{x})\leq \bm{0}
\end{empheq}
Broadly speaking such a problem is not necessarily convex (can have multiple, 
possibly equivalent, solutions), cannot be solved in closed form and may 
require very complex solution techniques (see e.g.~\cite{Ghiasi2009}).\\
However under some assumptions, approximated \textcolor{red}{analytical} 
results can be derived. These can be very useful to guide the initial design 
choices.
\end{frame}
}

\mode<article>{
\item nothing
}

\subsection{Optimal fibrous structures}

\mode<presentation>{
\begin{frame}
\frametitle{Optimal fibrous structures~\cite{Vasilev2001}}
\begin{columns}[T,onlytextwidth]
    \begin{column}{0.5\textwidth}
        A flat laminate is subject to membrane loads $\bm{N}$. Assumption: 
        made with a \emph{monotropic} material, i.e. only fibres can 
        carry loads
        $\Rightarrow$ PMRS stress state:
        $\bm{\sigma}_{1,2}= \left\lbrace \sigma_1,0,0 \right\rbrace$.\\
        \textcolor{red}{Design goal: $\min h, h = \sum\limits_{i=1}^{L} h_i$}\\
        \textcolor{blue}{Design constraints}:
        \begin{itemize}
            \item resistance to the loads: for each layer $i$, 
                $\sigma_1^i-c\leq0$
            \item equilibrium
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}[ht]
            \centering
            \includegraphics[width=\textwidth]{../images/optimal_model.png}
            % optimal_model.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
            \caption*{A laminate under membrane loads.}
            \label{fig:optimal_fibrous}
        \end{figure}
    \end{column}
\end{columns} 
Equilibrium requires (3 equations)
\begin{equation}
\label{eq:equilibrium}
 \begin{bmatrix}
    N_X\\N_Y\\N_{XY}
 \end{bmatrix}
 =
 \sum\limits_{i=1}^{L} h_i
  \begin{bmatrix}
    \sigma_X \\ \sigma_Y\\ \tau_{XY}
 \end{bmatrix}
 \Leftrightarrow
 \begin{bmatrix}
    N_X\\N_Y\\N_{XY}
 \end{bmatrix}
-
\sum\limits_{i=1}^{L} h_i \sigma_1^i
  \begin{bmatrix}
    \cos^2 \phi_i \\ \sin^2 \phi_i \\ \sin \phi_i \cos \phi_i   
 \end{bmatrix} = \bm{0}
\end{equation}
\end{frame}
}

\mode<article>{
\item As an example~\cite{Vasilev2001} we shall examine a simple case, trying 
to find out the optimal fibrous structure for a laminate to withstand some given 
loads.
To make things simple we shall assume that there are only membrane loads 
acting, and that our laminate is made of a monotropic material, which means 
that only the fibers can carry the load. This is a very simple model but it is 
reasonable when designing for strength, as the transverse and shear resistance 
of laminae are much lower than the one in the fiber direction. It can also 
applicable once there has been degradation of the matrix in a layer, as you 
might recall from the last lesson.
Using this model requires that for every load there must be layers with fibres 
oriented along the loading direction, otherwise the laminate would not be able 
to withstand the loads.
This kind of analysis is also called ``netting analysis''~\cite{Hull1996}.

This assumption implies that in the material principal reference system
the only non vanishing stress component is the one along the fibres $\sigma_1$.
Assume also all the laminae are made of the same material.
Let our design goal be the reduction of the thicknesses of the laminate to the 
minimum value, which as the material is fixed gives also the minimum weight and 
the minimum cost.

As to the constraints we shall use two constraints. The first is that the 
material must not break. This means that for every layer the 
stress along the fibres must be less than a critical value, say 
$c$, which represents the material resistance times a proper safety factor: 
$\sigma_1^i<c$. We could also think about applying a safety factor only at the 
end of the procedure.

The other constraint we shall apply is requiring that equiblibrium be 
satisfied, as for monotropic materials this is not to be taken for granted.
For monotropic materials equilibrium requires that the sum of the stresses in 
each laminate be equal to the applied loads; using the fact that there is only 
one component of stress in the material principal reference system.
 
\begin{equation}
 \begin{bmatrix}
    N_X\\N_Y\\N_{XY}
 \end{bmatrix}
 =
 \sum\limits_{i=1}^{L} h_i
  \begin{bmatrix}
    \sigma_X \\ \sigma_Y\\ \tau_{XY}
 \end{bmatrix}
 \Leftrightarrow
 \begin{bmatrix}
    N_X\\N_Y\\N_{XY}
 \end{bmatrix}
-
\sum\limits_{i=1}^{L} h_i \sigma_1^i
  \begin{bmatrix}
    \cos^2 \phi_i \\ \sin^2 \phi_i \\ \sin \phi_i \cos \phi_i   
 \end{bmatrix} = \bm{0}
\end{equation}

}

\mode<presentation>{
\begin{frame}[plain]
The constitutive equation for a monotropic material is obtained simply as
\begin{equation}
    \sigma_1^i= E_1 \epsilon_1 = E_1 (\epsilon_x \cos^2 \phi_i+ \epsilon_y 
    \sin^2 \phi_i+ \gamma_{xy} \sin \phi_i \cos \phi_i)
\end{equation}
yielding $L$ resistance constraints (one for each layer $i$)
\begin{equation}
    \label{eq:resistance_constraint}
    c - E_1 (\epsilon_x \cos^2 \phi_i+ \epsilon_y 
    \sin^2 \phi_i+ \gamma_{xy} \sin \phi_i \cos \phi_i) = 0
\end{equation}
Using the lagrangian multipliers method define the 
\textcolor{red}{objective function $f$} as:
\begin{align}
\textcolor{red}{ f\,(h_i,\phi_i) }=& \sum\limits_{i=1}^{L} h_i + \lambda_x 
    \left( N_x - 
    \sum\limits_{i=1}^{L} h_i \sigma_1^i \cos^2 \phi_i \right) +
     \lambda_y \left( N_y - 
    \sum\limits_{i=1}^{L} h_i \sigma_1^i \sin^2 \phi_i \right) \nonumber\\
    & + \lambda_{xy} \left( N_{xy} - 
    \sum\limits_{i=1}^{L} h_i \sigma_1^i \sin \phi_i \cos \phi_i \right) +
    \nonumber \\
    & + \sum\limits_{i=1}^{L} \lambda_i \left[  c - E_1 (\epsilon_x \cos^2 
    \phi_i+ \epsilon_y \sin^2 \phi_i+ \gamma_{xy} \sin \phi_i \cos \phi_i) 
    \right]
\end{align}
which depends on the \textcolor{red}{$2L$ design variables $h_i$ and 
$\phi_i$}, on 3 
lagrangian multipliers $\lambda_{(x,y,xy)}$ (equilibrium constraints) and 
on $L$ lagrangian multipliers $\lambda_i$ (resistance constraints).

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}[plain]
The derivatives of $f$ with respect to the design variables, $\partial f/ 
\partial h_i$ and $\partial f/ \partial \phi_i$ yield the following $2L$ 
equations (one for each $i$)

\begin{equation}
 \label{eq:d1}
 \sigma_1^i\left(\lambda_x \cos^2 \phi_i+ \lambda_y \sin^2 \phi_i+
 \lambda_{xy} \sin \phi_i \cos \phi_i\right) = 1 
\end{equation}
\begin{equation}
 \label{eq:d2}
 h_i \sigma_i \left[ (\lambda_y-\lambda_x) \sin 2 \phi_i+\lambda_{xy} \cos 2
 \phi_i \right] = E_1 \lambda_i \left[ (\epsilon_y-\epsilon_x) \sin 2 
 \phi_i +\gamma_{xy} \cos 2 \phi_i \right] 
\end{equation}
The solutions to eqs.\ref{eq:d2} (compare each trig. function coefficient) are
\begin{equation}
    \lambda_x =E_1 \epsilon_x \frac{\lambda_i}{h_i \sigma_1^i},
    \lambda_y =E_1 \epsilon_y \frac{\lambda_i}{h_i \sigma_1^i},
    \lambda_{xy} =E_1 \gamma_{xy} \frac{\lambda_i}{h_i \sigma_1^i},
\end{equation}
as they must be satisfied for each $i\Rightarrow$
\begin{equation}
 \frac{\lambda_i}{h_i \sigma_1^i}=\frac{\lambda_x}{E_1 \epsilon_x} =
    \frac{\lambda_y}{E_1 \epsilon_y}=\frac{\lambda_{xy}}{E_1 \gamma_{xy}} = 
        \frac{1}{d\,^2}
\end{equation}
where $d$ is some constant.
Using the resistance constraint, eq~\ref{eq:resistance_constraint}, and 
eqs~\ref{eq:d1} one finds out that $d=c=\sigma_1^k$.\\
Obviously the laminate with the minimum thickness is the one for which the 
stress in each layer is the admissible stress.

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}[plain]
Only equilibrium equations are left:
using the first two (eq.\ref{eq:equilibrium} for $N_x$ 
and $N_y$) and $h = \sum\limits_{i=1}^{L} h_i$ it follows that the optimal 
laminate total thickness satisfies
\begin{equation}
\label{eq:opt_h}
 h = \frac{1}{c}(N_x+N_y)   
\end{equation}
\textcolor{red}{for all possible optimal laminates}.\\
Use eq~\ref{eq:opt_h}, $h = \sum\limits_{i=1}^{L} h_i$ and the equilibrium 
equations to eventually find that
\begin{equation}
 \label{eq:design1}
 \sum\limits_{i=1}^L h_i\left( N_x \sin^2 \phi_i -N_y \cos^2 \phi_i \right)=0
\end{equation}
and
\begin{equation}
 \label{eq:design2}
 \sum\limits_{i=1}^L h_i\left[ (N_x+N_y) \sin \phi_i \cos \phi_i -N_{xy} 
    \right]=0
\end{equation}
Thus there are only 3 equations (eqs. \ref{eq:opt_h}, 
\ref{eq:design1} and \ref{eq:design2}) for $2L$ design variables: generally
speaking there are infinite optimal composites structures for given problem.\\
\begin{scriptsize}Disclaimer: approximate failure criterion and monotropic 
model $\Rightarrow$ approximate results!\end{scriptsize}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Some examples}

\smallskip 
\begin{columns}[onlytextwidth,T]
    \begin{column}{0.5\textwidth}
        Optimal design equations
        {\footnotesize
        \begin{align*}
        \text{(\ref{eq:opt_h})} &h = \frac{1}{c}(N_x+N_y)\\
        %line 2
        \text{(\ref{eq:design1})} &\sum\limits_{i=1}^L h_i\left( N_x 
        \sin^2 \phi_i + \right. \\
        %line 4
        & \left. -N_y \cos^2 \phi_i\right)=0\\
        %line 4
        \text{(\ref{eq:design2})} & \sum\limits_{i=1}^L h_i\left[ 
        (N_x+N_y) \sin \phi_i \cos \phi_i+\right. \\
        %line 5        
        &\left. -N_{xy}\right]=0\\
        \end{align*}
        \begin{center}
         \begin{tikzpicture}
            \begin{axis}[xmin=-90,xmax=90, xlabel = {$\phi$ [deg]}, 
                        ylabel near ticks,
                        ylabel= $\sin \phi\cos \phi$,
                        width = \textwidth, height= 0.55\textwidth,
                        xtick = {-90,-60,...,90}]
                \addplot[domain=-90:90, red,no markers, smooth,thick] 
                {sin(x)*cos(x)};
           
            \draw[ultra thin] (axis cs:\pgfkeysvalueof{/pgfplots/xmin},0) -- 
            (axis cs:\pgfkeysvalueof{/pgfplots/xmax},0);
            \draw[ultra thin] (axis cs:0,\pgfkeysvalueof{/pgfplots/ymin}) -- 
            (axis cs:0,\pgfkeysvalueof{/pgfplots/ymax});
            \end{axis}
         \end{tikzpicture}
        \end{center}
        }
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{block}{Uniaxial tension}
            $N_y=N_{xy}=0$ obviously then
            \begin{itemize}
                \item[$\Rightarrow$] $\phi_i=0\,\forall \,i$ (from 
                eq.\ref{eq:design1})
                \item[$\Rightarrow$] $h=N_x/c$ (from eq.\ref{eq:opt_h})
            \end{itemize}
        \end{block}
        \begin{block}{Tension in two orthogonal directions}
            $N_{xy}=0$ 
            \begin{itemize}
                \item [$\Rightarrow$] $0^\circ$ or $90^\circ$ layers or 
                balanced laminate (from eq.\ref{eq:design2}))
            \end{itemize}
            If $N_x=N_y=N$ eq.\ref{eq:design1} becomes
            \[
             \sum\limits_{i=1}^L h_i \cos 2\phi_i = 0
            \]
        \end{block}
    \end{column}
\end{columns}
\end{frame}

}

\mode<article>{
\item In the uniaxial case the second design equation is automatically 
satisfied.

$N_{xy}=0\Rightarrow$ the laminate should be balanced  ($\forall 
\phi_i$ there must be a layer oriented at $-\phi_i$, from eq.\ref{eq:design2}).
In addition it can contain any layer oriented at $0^\circ$ or at $90^\circ$, 
which give zero contribution to the sum.
}

\mode<presentation>{
\begin{frame}
\frametitle{Some examples 2}
\begin{columns}[onlytextwidth,T]
    \begin{column}{0.5\textwidth}
        Optimal design equations
        {\footnotesize
        \begin{align*}
        \text{(\ref{eq:opt_h})} &h = \frac{1}{c}(N_x+N_y)\\
        %line 2
        \text{(\ref{eq:design1})} &\sum\limits_{i=1}^L h_i\left( N_x 
        \sin^2 \phi_i + \right.\\
        %line 4
        & \left. -N_y \cos^2 \phi_i\right)=0\\
        %line 4
        \text{(\ref{eq:design2})} & \sum\limits_{i=1}^L h_i\left[ 
        (N_x+N_y) \sin \phi_i \cos \phi_i+\right.\\
        %line 5        
        &\left. -N_{xy}\right]=0\\
        \end{align*}
        \begin{center}
         \begin{tikzpicture}
            \begin{axis}[xmin=-90,xmax=90, xlabel = {$\phi$ [deg]}, 
                        ylabel= $\cos 2 \phi$,ylabel near ticks,
                        width = \textwidth, height= 0.55\textwidth,
                        xtick = {-90,-45,...,90}]
                \addplot[domain=-90:90, red,no markers, smooth,thick] 
                {cos(2*x)};
           
            \draw[ultra thin] (axis cs:\pgfkeysvalueof{/pgfplots/xmin},0) -- 
            (axis cs:\pgfkeysvalueof{/pgfplots/xmax},0);
            \draw[ultra thin] (axis cs:0,\pgfkeysvalueof{/pgfplots/ymin}) -- 
            (axis cs:0,\pgfkeysvalueof{/pgfplots/ymax});
            \end{axis}
         \end{tikzpicture}
        \end{center}
        }
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{block}{Tension in two orthogonal directions (continued)}
            \[
             \sum\limits_{i=1}^L h_i \cos 2\phi_i = 0
            \]
            \begin{itemize}
                \item [$\Rightarrow$] cross-ply laminate is a possible solution
                (from eq.\ref{eq:design1})); other common options are 
                quasi-isotropic laminates and $\pm45^\circ$ laminates
                \item [$\Rightarrow$] $h = 2N/c$ (twice a homogeneous plate)
            \end{itemize}
        \end{block}
    As a rule of thumb, \textcolor{red}{fibres along the direction of principal 
    strains are} (approximately) \textcolor{red}{optimal}.
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
If $N_x=N_y=N$ one possible optimal structure is a cross-ply laminate (from 
eq.\ref{eq:design1}), with optimal thickness $h = 2 N/c$.
We can think about generalising this results and the ne for uniaxial case: it 
is rather intuitive that placing fibres along the directions of principal 
strains should allow to exploit their resistance to the maximum, thus satisfying 
the optimal conditions we defined before~\cite{Vasilev2001}. 
However you must recall that such a result is valid only for monotropic 
materials and the simple strength criterion we used before. For more 
complicated criteria it can be shown that the optimal angle differs from the 
one of principal strains by quantities that depend on the elastic and failure 
properties in transverse and shear directions, as it is rather 
intuitive~\cite{sandhu1971,Christensen2005}.
Consider this result, from which it follows that the total thickness of 
the optimal laminate is twice as high as the thickness of a homogeneous plate 
under the same loading conditions. 
This result is quite natural because, in contrast to isotropic
materials, the monotropic layer can work only in one direction - along the 
fibers~\cite{Vasilev2001}.
So, we need to have the 0$^\circ$-layer to take $N_x$ and the same, but 
90$^\circ$-layer to take $N_y$. 
From this we can conclude that the directional character of a composite ply
stiffness and strength is actually the material shortcoming rather than its 
advantage.
Real advantages of composite materials are associated with their high specific
strength provided by thin fibers, and if we had isotropic materials
with such specific strength, the use of composites would be limited to very 
special cases.
Other possible solutions to the problem are for example quasi-isotropic 
laminates or angle-ply laminates oriented at $\pm45^\circ$.
}

\subsection{The case of pressure vessels}
\mode<presentation>{
\begin{frame}
\frametitle{Pressure vessel example}
\begin{columns}[onlytextwidth,T]
    \begin{column}{0.55\textwidth}
            Consider a very thin cylinder with closed ends loaded by pressure 
            $p$ only.\\
            Load symmetry requires 
            $\chi_{xy}=\chi_x=0$~\cite{Vasilev2001,Christensen2005}.\\
            The only curvature change would be linked to the change in 
            diameter
            $\chi_y:=\frac{1}{R_f}-\frac{1}{R}\cong -\frac{\eta_y}{R}$:
            negligible for small $\eta_y$.\\
            Internal actions:
             \[ N_x=\frac{1}{2} p R, N_y= p R,  N_{xy}=0 \]

            $\Rightarrow$ must be balanced or contain $0^\circ$ and $90^\circ$ 
            layers.\\
            Design equations become:
            \[
                h = \frac{3 p R}{2 c} \quad, 
                \sum\limits_{i=1}^L h_i \left( 3 \cos^2 \phi_i -1 \right) = 0
            \]
    \end{column}
    \begin{column}{0.45\textwidth}
        \begin{figure}[ht]
            \centering
            \begin{tikzpicture}[scale = 0.8,y={(0.25cm,-0.1cm)},x={(1cm,0cm)}, 
        z={(0cm,1cm)}]
             \input{../../images/mycylinder.tex}   
            \end{tikzpicture}
            \caption*{Scheme of a pressure vessel}
        \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item Now consider a pressure vessels with closed ends, assuming it can be 
approximated by a cylinder. The angles will be measured wit respect to 
the cylinder parallels.
We did not deal with curved laminates up to now, 
but this case is rather simple if the cylinder curvature is very small and the 
only load is pressure, we can neglect the effect of 
curvature and use the equations for flat laminates~\cite{Christensen2005}.
In fact pressure load, for symmetry imposes no curvature changes along the 
direction called $x$ and also no torsion.
The only curvature change would be linked to the change in diameter:
$\chi_y:=\frac{1}{R_f}-\frac{1}{R}\cong -\frac{\eta_y}{R}$ which we can assume 
negligible for small $\eta_y$.
If we can neglect the curvature, the internal actions are easy to determine.
In this case the orthoradial component is the through the thickness component 
which we always assume to be negligible in CLT. 
The internal actions are then given these expression, which are just Mariotte's 
forumulae integrated through the thickness of the cylindrical shell, as I am 
sure most of you have recognized.
As $N_{xy}=0$  due to what we said before the laminate must be balanced or 
contain only layers oriented at $0^\circ$ or $90^\circ$.
The design equations reduce to these ones. Note again that the thickness 
required is 1.5 times the one that would be needed for a homogeneous steel 
plate with strength $c$. Anyway, composite vessels are still interesting for 
they are very light.

The design equations tell us that there are infinite fibrous structures that
can be used to design a pressure vessel. We shall now see some example
}

\mode<presentation>{
\begin{frame}
\frametitle{Some optimal pressure vessels}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
     \begin{block}{$L=2,[0/90]$}
        $\phi_1=0$ and $\phi_2=90 \Rightarrow h_2 = 2 h_1$ \\
    \end{block}
    \begin{block}{$L=2,[\pm\phi]$}
        Assuming $h_{\phi}=h_{-\phi}\Rightarrow \cos^2 \phi = 1/3 
        \Leftrightarrow \phi^* \simeq 55^\circ$ 
     \end{block}
     \begin{block}{$L=3, [\pm \phi,?]$}
        Assuming $h_1=h_{\phi}=h_{-\phi}=h_2\Rightarrow$ 
        \[
            \frac{h_3}{h_{\phi}} =
            \begin{cases}
            \textcolor{red}{\frac{h_{90}}{h_{\phi}}} = \frac{1}{2} 
            (3 \cos^2 \phi -1) & \text{if } \phi < \phi^* \\
            \textcolor{blue}{\frac{h_{0}}{h_{\phi}}} =  (1 - 3 
            \cos^2 \phi) & \text{if } \phi > \phi^*
            \end{cases}
        \]
     \end{block}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{tikzpicture}
         \input{../../images/optimal_vessel_ratio.tex}   
        \end{tikzpicture}
    \end{column}
\end{columns}

\end{frame}
}

\mode<article>{
\item Let's consider a case with only two layers. Assume they are oriented 
along the 
circumferential and parallel directions only.
As $\phi_1=0$ and $\phi_2=0$ it follows that $ h_2 = 2 h_1$  i.e.The layer at 
$90^\circ$ must be thick twice the layer at $0^\circ$
This result seems obvious because the normal load along $y$ is twice 
the axial one, $N_y/N_x= 2$.
Unfortunately layers at $0^\circ$ cannot be laid using filament winding which 
is the most common production technique for 
Let's now consider an angle ply laminate. We can assume that the thickness of 
the two layer is the same, so that we find that the best angle would be about 
$55^\circ$, which is a most common angle for filament wound pressure vessels.

As a rule, helical plies are combined with circumferential plies to fully 
exploit fibre resistance in the circumferential direction.
In this case assuming $L=3$ and assuming again that angled layers have the same 
thickness.
Assume now that we want to add a layer at $90^\circ$ if we carry out the 
calculations using equation~\ref{eq:design1} we get that the ratio between the 
height of the third layer and the height of an angle layer must be given by the 
this expression. 
So for each angle $\phi$ we can chose the proper ply thickness using this 
relationship, which is plotted here. 
Of course as the angle of the fibres in the angle layer increases, so that they 
tend to become circumferential themselves the needed thickness of layers at 
$90^\circ$ decreases. 

As the thickness cannot be negative, this equation is valid only for 
$\phi<\phi_0$. For larger angles the fibres can  withstand well the 
circumferential load but may have problems with the axial load. 
In that case we should add not a $90^\circ$ but a $0^\circ$ layer, whose 
thickness is given by this expression, the blue line in the graph.
In this case, as the fibers in the angle layer rotate and tend to become 
circumferential, we must increase the thickness of the $0$ layer to bear the 
axial loads.

}

\subsection{Optimal sandwich beams for stiffness}
\mode<presentation>{
\begin{frame}
\frametitle{A sandwich beam in three point bending}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
        Assume isotropic facings and core, with moduli $E_f$ and $E_c$.
        The equivalent bending stiffness from CLT is
        \begin{align*}
            E_{XX} I_{YY} &= \frac{E_c b c^{\,3}}{12}+\frac{E_f b t d^{\,2}}{2} 
                +\frac{E_f b t^{\,3}}{6}\\
               & \simeq \frac{E_f b t c^{\,2}}{2},
        \end{align*}
        where $d$ is the distance between facing centroids, while
        the equivalent shear rigidity is
        \begin{align*}
            G_{XZ} A_{X} &= b A_{55} = \frac{b d^{\,2} G_c}{c}\\
               & \simeq b c G_c.
        \end{align*}
    \end{column}
    \begin{column}{0.5\textwidth}
        \def\svgwidth{\textwidth}
        \input{../../images/sandwich_beam_3pb.pdf_tex}
    \end{column}    
\end{columns}
\end{frame}
}

\mode<article>{
\item $A_{X}$ is the cross sectional area normal to $X$ 
}

\mode<presentation>{
\begin{frame}
\frametitle{}
The compliance of the beam is given by 
\begin{equation}\label{eq:compliance}
    \frac{\delta}{P} = \frac{l^{\,3}}{48 E_{XX} I_{YY}} +
                       \frac{l}{4 G_{XZ} A_{X}}.
\end{equation}
For most foams the shear modulus depends on density as~\cite{gibson1999cellular}
\begin{equation}\label{eq:densitydep}
    G_c = K E_s \left(\frac{\rho_c}{\rho_s}\right)^2.
\end{equation}


\end{frame}
}

\mode<article>{
\item the dependence of the shear modulus on the foam density allows us to 
design the material, here the foam, to reach the best sandwich properties. 
The formula we are using is valid as a first approximation for open cell 
foams~\cite{gibson1999cellular}.
}

\mode<presentation>{
\begin{frame}
\frametitle{Sandwich beams of minimum weight}
\begin{block}{Problem~\cite{gibson1999cellular}}
For a beam of given stiffness, find the foam density $\rho_c$, the foam 
thickness $c$ and the face thickness $t$ that provide the minimum beam 
weight.
\end{block}
The weight of the beam $W$ is
\begin{equation}\label{eq:sandweigth}
    W = W_f + W_c = 2 \rho_f t b l g + \rho_c b l c g.
\end{equation}
The stiffness constraint can be handled directly by inserting the shear modulus 
dependence on $\rho_c$ (eq.\ref{eq:densitydep}) in the compliance expression 
(eq.\ref{eq:compliance}) and solving for $\rho_c$:
\[
    \rho_c = \rho_s \sqrt{\frac{ E_{f} \, c l t}{K E_s}
    \frac{1}{4 (\delta/P) E_f b c^{2} t - l^{\,3}}}.
\]
This is the foam density needed to achieve the desired stiffness $P/\delta$ 
(for given $t,c$).
\end{frame}
}
    
\mode<article>{
\item these parameters of course affect each other and the design objective and 
constraints: for example the foam density affects the foam modulus, and thus 
helps reaching the desired stiffness but increasing the weight; the foam 
thickness affects mainly the stiffness in a quadratic way and the weight in a 
linear one. So the effects are intertwined and not immediately analysable.
However this problem is simple enough to be tackled analytically.
}

\mode<presentation>{
\begin{frame}
\frametitle{}
using this expression in (Eq.\ref{eq:sandweigth}) for $W$ the minimum weight 
can be found by: 
\[
    \nabla W(c,t) = \left\lbrace \frac{\partial W}{\partial c}, 
                    \frac{\partial W}{\partial t} \right\rbrace = \bm{0}.
\]
The optimal parameters for given stiffness are:
\begin{align*}
    \frac{t}{l} &= \sqrt[5]{
                    \frac{9}{16} \frac{1}{K^{\,2} E_s^{\,2} E_f}
                    \left( \frac{P}{\delta b}\right)^3
                    \left( \frac{\rho_s}{\rho_f} \right)^4
                    }\\
    \frac{c}{l} &= \sqrt[5]{
                    216 \frac{K E_s}{E_f^{\,2} }
                    \left( \frac{P}{\delta b}\right)
                    \left( \frac{\rho_s}{\rho_f} \right)^2
                    }\\                
    \frac{\rho_c}{\rho_s} &= \sqrt[5]{
                    \frac{9}{2} \frac{E_f}{E_s^{\,3} K^{\,3}}
                    \left( \frac{P}{\delta b}\right)^2
                    \left( \frac{\rho_s}{\rho_f} \right)
                    }
\end{align*}

\end{frame}
}

\mode<article>{
\item these formualae may find application for instance in the preliminary 
design of a skis
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{itemize}
    \item Irrespective of the materials, prescribed stiffness and span the 
        optimal weights are such that
        \[
            W_f/W_c= 1/4.
        \]
    \item This optimisation does not take strength into account.
\end{itemize}

\end{frame}
}