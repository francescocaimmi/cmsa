%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Thin shells with constant curvatures}
\begin{block}{Requirement}
    \centering
    $\frac{h}{R_i} < \frac{1}{20}$ (thin) and 
    $\partial a_{i}/ \partial \xi_i=0$ (constant curvature)
\end{block}

For thin shells it is possible to \emph{neglect the effect of initial 
curvature}, i.e. to assume that 
\[
    1+\frac{\zeta}{R_i} \simeq 1
\]
which implies
\begin{block}{Stress resultants}
    \[
        N_{12} = N_{21}, M_{12} = M_{21}, 
    \]
\end{block}
\begin{block}{Strain field}
    \[
    \epsilon_1 = \eta_1+\zeta \chi_1, 
    \epsilon_2 = \eta_2 +\zeta \chi_2,
    \epsilon_6 =  \eta_{12} + \zeta \chi_{12}
    \]
\end{block}
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Constitutive equations}
Reduce to 
\[
    \begin{bmatrix}
    \bm{N}\\ \bm{M}
    \end{bmatrix} 
    =
    \left[
    \begin{array}{c|c}
        \bm{A} & \bm{B}\\
        \hline
        \bm{B} & \bm{D}
     \end{array}
     \right].
    \begin{bmatrix}
    \bm{\eta}\\ \bm{\chi}
    \end{bmatrix},
\]
where:
\begin{align*}
    A_{ij} &=  \sum\limits^L_{k=1} \, \left\lbrace Q^{'}_k \right\rbrace_{ij} 
                (\zeta_{k+1}-\zeta_k) \qquad i,j = 1 \dots 6,\\
    B_{ij} &= \frac{1}{2} \sum\limits^L_{k=1} \left\lbrace 
                    Q^{'}_k\right\rbrace_{ij} 
                    (\zeta^{\,2}_{k+1}-\zeta^{\,2}_k) \qquad i,j = 1,2,6, \\
    D_{ij} &=  \frac{1}{3} \sum\limits^L_{k=1} \, \left\lbrace 
                Q^{'}_k \right\rbrace_{ij} 
                (\zeta^{\,3}_{k+1}-\zeta^{\,3}_k) \qquad i,j = 1,2,6;
\end{align*}
these matrices are symmetric under the previous assumptions.
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Equilibrium equations}
Letting $z_1 = \xi_1,z_2 = \xi_2, z_3 = \zeta, \, \partial z_i=a_i \partial 
\xi_i,\, N_{12}=N_{21}=N_{6}$ and 
$M_{12}=M_{21}=M_{6}$ the equilibrium equations in terms of stress resultants 
can be written as~\cite{reddy2003mechanics}
\[
\frac{\partial N_1}{\partial z_1} +
    \frac{\partial }{\partial z_2} (N_2+C_0 M_6)+\frac{V_1}{R_1}=0, \,
\frac{\partial N_2}{\partial z_2} +
    \frac{\partial }{\partial z_1} (N_1+C_0 M_6) +\frac{V_2}{R_2}=0
\]
\[
%shear equation
\frac{\partial V_1}{\partial z_1} +
\frac{\partial V_2}{\partial z_2} -
\left( \frac{N_1}{R_1}+\frac{N_2}{R_2} \right)+q=0, \,
%moments equation
\frac{\partial^2 M_1}{\partial z_1^{\,2}} +
\frac{\partial^2 M_{6}}{\partial z_1 \partial z_2}+
\frac{\partial^2 M_2}{\partial z_2^{\,2}} -
\left( \frac{N_1}{R_1}+\frac{N_2}{R_2} \right)+q=0
\]

with $C_0 = \frac{1}{2} \left(\frac{1}{R_1} - \frac{1}{R_2} \right)$
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Thin cylindrical shells under axially symmetric loads}
\begin{columns}
    \begin{column}{0.65\textwidth}
        Axial symmetry implies that $\partial / \partial \theta=0$, so that the 
        problem becomes one-dimensional.
        Assuming that the only external load is pressure $p$,
        the equilibrium equations become~\cite{Vinson1993}:
        \begin{align*}
        &\frac{\textsf{d}N_x}{\textsf{d}x} = 0, \quad
            \frac{\textsf{d}V_x}{\textsf{d}x} - \frac{N_{\theta}}{R} + p =0,\\
        &\frac{\textsf{d}M_x}{\textsf{d}x}-V_x=0
        \end{align*}
        
    \end{column}
    \begin{column}{0.35\textwidth}
        \begin{center}
        \includegraphics[width=\textwidth,keepaspectratio=true]
        {./shells_slides-figure7.pdf}
        \end{center}
    \end{column}
\end{columns}
\end{frame}
 
}

\mode<article>{
\item the first equilibrium equation is equilibrium along the cylinder axis; 
the second equilibrium equation is equilibrium along the through the thickness 
direction. Of course equilibrium along the $\theta$ direction is automatically 
satisfied due to symmetry. 
In the second equation the term $\frac{N_{\theta}}{R}$ is linked to the 
projection of $N_1$ along the through the thickness direction.
The third equation is the moment equilibrium along the directions perpendicular 
to the shell surface.
}