# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 10:20:33 2015

@author: fcaimmi

Gives the data needed to plot surfaces in the plot
"""
from __future__ import print_function
import numpy as np
from numpy import cos, sin, tan

def r(x):
    """
    Sphere equation
    """
    return np.array([cos(x[0])*sin(x[1]),sin(x[0])*sin(x[1]),cos(x[1])])

def rt(x):
    """
    dr/dt
    """
    return np.array([-sin(x[0])*sin(x[1]),cos(x[0])*sin(x[1]),0])

def rp(x):
    """
    dr/dp
    """
    return np.array([cos(x[0])*cos(x[1]),sin(x[0])*cos(x[1]),-sin(x[1])])

def s(t):
    """
    Witch of agnesi equation
    """
    return np.array([2*a/np.tan(t),2*a*sin(t)**2,0])

def s_prime(t):
    """
    Derivative of s
    """
    return np.array([2*a*(-tan(t)**2 - 1)/tan(t)**2,
                     4*a*sin(t)*cos(t),
                             0])
def s_second(t):
    """
    Second derivative of s
    """

    return np.array([4*a*cos(t)/sin(t)**3,
                     4*a*cos(2*t),
                    0])
def N(t):
    """
    normal vector to s
    """
    sp = s_prime(t)
    return np.array([-sp[1],sp[0],0])/np.linalg.norm(sp)

def k(t):
    """
    Curvature of s
    """

    sp = s_prime(t)
    ss = s_second(t)
    return (sp[0]*ss[1]-ss[0]*sp[1])/(sp[0]**2+sp[1]**2)**(3.0/2.0)

def Q(t):
    """
    Center of the osculating circle
    """

    return s(t)+N(t)/k(t)

#point on the sphere to draw tangen vectors
p = np.array([4*2*np.pi/20,14*np.pi/40])
print("Data for point p", p)
print("Corrinates of r(p)",r(p))
tt=rt(p)/np.linalg.norm(rt(p))
print("Tangent t vector in p",tt)
print("Affine vector to plot 0.5*tt+r(p)",0.5*tt+r(p))
tp=rp(p)/np.linalg.norm(rp(p))
print("Tangent t vector in p",tp)
print("Affine vector to plot 0.5*tt+r(p)",0.5*tp+r(p))
cp=np.cross(tt,tp)
n=-cp/np.linalg.norm(cp)
print("normal vector in p",n)
print("Affine vector to plot 1.5*n",1.5*n)
#equation of the tangent plane
print("z=",r(p)[2],"+",cp[0]/cp[2],"(",r(p)[0],"-x)","+",cp[1]/cp[2],"(",r(p)[1],"-y)")
#curves
print(40*'-')
print("The witch of Agnesi")
a=4
t = 0.85*np.pi/2
print("Point corresponding to t =", t, " is ", s(t))
print("Center of the osculating circle", Q(t))
print("Radius of curvature=",1/k(t))
#first quadratic form
print(40*'-')
print("points to draw on the line thorugh p and (0,0) in T")
dp= np.array([5*2*np.pi/20,7*5*2*np.pi/20/8])
print('Coordinates dr=',r(dp))