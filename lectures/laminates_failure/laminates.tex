%kate: default-dictionary en_GB;

\mode<presentation>{
\begin{frame}
    \frametitle{Introduction}
    From laminae properties, Laminate Failure Analysis aims at finding criteria 
to predict: 
    \begin{columns}
        \begin{column}{0.3\textwidth}
            \begin{figure}[ht]
              \centering
              \includegraphics[width=\textwidth]{../images/laminate_failure.jpg}
              % laminate_failure.jpg: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
              \label{fig:cracking}
          \end{figure}
        \end{column}

        \begin{column}{0.7\textwidth}
            \begin{itemize}
                \item \textcolor{red}{failure initiation loads}: 
                  \begin{itemize}
                    \item The load causing first failure somewhere in a 
                        laminate (First Ply Failure).
                    \item Often is the load for which the first 
                        \textcolor{red}{matrix cracks} appear. 
                    \item Rarely coincident with laminate collapse load, as 
                        fibers in the loading direction can still bear loads.
                  \end{itemize}
                \item \textcolor{red}{final failure loads}: 
                  \begin{itemize}
                    \item The load at laminate collapse and/or functionality 
                        loss.
                    \item It may be important to estimates failure kinematics.
                    \item Its evaluation may involve the study of the sequence 
                        of failure events, with consequent stiffness 
                        degradation.
                    \item It is often the first fiber failure load.
                  \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}
}

\mode<article>{
\item As we did for stiffness, we now try to set up a method that allow us to 
predict the behavior of laminates, failure behavior in this case, starting from 
the properties of a single lamina. As we shall see most unfortunately this does 
not work as good as in the stiffness case!

  Failure behavior of laminates, as we already discussed, involves evaluation of 
two loads: the failure initiation load and the final failure load.

  Initiation load is the load causing first failure somewhere in a laminate; as 
we saw in-plane stresses in laminates are discontinuous so that, generally 
speaking, a critical stress conditions will be reached, as the load is 
increased, only for a certain ply. This is termed First Ply Failure.
  
  Often this is the load for which the first matrix cracks appear in plies 
oriented transversely with respect to the main loads, as the transverse strength 
is very low and even if they carry a small portion of the loads~\cite{Hull1996}.
  
  Normally this is not the load which causes the final failure of the laminate. 
Let's say this clearly: you design your laminate in order to withstand the loads 
avoiding first ply failure. But at any rate you have still some resource. 
  First ply failure load can be coincident with laminate failure if there are no 
fibers oriented approximately in the direction of the load causing failure. For 
instance in highly angled angle ply laminates, if the matrix fails there is 
nothing to bear the load any longer and the laminates suddenly 
collapses~\cite{barbero2011introduction}. This is another reason why often 
laminates are made with combination of $0^\circ,90^\circ$ and $\pm45^\circ$ 
laminae, although of course this is not a rule~\cite{soden2004recommendations}!


  Laminate failure load instead is the load which causes catastrophic failure of 
our laminate or the loss of functionality, for example the leakage load for a 
pressure vessel. While design should avoid first ply failure, being able to 
estimate final failure  is interesting because can tell us which exceptional 
loads the laminate can withstand, and can be used to estimate the kinematics of 
failure: how the laminate collapses. This is of course important in many 
respects.

  The evaluation of the failure load can involve a series of subsequent 
analysis since when a lamina fails there is a load redistribution in the other 
laminae and a reduction of the stiffness of the damaged lamina.
  There are various models to estimate the stiffness degradation. Most of the 
load has to be transferred to other laminae. 
  In turn other laminae may fail if we increase the load and so on until we find 
the load which causes the collapse of the laminate as whole, which often turns 
out to be the load for which the fibers fails first in whatever 
lamina~\cite{barbero2011introduction}. 
  So we see that it is important to have failure criteria which tell us which 
constituent of the composite fails for a given load.
  
  How do we evaluate these loads? Let's start with First Ply Failure 
}


\subsection{First Ply Failure}

\mode<presentation>{
\begin{frame}
    \frametitle{First Ply Failure (FPF)}
    \begin{columns}
        \begin{column}{0.35\textwidth}
            \begin{figure}[ht]
                \centering
                
                \includegraphics[width=\textwidth,keepaspectratio=true]
                {../images/stress_thickness_dist.png}
                % stress_thickness_dist.png: 0x0 pixel, 300dpi, 0.00x0.00 
                \caption*{Piece-wise linear stress distribution across the 
                thickness of a laminate.}
                \label{fig:stress_dist}
            \end{figure}
        \end{column}

        \begin{column}{0.65\textwidth}
             \begin{itemize}
                \item design technique to estimate where failure will first take 
                    place in a laminate.
                \item uses a \textcolor{red}{single ply criteria} to do to the 
                    prediction.
                \item accuracy: the probability to get a 
                    \textcolor{red}{$\pm50$\textdiscount{}} accuracy is about 
                    80\textdiscount{}; for better accuracy (below 
                    \textcolor{red}{$\pm10$\textdiscount{}}) the probability 
                    is less than 
                    20\textdiscount{} 
                    \cite{Hinton15012013,hinton2002comparison}.
                    Over-and \mbox{under-estimation} are common.
            \end{itemize}
            Some reasons why:
            \begin{itemize}
                \item ply strengths in laminates can be different from 
                    those obtained from tests on single plies/UD.
                \item thermal stresses.
                \item the stress state may be actually 3D: no influence on 
                    stiffness but there can be some on failure.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}
}

\mode<article>{
  \item The estimation of first ply failure is usually performed in a very 
    simple way. You just calculate the stresses and the strains in your 
    laminate using the Classical Lamination Theory, pick up a criterion among 
    those we mentioned before and use it to predict failure in every ply. 
  As you remember the distribution of stresses along the thickness of a laminate 
is piece-wise linear, so for each lamina the maximum or minimum stress component 
is found at the top or bottom of each lamina. 
  You check both for failure as tension and compression strengths are different.
  
  Does it work?

  It does not even get close.
  More or less you have an 80\textdiscount{} probability to find an initial 
failure load which over\mbox{-} or under\mbox{-}estimates the first ply failure 
load with an accuracy of about 50\textdiscount{}. In the remaining 
20\textdiscount{} of cases it can be even worse~\cite{hinton2002comparison}. For 
better accuracy, like within 10\textdiscount{} you have less than a 
20\textdiscount{} chance.

  The main problem is that often one gets estimates which are not conservative. 
Which criteria to use? Tsai-Wu and the maximum stress criterion seem to perform 
reasonably well.
  If you are very careful and measure accurately the strength of your laminae 
you can get good results for some stacking sequence, but don't try to extend 
them to some other stacking sequence without verification.

  Some reasons why this does not work.

  First of all, the strength of a ply in a laminate can be different from that 
of UD laminate with the same orientation\footnote{This is not related to edge 
effects which are another 
problem~\cite{Vasilev2001}}~\cite{paris2001study,Sun1996}. 
  Many failure criteria in fact use what they call \textit{in situ} strengths, 
which anyway are not so easy to measure. 
  This happens due to constraints imposed by adjacent plies especially on matrix 
deformation and can also be due to thermal stresses.

  In laminates thermal stresses can be high and they should be accounted for. 
Unfortunately this can be very difficult.
  
  Another thing to remember is that, when we speak about failure, the hypothesis 
of plane stress may not be so good. While there is no effect on stiffness 
predictions, failure is a much more complicated thing.
  For instance it is easy to imagine cases when out of plane stresses may be 
important~\cite{paris2001study}. 
  For instance under transverse loading imagine to have a circumferential crack  
that forms due to debond of the fiber\mbox{-}matrix(vai alla lavagna) interface. 
A positive stress $\sigma_3$ promotes debond, a negative one hinders 
it~\cite{paris2001study}.
  The interaction of out of plane stresses with in\mbox{-}plane stresses when 
failure comes into play is one of the reasons why predictions for failure in 
laminates using lamina criteria performs so poorly~\cite{paris2001study}.

  Hence First Ply Failure is not very reliable, and adequate safety factors must 
be used in the analysis
}

\mode<presentation>{
\begin{frame}
    \frametitle{first ply failure envelope: example}
    \begin{scriptsize}
        First Ply Failure Envelope for E-glass/MY750 
        $\left[\pm55\right]$ laminate. Data from \cite{hinton2002comparison}.
    \end{scriptsize}
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[xlabel={$\sigma_y$ [MPa]}, ylabel={$\sigma_{x}$ [MPa]}, 
                  width =0.68\textwidth,ymax = 600, ymin = -600, xmax = 1000, 
xmin = -1000]
        \addplot+[only marks]  table[x index= 0, y index = 1] 
        {../../images/FPF_experimental.csv};
        \addplot+[only marks] table[x index= 0, y index = 1] 
        {../../images/FPF_tsai.csv};
        \addplot+[color=black,only marks] table[x index= 0, y index = 1] 
        {../../images/FPF_wolfe.csv};
        \draw (axis cs:\pgfkeysvalueof{/pgfplots/xmin},0) -- (axis 
cs:\pgfkeysvalueof{/pgfplots/xmax},0);
        \draw (axis cs:0,\pgfkeysvalueof{/pgfplots/ymin}) -- (axis 
cs:0,\pgfkeysvalueof{/pgfplots/ymax});
        \node[coordinate,pin=above left:{\textcolor{red}{Tsai-Wu}}]at (axis 
cs:-54.9451,50.7353) {};
        \node[coordinate,pin=below right:{\textcolor{blue}{Experimental}}]at 
(axis cs:-516.484,-350.735) {};
        \node[coordinate,pin=above:{\textcolor{black}{Wolfe}}]at (axis 
cs:362.637,92.6471) {};
      \end{axis}
    \end{tikzpicture}
  \end{center}
    
\end{frame}

}

\mode<article>{
\item Here we have an example of the failure envelope for a 
$\left[\pm55\right]$ 
laminate subject to biaxial normal stresses $\sigma_{x}$ and $\sigma_{y}$. 
  Of course for laminates the shape of the failure envelope is much more 
complicated than for unidirectional plies: in fact as we move in the stress 
space, different laminae can fail first, so the appearance of the envelope can 
be piece-wise and sharp-cornered.
  Here blue points are experimental data, red points are data generated using 
Tsai-Wu criterion and the other points are generated using Wolf's theory which 
we did not looked at, and at any rate it does not work either.
}
\subsection{Laminate Failure Analysis}

\mode<presentation>{  
\begin{frame}
    \frametitle{Laminate Failure}
    There are tens of theories/techniques for such 
    predictions~\cite{Hinton15012013,hinton2002comparison}:
    \begin{itemize}
        \item Some are very simple: linear elastic materials, no interaction 
            between failure modes, simple degradation of laminae properties.
        \item Some are very complex: \mbox{micro-mechanical} modeling of 
            damage, \mbox{non-linear} \mbox{stress-strain} material 
            behaviour (important for shear), 
            hygrothermal stresses etc. and use tens of material parameters.
    \end{itemize}
\end{frame}
}

\mode<article>{
\item What about Laminate Failure Analysis, or the prediction of ultimate 
failure load and failure mechanism?
  
  There are tens of  theories and methods for such 
predictions~\cite{Hinton15012013,Soden1998}. Some are rather simple and some are 
extremely complicated. The simple ones limit their attention to simple linear 
elastic behavior of the constituents, use simple criteria for first ply failure. 
Others add features and features in order to improve their predictions: they 
 consider micro-mechanics and non linear stress strain behavior of laminates 
such as the one you can see in shear, add hygrothermal stresses, model damage 
during progressive failure and so on. To use them one can easily have to measure 
tens of material parameters.
  Many of them have been implemented into complex structural analysis tools such 
as finite element packages.
 
}

\mode<presentation>{
\begin{frame}
    \frametitle{Progressive Failure Analysis: general approach}
    \begin{columns}
        \begin{column}{0.55\textwidth}
            \begin{tikzpicture}
                \begin{axis}[xlabel={Deformation}, ylabel={Load}, 
width=\textwidth, xmin = 0,xmax=1.6,ymax=1.3,ymin = 0,
                              ytick=\empty, xtick=\empty, extra y 
ticks={1},extra y tick labels={FF},unbounded coords=jump,
                                legend pos=south east]
                    \addplot plot coordinates
                    {(0,0) (0.1,0.5) (0.3,0.72) (0.5,0.85) (0.6,inf) (1,1)};
                    \addlegendentry{ply discount}
                    \draw[dashed] (axis cs:0,1) -- (axis 
cs:\pgfkeysvalueof{/pgfplots/xmax},1);
                    \draw[dashed,blue] (axis cs:0.5,0.85) -- (axis cs:1,1);
                    \node[coordinate,pin=below right:{FPF}]at (axis cs:0.1,0.5) 
{};
                    \node[coordinate,pin=below right:{\begin{scriptsize}Second 
Ply Failure\end{scriptsize}}]at (axis cs:0.3,0.72) {};
                    \node[coordinate,pin={[pin 
distance=5ex]90:{\begin{scriptsize}Third Ply Failure\end{scriptsize}}}]at (axis 
cs:0.5,0.85) {};
                \end{axis}
            \end{tikzpicture}

        \end{column}
        \begin{column}{0.45\textwidth}
            \begin{block}{Ply discount}
                \begin{itemize}
                    \item find FPF
                    \item reduce lamina stiffness
                    \begin{equation}
                        Q_{ij}=r_{ik}Q_{kj}^{\text{undamaged}}\nonumber 
                    \end{equation}
                    simplest choice:
                    \begin{align}
                        &E_{11} = E_{11}^{\text{undamaged}}\nonumber\\
                        &\text{all others: same coefficient $r$}\nonumber\\
                        &\text{(often set to zero)}\nonumber
                    \end{align}
                    \item rebuild $ABD$ matrix
                    \item load up to next ply failure; repeat until first fiber 
failure (FF)
                \end{itemize}
            \end{block}

        \end{column}
    \end{columns}

\end{frame}
}

\mode<article>{
  \item Although they are very different they more or less share the same 
    scheme.
  Now let's look schematically at what happens during laminate failure on this 
graph were we have some generic load intensity plotted against the deformation 
it causes to the laminate, be it extension or deflection or whatever. 
  As the load increases we reach the load level for first ply failure. 
  Here we can continue our analysis by taking into account that for most 
composites, if matrix cracking takes place, the stiffness of the layer is 
significantly reduced. So before proceeding we must find a new stiffness matrix 
for our layer. 
  Anyway since the exact amount of stiffness reduction is hard to measure, one 
uses some empirical coefficient to reduce the stiffness of the failed lamina. 
  This is often left to the experience of the designers. Common choices for such 
a degradation are to assume that the stiffness in fiber direction is not 
affected and that all other engineering constants undergo the same degradation. 
  Often it is assumed that the the stiffness in all directions except that in 
fiber direction reduces to zero, so this method is called ply discount method. 
Generally degradation is assumed to be the same irrespective of orientation.
  More refined theories of course use much more sophisticated procedures for 
stiffness knock down.

  Once you did that, you can rebuild the $ABD$ matrix for the laminate and find 
the load which causes the second layer to fail; now also this layer must be 
degraded, and you repeat until you find the load for the first fiber failure in 
the laminate. This is the failure load. In this process one must be careful when 
using failure criteria for laminae which already failed. If the lamina failed by 
a matrix related mechanism, it cannot fail again by the same mechanism and only 
fiber failure criteria shall be checked for that lamina.
  This allows the designer to find not only the load but also some deformation 
measure during the failure process, which is important as the geometry of the 
plate can be for instance subject to some design constraint, such as 
interference with other components.
}

\mode<presentation>{
\begin{frame}
    \frametitle{Progressive Failure Analysis: general approach}
    \begin{columns}
        \begin{column}{0.55\textwidth}
            \begin{tikzpicture}
                \begin{axis}[xlabel={Deformation}, ylabel={Load}, 
width=\textwidth, xmin = 0,xmax=1.6,ymax=1.3,ymin = 0,
                              ytick=\empty, xtick=\empty, extra y 
ticks={1},extra y tick labels={FF},unbounded coords=jump,
                                legend pos=south east]
                    \addplot plot coordinates
                    {(0,0) (0.1,0.5) (0.3,0.72) (0.5,0.85) (0.6,inf) (1,1)};
                    \addlegendentry{ply discount}
                    \addplot plot coordinates
                    {(0,0) (0.1,0.5) (1.5,1)};
                    \addlegendentry{full degradation}
                    \draw[dashed] (axis cs:0,1) -- (axis 
cs:\pgfkeysvalueof{/pgfplots/xmax},1);
                    \draw[dashed,blue] (axis cs:0.5,0.85) -- (axis cs:1,1);
                    \node[coordinate,pin=below right:{FPF}]at (axis cs:0.1,0.5) 
{};
                    \node[coordinate,pin=below right:{\begin{scriptsize}Second 
Ply Failure\end{scriptsize}}]at (axis cs:0.3,0.72) {};
                    \node[coordinate,pin={[pin 
distance=5ex]90:{\begin{scriptsize}Third Ply Failure\end{scriptsize}}}]at (axis 
cs:0.5,0.85) {};
                \end{axis}
            \end{tikzpicture}
        \end{column}

        \begin{column}{0.45\textwidth}
            \begin{block}{Full degradation}
                \begin{itemize}
                    \item find FPF
                    \item reduce  stiffnesses for \textcolor{red}{all} laminae 
according to
                    \begin{equation}
                        Q_{ij}=r_{ik}Q_{kj}^{\text{undamaged}}\nonumber 
                    \end{equation}
                    \item rebuild $ABD$ matrix
                    \item increase load to find first fiber failure
                \end{itemize}
            \end{block}

        \end{column}
    \end{columns}

\end{frame}
}

\mode<article>{
\item A simpler way to estimate the first fiber failure load is to assume that 
after First Ply Failure all of the laminae are degraded immediately and to the 
same extent. So now one just rebuilds the $ABD$ matrix and search for fiber 
failure 
load. This does not give a correct indication of the strain at failure. But 
neither do other and more sophisticated theories.
}



\mode<presentation>{
\begin{frame}
    \frametitle{Truncated Maximum Strain}
    alternative method for Fiber Failure in 
    laminates~\cite{barbero2011introduction}
    \begin{columns}
        \begin{column}{0.55\textwidth}
          \begin{figure}[ht]
              \centering
              \def\svgwidth{\textwidth}
              \input{../../images/truncated_max_strain.pdf_tex}
              % \input{<filename>.pdf_tex}: 0x0 pixel, 0dpi, 0.00x0.00 cm, bb=
              \label{fig:trunc_max_strain}
          \end{figure}
        \end{column}

\begin{column}{0.45\textwidth}
    \begin{itemize}
        \item can be used with [0/90/$\pm\theta$] type laminates 
            (no UD!) without clusters of layer with the same angle
        \item assumption: fiber strains $\epsilon_1,\epsilon_2$ are 
            equal to layer strains
        \item fiber failure
        \begin{align}
            E^-<&\epsilon_1<E^+\nonumber\\
            E^-<&\epsilon_2<E^+\nonumber\\
            \left|\epsilon_1 - \epsilon_2 \right| & = 
                (1+\nu_{12})\max(E^-,E^+)\nonumber
        \end{align}
        \item support for \textcolor{red}{micro-cracking} knock down 
when needed
    \end{itemize}
\end{column}

\end{columns}

\end{frame}
}

\mode<article>{
  \item The truncated maximum strain criterion~\cite{barbero2011introduction} 
    is an alternative  method to use with laminates to calculate the first fiber 
    failure load. 
    It is pretty common in the aeronautics industry.
    The criterion basically assumes that for each layer there is a layer with 
    fibers oriented more or less perpendicularly that can restrain the 
    fiber deformation of the former layer in transverse direction, and can carry 
    all the load transferred by a failed layer. 
    For the criterion to work, there mustn't be cluster of layers with similar 
    orientation.
    Is applied layer by layer but is laminate failure criteria, it cannot be 
    used with UD composites. 
    One basic assumption is that one can confuse the strains in the layers with 
    that in the fibers, which of course is OK along fiber direction but not so 
    good in transverse direction.

    Fiber failure is assumed to take place when one of the following equations 
    is satisfied:
    \begin{align}
        E^-<&\epsilon_1<E^+\\
        E^-<&\epsilon_2<E^+\\
        \left|\epsilon_1-\epsilon_2\right|&=(1+\nu_{12})\text{max}(E^-,E^+)
    \end{align}
    where $E^\pm, F^\pm$ are the maximum strain in tension and compression of a 
UD 
laminate, not of fibers: a sort of in situ strain at failure.
  The first equation tell us if the fiber in a lamina fails. 
  The third equation is the criterion for shear failure of fibers, derived under 
the simplifying assumption that the strains $\epsilon_1$ and $\epsilon_2$ are 
principal.
  Whether such an equation is really needed to reproduce the actual behavior of 
laminate is questionable~\cite{Sun1996}, but due to the large scatter in the 
available measurements there is still no clear understanding of the problem.
  As to the second equation, note that it predicts failure when the transverse 
strain equals the longitudinal failure strain. This sound strange but remember 
that by hypotheses for each layer there is a layer with perpendicular fibers. 
  The second equation then is actually a check of the fiber strain in the 
accompanying layer, which due to compatibility must have the same transverse 
deformation.
  
  In addition one can add some knock down to the strain limits. For instance if 
one is concerned with micro\mbox{-}cracking because the matrix is brittle with 
respect to the fibers, or when fatigue life is affected by 
micro-cracking~\cite{barbero2011introduction}. In this case one can use not the 
failure strain of UD laminae in the fiber direction in tension as a limit, but 
rather the one in transverse direction.
}

\mode<presentation>{
\begin{frame}[t]
    \frametitle{Accuracy of Laminate Failure Analysis}
    ultimate strength of multi-directional laminates:\newline
    from single-ply data, the most accurate theories/procedures can hope  at 
best for a \textcolor{red}{$\pm50$\textdiscount{} accuracy} in 80\textdiscount{} 
of cases and for a \textcolor{red}{$\pm10$\textdiscount{} accuracy} in 
35\textdiscount{} of cases~\cite{hinton2002comparison,Kaddour01032013}.
\newline \textcolor{red}{\emph{Poor and not conservative}}\newline 
Some design suggestions
\begin{itemize}
    \item use laminate failure analysis for preliminary design \ldots
    \item \ldots but do your homework and test your products: model 
calibration against laminate data can improve accuracy
    \item Knock down, knock down, knock down!
    \item where possible use fracture/damage mechanics approaches
\end{itemize}
\end{frame}
}

\mode<article>{
  \item So here we are. Do these criteria work or not? Hardly.
  For laminate failure the most accurate theories/procedures can hope at best 
for a $\pm50$\textdiscount{} accuracy in 80\textdiscount{} of cases and for a 
$\pm10$\textdiscount{} accuracy in 35\textdiscount{} of 
cases~\cite{hinton2002comparison,Kaddour01032013}.
  This is very poor and generally speaking not conservative.

  So what can we do about it?
  We can use these methods in preliminary design: we need something to do that. 
But we cannot hope at the moment to avoid a ``make and test'' 
approach~\cite{Kaddour01032013}. Extensive testing of laminates with the same 
stacking sequence of the final product and of the final product itself are still 
required. 
  Moreover, calibration of the theoretical models we outlined before against 
data from laminates rather than from UD laminates can often allow obtaining a 
better accuracy~\cite{hinton2002comparison,Kaddour01032013}. Of course one needs 
testing, because you won't find such data on data\mbox{-}sheets.

  In the meanwhile, try to be conservative, knocking down designs allowables.

  Broadly speaking, using strength values for design is a poor approach, as we 
already said. This is especially true for materials with a very complex and 
heterogeneous structure such as composites. Moreover nowadays composites 
manufacturing cannot grant the same reproducibility of, for instance, metals. 
This means that defects in the material can significantly vary for one case to 
another. 

  A better approach would be for instance using methods based on damage 
mechanics.
  An even better approach would be to use fracture mechanics based design, but 
we won't see nothing about it in this course as probably nobody as a background 
in fracture mechanics, so you  will have to wait until next year!

}
\subsection{Interlaminar Failure}

\mode<presentation>{
\begin{frame}
    \frametitle{Interlaminar stresses}
    Transverse loads and compatibility between layers with different 
    orientation cause \textcolor{red}{interlaminar stresses}.
    \begin{columns}
        \begin{column}{0.45\textwidth}
            \begin{figure}[ht]
                \centering
                \def\svgwidth{\textwidth}
                \input{../../images/interlaminar_stresses.pdf_tex}
                % \input{<filename>.pdf_tex}: 0x0 pixel, 0dpi, 0.00x0.00 cm, bb=
                \label{fig:interlam_stress}
            \end{figure}

        \end{column}
        \begin{column}{0.55\textwidth}
            How do we estimate interlaminar stresses? Easy!
            \begin{itemize}
                \item Use CLT to calculate in-plane stresses
                \item Use equilibrium $\nabla . 
\boldsymbol{\sigma}=\mathbf{0}$\newline
                \begin{align}
                    %line 1
                    \Rightarrow 
&\frac{\partial\textcolor{red}{\sigma_{z}}}{\partial z} 
=-\left(\frac{\partial\tau_{xz}}{\partial x}  + 
\frac{\partial\tau_{yz}}{\partial y}\right) \nonumber \\
                    %line 2
                    &\frac{\partial\textcolor{red}{\tau_{xz}}}{\partial z} 
=-\left( \frac{\partial\sigma_x}{\partial x}  + 
\frac{\partial\tau_{xy}}{\partial y}  \right) \nonumber\\
                    %line 3
                    &\frac{\partial\textcolor{red}{\tau_{yz}}}{\partial z} 
=-\left( \frac{\partial\sigma_y}{\partial y}  + 
\frac{\partial\tau_{xy}}{\partial x}  \right) \nonumber
                \end{align}
                \item Integrate with proper outer layer conditions to get the 
interlaminar stresses!
            \end{itemize}

        \end{column}

    \end{columns}
\end{frame}
}

\mode<article>{
  \item One of the field where fracture mechanics has been most successfully 
applied 
is that of the prediction of delaminations, that is of separation between 
different laminae. 
  This phenomenon is neglected in most laminate failure theories and this is 
surely one of the causes of their poor success rates~\cite{paris2001study}.
  The driving force of delaminations are interlaminar stresses, that is stresses 
that develop between laminae in order to grant compatibility between the layers, 
so that the displacement field can be continuous across the thickness of the 
laminate. Interlaminar stresses can also be induced by transverse loads. 
  If we extract a lamina from a laminate the interlaminar stresses  are those 
acting on the face normal to the lamina plane.

  Composites resistance to delaminations is extremely low as you saw (spacca il 
campione) and it's easy to induce delaminations even by low velocity impacts. 
Transverse cracking also promotes delamination.

  We didn't deal with interlaminar stresses during our treatment of CLT, so how 
can we estimate the interlaminar stresses? You calculate the in\mbox{-}plane 
stresses using CLT. Then you just need to use the fact that the stress tensor, 
in the absence of body forces is divergence free. You can hence obtain these 
relationships that upon integration along the laminate thickness and with the 
proper boundary conditions  allow us to estimate the stresses.
}
\mode<presentation>{
\begin{frame}[t]
    \frametitle{Interlaminar stresses: failure criteria}
    \begin{columns}
        \begin{column}{0.3\textwidth}
            \begin{figure}[ht]
              \centering
                \includegraphics[width=\textwidth,keepaspectratio=true]
                {../images/sbs.jpg}
              % sbs.jpg: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
              \caption*{The short beam shear test (ASTM D 2344): bending of
                        a squat beam to minimize bending and maximize shear 
                        stresses. Large pins are used to avoid compression 
                        failure}
              \label{fig:sbs}
            \end{figure}
        \end{column}
        \begin{column}{0.7\textwidth}
            Use an \textit{ad hoc} restriction of Tsai-Wu criterion
            \cite{Vasilev2001}:
            \[
            \sigma_z \left(\frac{1}{Z^+} - \frac{1}{Z^-} \right) + 
            \left(\frac{\tau_r}{I}\right)^2 = 1 
            \]
            where $I$ = interlaminar shear resistance, $Z$ = 
            interlaminar normal strength (not necessarily equal to $Y$) and 
            $\tau_r = \sqrt{\tau_{zx}^2+\tau_{zy}^2}$. \newline
            $I$ can in principle be obtained by the \textcolor{red}{short beam 
            shear} test.\newline
            This is very rough and fracture mechanics (see 
            e.g.~\cite{Williams1989}) should be preferred.
        \end{column}
    \end{columns}
\end{frame}
}

\mode<article>{
  \item Using the same strength approach we used until now, failure by 
interlaminar 
stress can be estimated by using a specialization of Tsai-Wu 
criteria~\cite{Vasilev2001}. One commonly used is 
  \[
\sigma_z\left(\frac{1}{Z^+}-\frac{1}{Z^-}\right)+\left(\frac{\tau_r}{I}\right)^2 
=1
  \]
  where  $I$ is  interlaminar shear resistance, $Z$  is the interlaminar normal 
strength (which is not necessarily equal to $Y$) and $\tau_r = 
\sqrt{\tau_{zx}^2+\tau_{zy}^2}$.
  $I$ can in principle be obtained by the short beam shear test, which is a 
bending test of a UD laminate. The laminate is very squat; the ASTM 
standards\cite{ASTMD2344} prescribes a length over thickness ratio of about 6. 
  In this way the influence of bending stresses is reduced and ideally the 
laminate should fail by shear. 
  Anyway with most modern tough matrices it often happens that some other kind 
of failure mechanism takes place, such as compression failure near the load 
points.

  As with other stress based criteria the predictions are broadly  speaking poor 
and fracture mechanics is preferable in this case.
  
}