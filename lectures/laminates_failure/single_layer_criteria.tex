%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
  \frametitle{Single ply criteria}
  Failure criterion: rule that says when a critical event (failure) takes 
  place. They have the general form 
  \[
      F(\boldsymbol{v})=1\quad \text{such that}\quad 
      \begin{dcases}  
      F < 1 & \text{safe} \\
          F \geq1 & \text{failure}
      \end{dcases}
    \]
  and the critical condition depends on the set of variables $\bm{v}$ 
  \begin{columns}[T]
    \begin{column}{0.4\textwidth}
        \begin{block}{An example: Von Mises yield criterion}
            In this case $\bm{v}=\bm{\sigma}$ 
            and $F=\sqrt{3 J_2}/\sigma_y$. Its a cylinder in the 
            principal stress space. You generally \emph{don't apply it to
            composites.}
        \end{block}
    \end{column}
    \begin{column}{0.6\textwidth}
    \centering
    \begin{tikzpicture}
        \begin{axis}[width=0.8\textwidth,
                     axis lines=center, axis on top,
                     xtick=\empty,ytick=\empty,ztick=\empty,
                     clip=false,
%                      xmin=-2,xmax=2, ymin=-2, ymax=2,
                     view={-30}{-30},
                     xlabel={$\sigma_I/\sigma_v$},
                     ylabel={$\sigma_{II}/\sigma_v$},
                     zlabel={$\sigma_{III}/\sigma_v$}
                     ]
        \addplot3[mesh,domain=0:2*pi,y domain=-1:1,draw=red] 
        ({sqrt(2)*sin(deg(x))+y},
         {sqrt(2)*cos(deg(x))+y},
         {-sin(deg(x))-cos(deg(x))+y});
        \end{axis}
    \end{tikzpicture}
    \end{column}
\end{columns}

\end{frame}
}

\mode<article>{
\item A failure criteria is a rule that allow one to discriminate whether 
something 
will fail or not. 

}


\mode<presentation>{
\begin{frame}
\frametitle{}
Failure Criteria for laminae are generally~\cite{Vasilev2001}
\textcolor{red}{phenomenological} 
(do not consider the physics of failure)
\begin{itemize}
\item micro-phenomenological: based on constituent micro-stresses and 
strain acting at the micro-scale (uncommon)
\item macro-phenomenological: based on laminate \textcolor{red}{stresses} 
or \textcolor{red}{strains}
\item general expression for single plies (plane stress):
    as a surface in stress space (PMRS)
    \item differ for the inclusion of 
    \begin{itemize}
    \item \textcolor{red}{interaction terms} in the expression for $F$
    \item \textcolor{red}{failure mechanism} \textit{e.g.} fiber fracture, 
    matrix failure etc.
    \end{itemize}
    \end{itemize}
\end{frame}
}

\mode<article>{
    \item Broadly speaking all the criteria for failure of a single ply are 
    phenomenological, meaning that they do not consider the physics of failure, 
    for instance at a molecular level.
    Among phenomenological criteria there are basically two types: 
    micro-phenomenological or macro-phenomenological criteria\cite{Vasilev2001}.
    The first are based on the stresses that act at the microscale within 
    the single composite constituents. 
    These are pretty uncommon. 
    The latter instead are based on the stresses as evaluated at the 
    laminate scale and are those used routinely.
    If we stick to macro-phenomenological criteria, for single plies 
    the most generic expression of failure criterion can be given as   
    \begin{equation}
        F(\boldsymbol{\sigma})=1\quad \text{such that}\quad 
        \begin{dcases}  
        F < 1 & \text{safe} \\
            F \geq1 & \text{failure}
        \end{dcases}
    \end{equation} 
        which is a surface in the stress space. 
   It is pretty common to express them in principal material reference 
        system.
        As a rule, let me say that stress or strain based criteria for failure 
        are a 
        poor approach even for homogeneous materials, let alone composites which 
        are 
        much more complicated from the micro-structural point of view
        There are a great number of failure criteria which have been put forward 
        to 
        model failure of single ply or UD laminates, at least 10 
        (see \cite{paris2001study} for a review). 
The main differences between are based on 
the inclusion in their formulation of two features.
The first is interaction between different stress components, for example 
transverse and shear stresses. The second one is the inclusion, in the 
criterion, of a suitable way to discriminate between different failure modes of 
the composite, such as fiber fracture, matrix failure by shear or tensile and 
so 
on. 

This is important in laminate design, as we said before, because if fiber fails 
it is possible that the whole laminate fails, while if it is matrix which fails 
first there may still be some load-carrying capability if there are some fibers 
in the load direction. Moreover this gives the designer an indication on how to 
improve the laminate by adding fibers in the direction of the load component 
which causes failure~\cite{barbero2011introduction}.

These two aspects as we shall see later are rather intertwined.
}

\mode<presentation>{
\begin{frame}
\frametitle{Maximum Stress Criterion}
        \begin{equation}
        \begin{cases}
            \sigma_1< X^+, \sigma_2< Y^+ \quad &\text{if} \quad \sigma_1, 
                \sigma_2 > 0\\
            \lvert\sigma_1\rvert< X^-, \lvert\sigma_2\rvert< Y^- \quad 
                &\text{if} \quad \sigma_1, \sigma_2 < 0\\
                \lvert\tau_{12}\rvert<S
            \end{cases}\nonumber
        \end{equation}
    \begin{itemize}
     \item no stress interaction 
     \item failure mode taken into account
    \end{itemize}
    \begin{center}

    \begin{tikzpicture}
    
    \begin{axis}[xlabel={$\sigma_1/X^+$},
             ylabel={$\sigma_2/Y^+$},
             zlabel={$\sigma_{12}/S$},
             width= 0.5\textwidth,
             view/h=135 
                 ]
    \addplot3[mesh,draw=red,domain=-1.3:1,y domain=-1.2:1]
        ({x},{y},{1});
    \addplot3[mesh,draw=blue,domain=-1.2:1,y domain=-1:1]
        ({1},{x},{y});
    \addplot3[mesh,draw=black,domain=-1.3:1,y domain=-1:1]
        ({x},{1},{y});
    \end{axis}
    \end{tikzpicture}
    \end{center}   
\end{frame}
}

\mode<article>{
    \item The first one 
    you saw is the maximum stress criterion. This criterion says that a lamina 
    fails 
    when one of the component of stress independently reaches some critical 
    value, 
    which is generally different for tension and compression.
    Here it is in formulae, referred to the principal material reference system:
        \begin{equation}
        \begin{cases}
        \sigma_1< X^+, \sigma_2< Y^+ \quad &\text{if} \quad \sigma_1, 
        \sigma_2 
        > 0\\
            \lvert\sigma_1\rvert< X^-, \lvert\sigma_2\rvert< Y^- \quad 
        &\text{if} 
        \quad \sigma_1, \sigma_2 < 0\\
            \lvert\tau_{12}\rvert<S
        \end{cases}
        \end{equation}
        where $X,Y$ and $S$ are strength value obtained when only one component 
        acts, 
        the $\pm$ signs are used to differentiate between tension and 
        compression. 
        In the stress space this obviously defines box. 
        This criterion does not take stress interaction into account but allows 
        distinguishing the various failure mechanism, as longitudinal failure 
is 
fiber-dominated  while transverse and shear failure are usually associated with 
matrix failure.
  Anyway sometimes failure takes place due to failure of fibers, such as in the 
case of aramid fibers~\cite{Hull1996,Vasilev2001}.
  This criterion generally over-estimates the failure stress when there is more 
than one component of stress acting~\cite{barbero2011introduction}.
}

\mode<presentation>{
\begin{frame}
\frametitle{Maximum Strain Criterion}
\begin{equation}
    \begin{cases}
    \epsilon_1< E^+, \epsilon_2< F^+ \quad &\text{if} \quad 
    \epsilon_1, \epsilon_2 > 0\\
        \lvert\epsilon_1\rvert< E^-, \lvert\epsilon_2\rvert< F^- \quad 
    &\text{if} \quad \epsilon_1, \epsilon_2 < 0\\
        \lvert\gamma_{12}\rvert<G
    \end{cases}\nonumber
\end{equation}
\begin{itemize}
    \item stress interaction via Poisson effect
    \item failure mode taken into account
\end{itemize}
\end{frame}
}

\mode<article>{
\item  Maximum strain criterion is similar to the maximum stress criterion, 
but is 
expressed in terms of strains. The lamina fails when one of the strains 
components independently reaches a critical value. 
In formulae:
    \begin{equation}
    \begin{cases}
    \epsilon_1< E^+, \epsilon_2< F^+ \quad &\text{if} \quad \epsilon_1, 
    \epsilon_2 > 0\\
        \lvert\epsilon_1\rvert< E^-, \lvert\epsilon_2\rvert< F^- \quad 
    &\text{if} \quad \epsilon_1, \epsilon_2 < 0\\
        \lvert\gamma_{12}\rvert<G
    \end{cases}
    \end{equation}
    where $E,F$ and $G$ are failure strain as before. In the stress space it 
    is a 
    skewed box, skew angle being linked to Poisson's ratio which of course 
    comes 
    into play when going from a strain representation to a stress 
    representation of 
    the criterion.
    The criterion ignores strain interaction, but takes into account stress 
    interaction via Poisson's effect, but its predictions are overall 
similar to the maximum stress criterion~\cite{Vasilev2001}.
  Anyway note that Maximum strain criterion is useful for those material, such 
as wood, that in compression fail by longitudinal cracking in the matrix, where 
it is the only criterion that actually works~\cite{Vasilev2001}.
}



\mode<presentation>{
\begin{frame}
\frametitle{Tsai-Hill failure criterion}

\begin{columns}[T]
    \begin{column}{0.52\textwidth}
    \begin{equation}
    \left( \frac{\sigma_1}{X} \right)^2 - 
    \frac{\sigma_1\sigma_2}{X^{\,2}} + \left( 
        \frac{\sigma_2}{Y} \right)^2 + \left( \frac{\tau_{12}}{S} \right)^2 = 1 
    \nonumber
    \end{equation}
    \begin{itemize}
    \item Stress interaction: partially accounted for 
        different stress  components.
    \item Failure mode:  not taken into account 
        (can use different strengths for tension/compression).
    \end{itemize}
    \end{column}
    \begin{column}{0.48\textwidth}
    \begin{figure}
        \centering
        \def\svgwidth{\textwidth}
        \input{../../images/tsai_hill.pdf_tex}
        \label{fig:tsai_hill}
    \end{figure}
\end{column}
\end{columns}
\end{frame}
}
                
\mode<article>{
\item  The criteria we saw before are linear with respect to the stresses or 
strains. Another commonly used type of criteria are quadratic critera. The 
idea of using this type of criteria comes of course from the actual shape of 
the failure loci in composite materials, such as those we saw before. 
The simplest quadratic criteria is Tsai-Hill  criterion, whose expression is 
the one given 
here:
\begin{equation}
\left(\frac{\sigma_1}{X}\right)^2-\frac{\sigma_1\sigma_2}{X^2}+\left(\frac{
\sigma_2}{Y}\right)^2+\left(\frac{\tau_{12}}{S}\right)^2=1
\end{equation}

Basically it is an adaptation of Hill criterion for anisotropic metals to plane 
stress. 
Therefore it does not take into account the heterogeneous nature of 
composites.

In the stress space it is an ellipsoid skewed with respect to the material 
reference system,due to this term taking into account interaction between normal 
stresses. It is centered at the origin anyway. So there is no difference between 
tension and compression! Anyway one can use different values in the criterion 
expression depending on the sign of the normal stresses.

In contrast to the maximum stress criterion, it allows for stress interaction, 
but in such a way that transverse stress $\sigma_2$ reduces material strength 
under shear. However, as we saw before, this is true only if transverse stress 
is tensile. If the transverse stress is compressive this is no longer true. In 
fact as you saw before, Tsai-Hill criterion can fit very well the data for a UD 
lamina in tension but not in compression.

I said fit, and in fact Tsai-Hill criterion basically is only something which 
provides a good fit to experimental data, as it has no physical basis and 
provides no way to tell by which failure mechanism the lamina will fail.
}


\mode<presentation>{
\begin{frame}
\frametitle{Tsai-Wu Criterion}
\begin{itemize}
    \item Takes interaction into account.
    \item Similar to Tsai-Hill: a generalization of the Von Mises criterion.
    For a general stress state it's given by:
    \begin{equation}
        F_i\sigma_i+F_{ij}\sigma_i\sigma_j=1.\nonumber
    \end{equation}
    \item For plane stress reduces to:
    \[
    F_1 \sigma_1 + F_2\sigma_2 + F_{11}\sigma_1^2 + F_{22}\sigma_2^2 + 
    F_{12} \sigma_1\sigma_2 + F_{66} \tau_6^2 = 1 
    \]
    where
    \begin{align*}
            & F_1 = \left( \frac{1}{X^+} - \frac{1}{X^-} \right), F_2 = 
            \left( \frac{1}{Y^+} -\frac{1}{Y^-} \right),\\
            & F_{11}=\frac{1}{X^+X^-}, 
            F_{22} = \frac{1}{Y^+Y^-}, F_{66}=\frac{1}{S^2}.
    \end{align*}
    \item $F_6=0$.
    \item $F_{12}$: to be measured independently. Sensible estimates: 
    $F_{12}=-\sfrac{1}{X^2}$ (as in Tsai-Hill) or        
    $F_{12}=\sfrac{-1}{\sqrt{X^+X^-Y^+Y^-}}$.
    \end{itemize} 
\end{frame}
}

\mode<article>{
\item To overcome the difficulties associated with Tsai-Hill criterion, Tsai 
and Wu 
proposed a new criterion which is again a generalization of the Von Mises 
criterion and also a generalization of the Tsai-Hill criterion. 

For a general stress state it's given by:
  \begin{equation}
      F_i\sigma_i+F_{ij}\sigma_i\sigma_j=1
  \end{equation}

For plane stress it reduces to 

  \[
F_1\sigma_1+F_2\sigma_2+F_{11}\sigma_1^2+F_{22}\sigma_2^2+F_{12}\sigma_1\sigma_2 
+ F_{66}\tau_6=1\nonumber
  \]
  where the coefficients are given by some combinations of the uniaxial 
resistance values:
  \begin{align*}
    &F_1 = \left(\frac{1}{X^+}-\frac{1}{X^-}\right),F_2 = 
    \left(\frac{1}{Y^+}-\frac{1}{Y^-}\right),\\
    &F_{11}=\frac{1}{X^+X^-},F_{22}=\frac{1}{Y^+Y^-},F_{66}=\frac{1}{S^2}
  \end{align*}

If the Tsai-Hill criterion is an ellipsoid whose center lies at the origin of 
the reference system but rotated, in this case we have an ellipsoid whose center 
does not necessarily correspond to the origin of the reference system: in fact 
we have a linear term. This helps when failure is not symmetric with respect to 
tension-compression. 

Of course in shear there is no difference between the sign of the shear stress, 
so the corresponding coefficient is set to zero.

The criterion contains also an interaction term for normal stresses, which 
should be measured independently under some adequate experimental conditions. 
Sensible estimates are given by these 
expressions~\cite{barbero2011introduction}: $F_{12}=-\sfrac{1}{X^2}$ (as in 
Tsai-Hill) or $F_{12}=\sfrac{-1}{\sqrt{X^+X^-Y^+Y^-}}$
}

\mode<presentation>{
\begin{frame}
    \frametitle{Tsai-Wu: UD lamina}
    \begin{columns}
    \begin{column}{0.6\textwidth}
        \begin{figure}[ht]
          \centering
          \def\svgwidth{\textwidth}
          \input{../../images/tsai_wu.pdf_tex}
          % \input{<filename>.pdf_tex}: 0x0 pixel, 0dpi, 0.00x0.00 cm, bb=
          \label{fig:tsai_wu_ud}
        \end{figure}
    \end{column}

    \begin{column}{0.4\textwidth}
     fit to UD lamina are excellent but still:
      \begin{itemize}
        \item cannot predict the failure mode
        \item interaction is built in even when there should be none (good fits 
for biaxial loading only if failure mechanism is the same for both loads)
      \end{itemize}

    \end{column}

  \end{columns}
\end{frame}
}

\mode<article>{
\item With all this enhancements, the criterion works beautifully for a UD 
laminae,providing now an excellent fit also for compression data.

Anyway it still suffers from the drawback of its close relative. First, it 
offers no mean to understand the failure mechanism. Second interaction is built 
in: you cannot avoid it even when you know there is none: each failure mechanism 
is assumed to have a gradually increasing influence on the others. As we saw 
when looking at the data for textiles, for some materials it may be OK, for 
others no, and anyway there is now way to tell if this interaction will lead to 
conservative failure predictions or not~\cite{hinton2002comparison}. As a rule, 
quadratic criteria fit well experimental data under biaxial loading as long as 
the two interacting stresses affect the same failure 
mechanism~\cite{barbero2011introduction}. When this is not true, smooth criteria 
as Tsai\mbox{-}Wu cannot work.

Anyway it provides the best available fits to the experimental data for a 
unidirectional lamina tested at varying orientation

Someone tried to use a 3D version of Tsai-Wu criteria also for laminates. Well, 
please don't.
}

\mode<presentation>{
\begin{frame}
    \frametitle{Tsai-Wu: separation of fiber and matrix failure modes}
    Build separate quadratic criteria for fiber and matrix failure:
    \begin{block}{Fiber Failure}
        \begin{equation}
            F_1\sigma_1+F_{11}\sigma_1^2=1\nonumber
        \end{equation}
        or (Hashin)
        \begin{equation}
            \begin{cases}
                
\left(\frac{\sigma_1}{X^+}\right)^2+\left(\frac{\tau_{12}}{S}\right)^2=1 \quad 
\sigma_1>0\\
                \left|\sigma_1\right|=X^-\quad \sigma_1<0 \nonumber
            \end{cases}
        \end{equation}
    \end{block}
    \begin{block}{Matrix Failure}
        \begin{equation}
            F_{22}\sigma_2^2+F_2\sigma_2+ F_{66}\tau_6^2=1\nonumber
        \end{equation}
    \end{block}
\end{frame}
}

\mode<article>{

\item In order to try to remove these shortcomings it has been tried to 
separate the fiber dominated failure mechanisms from the matrix dominated ones.

This can be used during laminate design to improve the strength of laminate by 
adding some laminae in the direction of the loads that promote matrix 
failure~\cite{barbero2011introduction}.

There have been many proposals also in this field~\cite{paris2001study}. As a 
general rule one can separate fiber failure by taking, from Tsai-Wu criterion, 
the terms corresponding to the loading in fiber direction (direction one) 
separately. According to our expression of the criterion one is lead to this 
expression. Similar expression where used by Hashin for its failure criterion 
which is pretty common in industry, and also takes into account a possible fiber 
shear failure, which can happen for instance for aramid fibers.

Following the same reasoning the stress components that are likely to affect 
matrix failure are grouped together in the same expression, giving a criterion 
for matrix failures.
 
}

\mode<presentation>{
\begin{frame}
    \frametitle{Which criterion do you use?}
    \footnotesize{data from an old survey (1983) by the American Institute of 
Aeronautics and Astronautics}
    \begin{tikzpicture}
        \centering
        \begin{axis}[ybar, symbolic x coords={{Maximum Strain}, {Maximum 
Stress},{Tsai-Hill},Tsai-Wu, Other},%xtick={0,...,4}
                    ymin=0, width = 0.75\textwidth, ylabel = 
{\begin{large}Replies [\textdiscount]\end{large}},%
                      xticklabel style={align=center,text width=1cm, 
font=\footnotesize}]
         \addplot[fill=blue,draw=black] plot coordinates
        {(Maximum Strain,30) (Maximum Stress,22) (Tsai-Hill,17.5) (Tsai-Wu,12.5) 
(Other,18)};
        \end{axis}

    \end{tikzpicture}

\end{frame}
}

\mode<article>{
  \item The criteria we saw have their own sets of advantages and drawbacks.
  So people which criterion does use in design? 
  Well, these are the data from an old survey from the American Institute of 
Aeronautics and Astronautics~\cite{Burk198358}, which asked to designers which 
criterion did they use. 
  Anyway, maximum strain and stress criteria are the most commonly used followed 
by Tsai-like criteria. A substantial portion of designers
  uses criteria which we did not mention.
  Unfortunately there are no more recent data available. 
  Anyway it is likely that most use the criteria built-in the with the finite 
elements code they use in design.
}