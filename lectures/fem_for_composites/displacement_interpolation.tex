%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Interpolation}
Interpolation: technique to generate additional data given a discrete data 
set at some points (poles or \emph{nodes}).
Considering a univariate function, interpolation of the 
\textcolor{red}{data set}
$\lbrace(x_0,f(x_0)),\ldots,(x_n,f(x_n)),\ldots, (x_n,f(x_n))\rbrace$ 
means to find a possibly piece-wise function $\textcolor{blue}{g}$ defined over 
the interval 
$\Omega_e = \left[\min(x_i), max(x_i) \right]$ such that
\begin{columns}[T,onlytextwidth]
    \begin{column}{0.5\textwidth}
    \[
        \textcolor{blue}{g(x_i)} = \textcolor{red}{f(x_i)},
    \]
    i.e. which goes through the poles.
    Data can be generated over $\Omega_e$ by evaluating $g$.\newline
    Hopefully $g(x)$ is close to $f(x)$ also for $x \neq x_i$;
    if $f$ is well behaved, the smaller $\Omega_e$ the better the chances of 
    making a good approximation.
    \end{column}
    \begin{column}{0.5\textwidth}
        \centering
        \begin{tikzpicture}
            \begin{axis}[xlabel=$x$, xmin=0, xmax=3,
                         ylabel={\textcolor{red}{$f$},\textcolor{blue}{$g$}},
                         width = \textwidth]
            \addplot[samples=20,smooth,blue]{x^3-3*x^2+2};
            \addplot[only marks, red, mark=*] coordinates
            {(0,2)
             (0.5,1.375)
             (0.75,0.734375)
             (1.3,-0.8730)
             (1.65,-1.675375)
             (2.2,-1.871)
             (2.8,0.432)
             
                };
            \end{axis}

        \end{tikzpicture}
    \end{column}

\end{columns}


\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Finite element interpolation}
In solid mechanics, usually:
\begin{itemize}
    \item the unknown is the displacement field, a 3D 
    vector field over 3D spaces. However with composites often one works with 2D 
    domains (plates and shells); attention will therefore be confined to 2D 
    domains.
    \item the 2D shapes for the finite elements is limited 
    to triangles and quadrilaterals; this provides a good balance between 
    simplicity and the ability to approximate complex shapes.
\end{itemize}

Requirements for automatic convergence of the solution with shrinking element 
size~\cite{zienkiewicz2000finite}:
\begin{itemize}
    \item \textcolor{red}{completeness}: the displacement field must be able
    to reproduce exactly the constant strain case; this includes rigid body
    motions (vanishing constant strain)
    \item \textcolor{red}{compatibility}: the approximated field over the 
    boundary of different elements must be continuous ($C^0$ continuity)
\end{itemize}
For the sake of simplicity usually no stricter requirements are imposed.
\end{frame}
    
}

\mode<presentation>{
\begin{frame}
\frametitle{An example: triangular elements}
\begin{itemize}
    \item interpolation functions: usually polynomials (easy to grant the 
    requirements)
    \item usually the interpolation coefficients are chosen as the values of
    the unknown at the vertices/nodes: they become the \alert{nodal degrees of 
    freedom}
\end{itemize}

\begin{columns}[T,onlytextwidth]
    \begin{column}{0.5\textwidth}
    \begin{itemize}
     \item the simplest polynomial is a linear one; e.g. for $s_1$
        \[
            s_1 = \alpha +\beta x + \gamma y,
        \]
        constant strains are allowed
     \item need three nodes to define a plane; usually taken at 
        the vertices $\Rightarrow$ displacement continuity is granted if the 
        adjacent elements have the same nodal displacements
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{tikzpicture}
        \node[label=below left:$1$,circle,fill=black](P1) at (-0.5,-1) {};
        \node[label=below right:$2$,circle,fill=black](P2) at (2,-1) {};
        \node[label=above right:$3$,circle,fill=black](P3) at (-0.5,1) {};
%         \node[label=above left:$4$,circle,fill=black](P4) at (-1.5,1) {};
        \draw[thick] (P1) -- (P2) -- (P3) -- (P1)--cycle;
        \draw[-latex] (0,0)--(0,2)node[left]{$y$};
        \draw[-latex] (0,0)--(2,0)node[right]{$x$};
        \draw[blah-blah] (P1.west)--(P3.west)node[pos=0.5,left]{$b$};
        \draw[blah-blah] (P1.south)--(P2.south)node[pos=0.5,below]{$a$};
    \end{tikzpicture}

    \end{column}

\end{columns}

\end{frame}
}
        
\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{columns}[T,onlytextwidth]
    \begin{column}{0.5\textwidth}
    \begin{itemize}
     \item the constants $\alpha,\beta$ and $\gamma$ can be determined in terms
     of the nodal values $\bar{s}_1^i$~\cite[p.88]{zienkiewicz2000finite} as
     \begin{align*} 
     s_1 &= \sum_{i=1}^{3} \bar{s}_1^{i} f_{i}(x,y) \\
         &= \frac{1}{2\Delta}\left(f_1 \bar{s}_1^{1} + f_2 \bar{s}_1^{2} +
                                   f_3 \bar{s}_1^{3} \right).
    \end{align*}
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{center}
    \includegraphics{./fem_slides-figure3.pdf}
    % fem_slides-figure3.pdf: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \end{center}
    \end{column}
\end{columns}
    \indent with 
    \[
        f_i(x,y) = \frac{1}{2\Delta} \left(\delta_i +\epsilon_i x + \zeta_i 
            y\right)
    \]
    and 
    \[
        \delta_i = x_j y_m - x_m y_j, \epsilon_i = y_j-y_m, \zeta_i = x_m-x_j
    \]
    $x_i,y_i$ being the nodal coordinates ($i \in [1,2,3]$) and $\Delta$ 
    the triangle's area; for example $\delta_1 = x_2 y_3 - x_3 y_2$.
\begin{itemize}
 \item each nodal value contributes to the displacement at some generic point 
  via its interpolation function.
\end{itemize}

\end{frame}

}
    
\mode<presentation>{
\begin{frame}
\frametitle{}
for these elements the strains are given by 
expressions of the form~\cite{zienkiewicz2000finite}

\[
    \epsilon_x = \frac{\partial s_1}{\partial x} \simeq \sum_{i=1}^{3} c_i 
\bar{s}_i^1
\]

where the constants $c_i$ obviously depend only on the values of nodal 
coordinates$\Rightarrow$ the strains are constant throughout the element and
discontinuous across elements.

\end{frame}

}

\mode<presentation>{
\begin{frame}
\frametitle{Another example: linear rectangular element}
\begin{columns}[T,onlytextwidth]
    \begin{column}{0.5\textwidth}
    \begin{itemize}
     \item again, it's convenient to put nodes at the vertices
     \item if \alert{natural coordinates } $\xi= x/a$ and $\eta = y/b$ are used 
        the expression for the interpolating functions simplifies: e.g.
        \[
            f_1 = \frac{1}{4} (1-\xi)(1-\eta)
        \]
        and, given $\bar{\eta} = \eta \eta_i$ and $\bar{\xi} = \xi \xi_i$,
        \[
            f_i = \frac{1}{4} (1+\bar{\xi})(1+\bar{\eta}), i \in \lbrace 
            1,2,3,4 \rbrace
        \]
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{tikzpicture}
        \node[label=below left:$1$,circle,fill=black](P1) at (-2,-1) {};
        \node[label=below right:$2$,circle,fill=black](P2) at (2,-1) {};
        \node[label=above right:$3$,circle,fill=black](P3) at (2,1) {};
        \node[label=above left:$4$,circle,fill=black](P4) at (-2,1) {};
        \draw[thick] (P1) rectangle (P3);
        \draw[-latex] (0,0)--(0,0.5)node[left]{$\eta$};
        \draw[-latex] (0,0)--(0.5,0)node[right]{$\xi$};
        \draw[blah-blah] (P1.west)--(P4.west)node[pos=0.5,left]{$b$};
        \draw[blah-blah] (P1.south)--(P2.south)node[pos=0.5,below]{$a$};
    \end{tikzpicture}
    \vspace{3ex}
    \begin{tikzpicture}
    \begin{axis}[   view={45}{-45},
                    axis lines=none,
                    clip=false,
                    width=\textwidth
                    ]
        %plane
         \addplot3[mesh,draw=red,
        samples=20,domain=-2:2,y domain=-1:1]
        ({x},{y},
        {0.25*(1-x*0.5)*(1-y)});
    \node[label=below left:$1$](P1) at (axis cs: -2,-1,0) {};
    \node[label=below left:$2$](P2) at (axis cs: 2,-1,0) {};
    \node[label=below right:$3$](P3) at (axis cs: 2,1,0) {};
    \node[label=below right:$4$](P4) at (axis cs: -2,1,0) {};
    \node[label=below:{\textcolor{red}{$f_1$}}] at (axis cs: -2,-1,1) {};
    \draw[fill] (P1) circle [radius=0.05];
    \draw[fill] (P2) circle [radius=0.05];
    \draw[fill] (P3) circle [radius=0.05];
    \draw[fill] (P4) circle [radius=0.05];
    \draw[thick] %
        (P1.center)--(P2.center)--(P3.center)--(P4.center)--(P1.center)--cycle;
    \end{axis}
    \end{tikzpicture}
    \end{column}

\end{columns}
\end{frame}

}


\mode<presentation>{
\begin{frame}
\frametitle{Making it smarter: mapped elements}
To better reproduce shapes we need more flexibility: mapped elements are 
the answer
\begin{center}
\begin{tikzpicture}
\matrix[every node/.style={anchor=center}, 
        column sep=0.1\textwidth,
        matrix of nodes] (m)
{  Parent element \pgfmatrixnextcell Actual element\\
%first node
        \node[label=below left:$1$,circle,fill=black](P1) at (-2,-1) {};
        \node[label=below right:$2$,circle,fill=black](P2) at (2,-1) {};
        \node[label=above right:$3$,circle,fill=black](P3) at (2,1) {};
        \node[label=above left:$4$,circle,fill=black](P4) at (-2,1) {};
        \node[label=below:$5$,circle,fill=black](P5) at (0,-1) {};
        \node[label=right:$6$,circle,fill=black](P6) at (2,0) {};
        \node[label=above:$7$,circle,fill=black](P7) at (0,1) {};
        \node[label=right:$8$,circle,fill=black](P8) at (-2,0) {};
        \draw[thick] (P1) rectangle (P3);
        \draw[-latex] (0,0)--(0,0.5)node[left]{$\eta$};
        \draw[-latex] (0,0)--(0.5,0)node[right]{$\xi$};
        
        \pgfmatrixnextcell
%second node     
        \node[label=below left:$1$,circle,fill=black](Q1) at (-2,-1) {};
        \node[label=below right:$2$,circle,fill=black](Q2) at (2.1,-1.8) {};
        \node[label=above right:$3$,circle,fill=black](Q3) at (1.8,1) {};
        \node[label=above left:$4$,circle,fill=black](Q4) at (-2.5,1.5) {};
        \node[label=below:$5$,circle,fill=black](Q5) at (0.35,-1.25) {};
        \node[label=left:$6$,circle,fill=black](Q6) at (1.85,0) {};
        \node[label=above:$7$,circle,fill=black](Q7) at (0,1.1) {};
        \node[label=left:$8$,circle,fill=black](Q8) at (-2.15,0.25) {};
        \draw[-latex] (0,0)--(0,0.5)node[left]{$X$};
        \draw[-latex] (0,0)--(0.5,0)node[right]{$Y$};
        \draw[thick] (Q1)  parabola (Q2) parabola (Q3) parabola (Q4) %
            parabola (Q1) -- cycle;
        \\
};
        \draw[smooth,-latex] (P6.north east) to[out=45,in=135]%
        (Q8.north west);
\end{tikzpicture}
\end{center}
The machinery necessary to work with these mapping is the same developed for 
surfaces!
\end{frame}

}

\mode<article>{
    \item Mapping can also be performed in 3D in the same way as we build 
surfaces from three dimensional domains
}


\mode<presentation>{
\begin{frame}
\frametitle{Adaptation to plates}
\begin{itemize}
 \item CLT: the problem is defined in terms of \alert{three unknown} 
    functions $u,v,w$ (midplane displacements):
        \[
            \bm{s}(X,Y,Z) = 
            \begin{bmatrix}
            
            u(X,Y)-Z\frac{\partial w}{\partial X}\\
            v(X,Y)-Z\frac{\partial w}{\partial Y}\\
            w(X,Y)
            \end{bmatrix}.
        \]
    Displacement continuity: the rotations ($\partial w/\partial X, 
    \partial w/\partial Y $)  must  be  continuous too!
    $\Rightarrow$ proper, specifically derived interpolating functions should be 
    formulated.
    \item for shear deformable plates kinematics is described by rotations, that
    do not depend on $w$ and are \alert{additional unknowns}~\cite{Reddy2015}.
            \[
            \bm{s}(X,Y,Z) = 
            \begin{bmatrix}
            
            u(X,Y)+Z\phi_X\\
            v(X,Y)+Z\phi_Y\\
            w(X,Y)
            \end{bmatrix},
        \]
\end{itemize}
\end{frame}
}
            
\mode<article>{
\item nothing
}
            
\mode<presentation>{
\begin{frame}
\frametitle{Adaptation to plates}
\begin{itemize}
 \item for shear deformable plates the problem is therefore formulated in terms
 of \alert{five unknown} functions. The rotations become 
 \alert{additional  nodal degrees of freedom}, work conjugated with bending 
 stress resultants~\cite{Reddy2015}.
 Displacement continuity, however, requires only that the five unknowns be 
 continuous $\Rightarrow$ the standard shape functions commonly used can be
 used in this case without modification.
 \item the problem for CLT can be formulated in the same way without requiring
 the introduction of special elements. 
 The additional constraint that $w$ shall satisfy
 \[
     \phi_x = -\frac{\partial w}{\partial x}, \text{ and }
     \phi_y = -\frac{\partial w}{\partial y}
 \]
 can be enforced using techniques that are standard in the FE method (see
  \cite{Zienkiewicz2000,zienkiewicz2000finite})
 \item however FEM solutions for CLT problems are possibly not 
    worth the effort: the same effort solves problem with a better 
    approximation so generally shear deformable plates are considered
 \item extension to shells is almost straightforward
\end{itemize}
\end{frame}
}

