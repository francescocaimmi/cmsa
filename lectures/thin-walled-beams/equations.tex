%kate: default-dictionary en_GB;
\subsection{Wall constitutive equations}
\mode<presentation>{
\begin{frame}
\frametitle{Wall constitutive equations}
Let $\bm{\alpha}=\bm{A}^{-1}$,$\bm{\delta}=\bm{D}^{-1}$ and 
$\bm{\beta}=\bm{B}^{-1}$.
Assumptions:
\begin{itemize}
    \item $\sigma_s^k=0 \quad \forall k$
    \item there is uncoupling between normal and shearing effects:
        $\alpha_{16} \approx 0,\beta_{16} \approx 0, \delta_{16} \approx 0,$.
        For example this can be achieved when off-axis layers ($\phi_k \neq 
        0^\circ \text{ or } 90^\circ$) are placed in a balanced symmetrical 
        configuration (the stacking sequence may still be not symmetric).
\end{itemize}
Then for each wall $i$
\[
    \begin{bmatrix}
       \eta_x^i\\
       \chi_x^i\\
       \gamma_{xs}^i\\
       \chi_{xs}^i 
    \end{bmatrix}
    =
    \begin{bmatrix}
        \alpha_{11} & \beta_{11} & 0 & 0\\
        \beta_{11} & \delta_{11} & 0 & 0\\
        0 & 0 & \alpha_{66} & \beta_{66}\\
        0 & 0 & \beta_{66} & \delta_{66}
    \end{bmatrix}
    \begin{bmatrix}
        N_x^i\\
        M_x^i\\
        N_{xs}^i\\
        M_{xs}^i 
    \end{bmatrix}.
\]

\end{frame}
}
\mode<article>{
\item there are case in which these coupling effects are very important and 
desired, for example in aeroelsticity (e.g.~\cite{WEISSHAAR1985}). 
However these cases are very complicated; for a membrane approximation 
treatment the reader is referred to~\cite{Vasilev2001}.
In case there are coupling, the theory still holds approximate 
results~\cite{barbero2011introduction}.
}

\mode<presentation>{
\begin{frame}
\frametitle{}
Inversion gives the segment stiffness of each segment:
\[
    \begin{bmatrix}
        N_x^i\\
        M_x^i\\
        N_{xs}^i\\
        M_{xs}^i 
    \end{bmatrix}
    =
    \begin{bmatrix}
        A_i & B_i & 0 & 0\\
        B_i & D_i & 0 & 0\\
        0 & 0 & F_i & C_i\\
        0 & 0 & C_i & L_i
    \end{bmatrix}
    \begin{bmatrix}
       \eta_x^i\\
       \chi_x^i\\
       \gamma_{xs}^i\\
       \chi_{xs}^i 
    \end{bmatrix}.
\]

For a homogeneous material they reduce to:
\begin{align*}
    A_i = E h = E \mathcal{A}_i,\\
    D_i = E h^3/12 = E I_s/ b_i ,\\
    F_i = G h = G\mathcal{A}_i/b_i,\\
    L_i = G h^3 /12 =\frac{1}{4} G J_R /b_i,
\end{align*}
where $\mathcal{A}_i$ is the segment area, $b_i$ its length, $J_R$ its polar 
moment of inertia and $I_s$ its second moment of area for rotations directed as
$s$.
\end{frame}
}

\mode<article>{
\item $A_i$ is the segment axial stiffness, $B_i$ is the coupling term between
axial forces and bending curvatures (zero for homogeneous materials)
}

\mode<presentation>{
\begin{frame}
\frametitle{Neutral axes of bending and torsion}
\begin{columns}[T]
\begin{column}{0.5\textwidth}
\alert{For each segment $i$}, these are found introducing:
\begin{itemize}
    \item bending: $e_b = B_i/A_i$
    \item torsion: $e_q = C_i/F_i$
\end{itemize}
The segment neutral bending axes are given by:
\[
    Y(s') = Y(s)- e_b \sin \alpha_i 
\]
\[
    Z(s') = Z(s)- e_b \sin \alpha_i 
\]
(replace $e_b$ with $e_q$ and $s'$ with $s''$ for torsion)
\end{column}
\begin{column}{0.5\textwidth}
\begin{figure}
\centering
\footnotesize
\def\svgwidth{\textwidth}
\input{../../images/thin_walled_beam_segment.pdf_tex}
\caption*{Geometry of a single segment.}
\end{figure}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
    \item Of course the neutral axes lie at $b/2$ due to symmetry of the wall.
    these quantities give the points where if we apply the resultant 
    normal or shear stresses we do not get a bending or torsion moment.

}

\subsection{Cross-section properties}
\mode<presentation>{
\begin{frame}
\frametitle{Axial stiffness}
All the section properties can be calculated by integration over the 
cross-section area $\mathcal{A}$. 
Assume a constant strain state $\eta_X^i=\eta_X$, all other strain being zero, 
then the axial force is given by
\[
    P_X = \int_\mathcal{A} \sigma_X \,\textrm{d}\mathcal{A} 
    = \int_{s'} \int\limits_{-h/2}^{h/2} \sigma_X \,\textrm{d}s'\textrm{d}r
    = \int_{s'} N_X(s) \,\textrm{d}s',
\]
and using the wall constitutive equations:
\[
    P_X = \eta_X \int_{s'} A_i \,\textrm{d}s' = \overline{EA}\, \eta_X.
\]
The symbol $\overline{EA}=\int_{s'} A_i \,\textrm{d}s'$ represents the axial 
stiffness of the beam;
for a section made of straight segments:
\[
    \overline{EA} = \sum_i A_i b_i.
\]
It depends on the segments stacking sequence but does not depend on the segments
relative position with respect to the $\lbrace Y,Z \rbrace$ reference system.
\end{frame}
}

\mode<article>{
\item the integral is performed along $s'$ to avoid inducing bending moments
}


\mode<presentation>{
\begin{frame}
\frametitle{Mechanical centre of gravity}
Point G can be obtained by moment equilibrium:
\[
    \begin{bmatrix}
        X\\Y_G\\Z_G
    \end{bmatrix}
    \times
    \begin{bmatrix}
        P_X\\0\\0
    \end{bmatrix}
    =
    \bigintss_{\mathcal{A}} \bm{X} \times 
            \begin{bmatrix}\sigma_X \\0\\0\end{bmatrix}
            \,\textrm{d}\mathcal{A} 
    =
    \bigintss_{s'} \bm{X} \times 
            \begin{bmatrix}\sigma_X \\0\\0\end{bmatrix}
            \,\textrm{d} s' \textrm{d} r
    =
    \bigintss_{s'} \begin{bmatrix} z(s') N_X^i\\ -y(s')N_X^i\\0\end{bmatrix}
        \,\textrm{d} s'
\]
By using the constitutive law:
\[
 Y_G = \frac{\overline{ES_Z}}{\overline{EA}},\quad 
 Z_G = \frac{\overline{ES_Y}}{\overline{EA}},
\]
where the mechanical static moments of the cross-section are given by
\begin{align*}
   &\overline{ES_Z} = \int_{s'} z(s') A_i  \,\textrm{d} s' 
                    = \sum_i z_i' A_i b_i\\
   &\overline{ES_Y} = \int_{s'} y(s') A_i  \,\textrm{d} s' 
                    = \sum_i y_i' A_i b_i,
\end{align*}
with $(y_i',z_i')$ being the coordinate of point P' at $s=b_i/2$.
\end{frame}
}

\mode<article>{
\item the mechanical centre of gravity can be determined by requiring that
the moment of the axial resultant $P_X$ applied in G is equal to the moment of
the stress distribution. $\bm{X}$ is the position vector of a point on the 
section.
In the last equations the second equality holds for a section made of straight 
segments only.
Of course for symmetric cross-sections it will lay on the symmetry axis, but 
also the lamination sequences are to be symmetric with respect to the symmetry 
axes of the section~\cite{Vasilev2001}.
}

\mode<presentation>{
\begin{frame}
\frametitle{Bending stiffness}
Can be obtained by a procedure analogous to the axial one. For each segment the
bending stiffnesses in the $\lbrace s',r' \rbrace$ reference system are:
\[
    \overline{EI_{s'}^i}=\overline{D_i} b_i=(D_i-e_b^2A_i)b_i,\qquad
    \overline{EI_{r'}^i}=A_i b_i^3/12,\qquad
    \overline{EI_{r's'}^i}=0.
\]
In the  $\lbrace Y',Z' \rbrace$ system:
\begin{align*}
    \overline{EI_{Y'}^i} &= \overline{EI_{s'}^i} \cos^2 \alpha_i + 
        \overline{EI_{r'}^i} +\sin^2 \alpha_i\\
    \overline{EI_{Z'}^i} &= \overline{EI_{s'}^i} \sin^2 \alpha_i + 
        \overline{EI_{r'}^i} +\cos^2 \alpha_i\\
    \overline{EI_{Y'Z'}^i} &= [\overline{EI_{r'}^i}-\overline{EI_{s'}^i}]
        \sin \alpha_i \cos \alpha_i 
\end{align*}

Upon integration over the cross-section area $\mathcal{A}$ and assuming a 
section made only of straight segments~\cite{massa1998strength}, in the 
$\lbrace Y_G,Z_G \rbrace$ reference system:
\end{frame}
}

\mode<article>{
\item in the $\lbrace s',r' \rbrace$ reference system the product of inertia 
vanishes as it is principal.
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{align*}
    \overline{EI_{Y_G}^i} &= \sum_i \overline{EI_{Y'}^i} + 
                            A_i b_i (z_i + e_b \cos \alpha_i)^2\\
    \overline{EI_{Z_G}^i} &= \sum_i \overline{EI_{Z'}^i} + 
                            A_i b_i (y_i - e_b \sin \alpha_i)^2\\
    \overline{EI_{Y_G Z_G}^i} &= \sum_i \overline{EI_{Y'Z'}^i} +
                            A_i b_i (z_i + e_b \cos \alpha_i)(y_i - e_b \sin 
                            \alpha_i)
\end{align*}
\begin{columns}[T,onlytextwidth]
\begin{column}{0.5\textwidth}
where $(y_i,z_i)$ are the coordinates of point $P'$, the segment centre 
($s=bi/2,r=0$).
The principal bending axes $\lbrace \psi,\zeta \rbrace$ are rotated w.r.t. to 
the $\lbrace Y_G,Z_G \rbrace$ reference system of the angle $\theta$ such that
\[
    \tan 2\theta = \frac{2 \overline{EI_{Y_G Z_G}^i}}
            {\overline{EI_{Z_G}^i}-\overline{EI_{Y_G}^i}}.
\]
\end{column}
\begin{column}{0.5\textwidth}
\begin{figure}
\centering
\footnotesize
\def\svgwidth{\textwidth}
\input{../../images/thin_walled_beam_segment.pdf_tex}
\end{figure}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item ricordasri di andare indietro a far vedere i punti sulla figura
}

\mode<presentation>{
\begin{frame}
\frametitle{Torsional stiffness}
The torsional stiffness $\overline{GJ_R}$ can be found by equating the work 
done by the torque $T$ and the shear actions acting on the 
cross-section~\cite{massa1998strength}:
\[
    \frac{1}{2} T l \beta = \frac{l}{2} \int_{s''} \gamma_{Xs} q^i + M_{Xs}^i 
\chi_{Xs}^i \textrm{d}s''
\]
where $l$ is the beam axis length, $q^i=\int_{-h/2}^{h/2} \tau_{Xs}\,
\textrm{d}r=N_{Xs}^i$ the shear flow and $\beta$ the torsion rate (such that 
the torsion angle is $\beta X$), which is independent of 
$X$~\cite{Vasilev2001}.
\begin{columns}[T,onlytextwidth]
\begin{column}{0.48\textwidth}
\begin{block}{Open sections}
    For open sections under pure torsion $q^i=0$; letting 
    $\overline{H_i}=H_i-e_q^2F_i$ it follows that
    \[
        \overline{GJ_R} = 4 \int_{s''} \overline{H_i} \textrm{d}s''
        = 4 \sum_i \overline{H_i} b_i.
    \]
\end{block}
\end{column}
\begin{column}{0.48\textwidth}
\begin{block}{Single cell closed sections}
    Under the assumption of constant shear distribution through the thickness 
    can be approximated by:
    \[
        \overline{GJ_R} = \frac{4\mathcal{A}_{s''}}{\sum_i b_i/F_i},
    \]
    where $\mathcal{A}_{s''}$ is the area enclosed by the contour described by 
    $s''$.
\end{block} 
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item here as with the standard De Saint Venant problem we have to differentiate
between closed and open sections. 
For open section under torsion you might remebre that the shear flow must be 
zero, due to the fact that the shear stresses must be equal and opposite when
moving through the wall thickness.
The second equality holds only for sections made of straight segments. For more 
complicate sections see~\cite{Vasilev2001}.
}

\mode<presentation>{
\begin{frame}
\frametitle{Shearing of open sections}
Consider a shear force acting along $\Psi$ in the principal bending system.The 
following replacement for the Jourawski’s formula can be used to evaluate the 
shear flow along the contour\footnote{Pay attention to the sign.}:
\[
    q_{\Psi} = 
        -\frac{\overline{ES_{\zeta}}V_{\Psi}}{\overline{EI_{\zeta}}k_{\zeta}}
\]
where $k_{\zeta}$ is a correction factor that can be taken equal to one for 
thin sections~\cite{barbero2011introduction,Barbero_1993}.
The mechanical static moment is given by:
\[
    \overline{ES_{\zeta}}(s) = \int\limits_0^s \Psi(s')A_i \,\textrm{d}s
            = \int \limits_0^s (\Psi(s)-e_b \sin \alpha_i)A_i \,\textrm{d}s.
\]
and the shear stiffness can be calculated as~\cite{barbero2011introduction}:
\[
    \overline{GA_{\zeta}} = \frac{\overline{EI_{\zeta}}^2}
        {\int_s \overline{ES_{\zeta}}^2(s) \frac{\textrm{d}s}{F_i} } 
\]
\end{frame}
}

\mode<article>{
\item the shear stiffness can be obtained equating the virtual work of the 
applied external forces and that of the internal stresses.
}

\mode<presentation>{
\begin{frame}
\frametitle{}
The integral can be evaluated as a sum if the segments are straight. Analogously
for a shear force along $\zeta$:
\[
    \overline{GA_{\Psi}} = \frac{\overline{EI_{\Psi}}^2}
        {\int_s \overline{ES_{\Psi}}^2(s) \frac{\textrm{d}s}{F_i} }.
\]
\begin{columns}[T,onlytextwidth]
\begin{column}{0.48\textwidth}
Let \textcolor{SlateBlue}{$R$} be the projection on $r$ of the position vector. 
Introducing the sectorial area 
\textcolor{OrangeRed}{$\omega=2\Gamma_G$}:

\[
    \textcolor{OrangeRed}{\omega} = \int_s \textcolor{SlateBlue}{R}(s'') \,
        \textcolor{blue}{\textrm{d}s}
    \]

the sectorial moments can be introduced as:
\begin{align*}
    \overline{ES\omega_{\zeta}} &= \int_s (\Psi(s)-e_b \sin \alpha^i_{\Psi}) 
        \omega(s) A_i \,\textrm{d}s\\
    \overline{ES\omega_{\Psi}} &= \int_s (\zeta(s)+e_b \cos \alpha^i_{\Psi}) 
        \omega(s) A_i \,\textrm{d}s
\end{align*}

where $\alpha^i_{\Psi}$ is the angle made by direction $s$ with direction 
$\Psi$.

\end{column}
\begin{column}{0.5\textwidth}
\centering
\footnotesize
\def\svgwidth{\textwidth}
\input{../../images/thin_walled_sectorial_area.pdf_tex}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item mah
}

\mode<presentation>{
\begin{frame}
\frametitle{}
The shear centre can then be found by
\begin{align*}
    &\zeta_C = -\frac{\overline{ES\omega_{\zeta}}}{k_{\zeta} 
        \overline{EI_{\zeta}}} \\
    &\Psi_C = \frac{\overline{ES\omega_{\Psi}}}{k_{\Psi} \overline{EI_{\Psi}}} 
\end{align*}
The shear centre is coincident with the twist centre and lies on symmetry axis.
Refer to~\cite{barbero2011introduction,massa1998strength} for shearing of 
closed sections.
\end{frame}
}

\mode<article>{
\item the shear center is the point such that shear forces applied there induce 
no torsion. The twist center is the point where the rotations vanish under 
torsion. We shall not deal with the problem of shearing in closed sections; the 
reader is referred 
}


\subsection{Beam deformations}

\mode<presentation>{
\begin{frame}
\frametitle{Beam deformations}
Once the equivalent stiffness is calculated following the procedure outlined 
before, the usual beam theory equations can be used to derive the distribution 
of the stress resultants along the beam axis and the displacement field if 
needed.
For example the following relationship holds:
\begin{align*}
    &\eta_X = \frac{P_X}{\overline{EA}}, \quad &\chi_{\Psi} = 
        \frac{H_{\Psi}}{\overline{EI_{\Psi}}}\\
    &\chi_{\zeta} = \frac{H_{\zeta}}{\overline{EI_{\zeta}}}, \quad
        &\beta = \frac{T}{\overline{GJ_{R}}}
\end{align*}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Segment deformations and stresses}
The axial strain in each segment is obtained 
as~\cite{massa1998strength,Vasilev2001}:
\[
    \eta_X^i = \frac{P_X}{\overline{EA}} +\zeta(s) 
        \frac{H_{\zeta}}{\overline{EI_{\zeta}}} - 
        \Psi(s)\frac{H_{\Psi}}{\overline{EI_{\Psi}}}.
\]
The bending curvature, which is unique for each segment, is obtained as:
\[
    \chi_X^i = \chi_{\Psi} \cos \alpha_{\Psi}^i+\chi_{\zeta} \sin 
        \alpha_{\Psi}^i.
\]

The torsion, which is the same for every segment, is obtained as 
\[
    \chi_{Xs}^i = 
    \begin{cases}
        - 2 T / \overline{GJ_R} \text{ open sections}\\
        - T / \overline{GJ_R} \text{ closed sections}
    \end{cases}
\]
The shear flow is the sum of the shear flows due to torsion and shear forces:
\[
    q^i(s) = q_T+q_{\Psi}^i(s)+q_{\zeta}^i(s),
\]
giving the mid-plane shear strain
\end{frame}

}

\mode<article>{
\item the minus signs comes from the various conventions
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\[
    \gamma_{Xs}^i =\frac{q^i(s)-C^i \chi_{Xs}^i}{F_i}.
\]
\end{frame}
}

\mode<article>{
\item nothing
}