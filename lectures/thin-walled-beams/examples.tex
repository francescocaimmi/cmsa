%kate: default-dictionary en_GB; 
\subsection{Cantilever with $\mathrm{I}$-section}
\mode<presentation>{
\begin{frame}
\frametitle{Problem}
\begin{columns}[T]
\begin{column}{0.7\textwidth}
A cantilever beam of length $l=300$ mm is subject to a tip shear force 
$V_Z=445$ N.
Both the flange and the web are made with a $[\pm30/0]_S$ lamination 
sequence, where the reference direction is the beam axis and angles are 
measured from the beam axis to the direction perpendicular to the beam axis and 
the segment thickness direction.
Verify the resistance of the beam~\cite{barbero2011introduction}.
\begin{block}{material}
    Kevlar-epoxy. Elastic properties: $E_{11}=76$ GPa, $E_{22}=5.56$ GPa, 
$G_{12}=2.3$ GPa,     $\nu_{21}=0.34$. Failure properties (MPa):
    $X^+ = 1380$, $X^- = 586$,$Y^+ = 34.5 $, $Y^- = 138$, $S = 44.1 $.
\end{block}

\end{column}
\begin{column}{0.3\textwidth}
    \centering
    \footnotesize
    \def\svgwidth{0.98\textwidth}
    \input{../../images/thin_walled_ibeam_geom.pdf_tex}
\end{column}
\end{columns}
Due to material and geometry symmetry, the mechanical centre of mass is 
coincident with the geometrical one at the crossing
of the two section symmetry axes.
\end{frame}
}

\mode<article>{
\item the example is simple as to the loading conditions, but the point here is
to be able to deal with laminated materials, not with beams in general.
}

\mode<presentation>{
\begin{frame}
\frametitle{Wall stiffness}
The wall compliance is the same for all segments since the lamination 
sequence is the same for each wall. By using the definition and exploiting the 
symmetry of the stacking sequence:
\[
    \begin{bmatrix}
        \num{1.47e-05} & 0 & 0 & 0\\
        0 & \num{1.27e-04} & 0 & 0\\
        0 & 0 & \num{5.84e-05} & 0\\
        0 & 0 & 0 & \num{2.50e-04}
    \end{bmatrix} 
\]
(base units are MPa and mm).\\
Inversion leads to 
\[
    \begin{bmatrix}
        \num{67.9e03} & 0 & 0 & 0\\
        0 & \num{7.85e03} & 0 & 0\\
        0 & 0 & \num{17.1e03} & 0\\
        0 & 0 & 0 & \num{3.99e03}
    \end{bmatrix} 
\]
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Bending stiffness}
For each wall $i$, the membrane-bending coupling stiffness $B_i$ is equal to 
zero $\Rightarrow$ the mechanical centre of gravity lies on the midline 
($e_b=0$).
Hence, in the $\lbrace s',r' \rbrace$ reference system the bending stiffness 
can be obtained as:
\begin{align*}
    &\overline{EI_{s'}^1}=(D_1-e_b^2A_1)b_1 = 
        \SI{98.1e3}{\mega\pascal\milli\metre^4},\\
    &\overline{EI_{r'}^1}=A_1 b_1^3/12 = 
        \SI{11.05e6}{\mega\pascal\milli\metre^4},\\
    &\overline{EI_{r's'}^1}=0,
\end{align*}
for the flanges (walls 1 and 3) and
\begin{align*}
    &\overline{EI_{s'}^2}=(D_2-e_b^2A_1)b_2 = 
        \SI{184e3}{\mega\pascal\milli\metre^4},\\
    &\overline{EI_{r'}^2}=A_2 b_2^3/12 = 
        \SI{72.9e6}{\mega\pascal\milli\metre^4}, \\
    &\overline{EI_{r's'}^2}=0,
\end{align*}    
for the web (wall number 2).
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
Rotation into the $\lbrace Y',Z' \rbrace$ system using:
\begin{align*}
    \overline{EI_{Y'}^i} &= \overline{EI_{s'}^i} \cos^2 \alpha_i + 
        \overline{EI_{r'}^i} +\sin^2 \alpha_i\\
    \overline{EI_{Z'}^i} &= \overline{EI_{s'}^i} \sin^2 \alpha_i + 
        \overline{EI_{r'}^i} +\cos^2 \alpha_i\\
    \overline{EI_{Y'Z'}^i} &= [\overline{EI_{r'}^i}-\overline{EI_{s'}^i}]
        \sin \alpha_i \cos \alpha_i 
\end{align*}
yields, for segments 1 or 3:
\begin{align*}
    &\overline{EI_{Y'}^1}= \SI{98.1e3}{\mega\pascal\milli\metre^4},\\
    &\overline{EI_{Z'}^1}= \SI{11.05e6}{\mega\pascal\milli\metre^4}, \quad 
        \overline{EI_{Z'Y'}^1}=0,
\end{align*}
which of course are the same as before (since $s'\parallel Y'$) and 
\begin{align*}
    &\overline{EI_{Y'}^{\,2}}= \SI{72.9e6}{\mega\pascal\milli\metre^4}\\
    &\overline{EI_{Z'}^{\,2}}= \SI{184e3}{\mega\pascal\milli\metre^4}, \quad 
        \overline{EI_{Z'Y'}^{\,2}}=0,
\end{align*}
for segment 2, which are the same as before but rotated of $90^\circ$ since $s' 
\perp Y' $.
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
By assembling in the $\lbrace Y_G, Z_G \rbrace$ reference system the bending 
stiffness against rotations along the $Y_G$ axis is finally obtained:
\[
    \overline{EI_{Y_{G}}} = \SI{3.06e8}{\mega\pascal\milli\metre^4}
\]
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Shear stiffness}
Need to evaluate the static moments: due to symmetry $\zeta \parallel Z_G$ 
and $\Psi \parallel Y_G$. Since the shear force is directed as $Z_G$, we need 
just to calculate 
\[
    \overline{ES_{\Psi}}(s) = \int\limits_0^s \zeta(s')A_i \,\text{d}s
\]
i.e.
\[
    \overline{ES_{Y_G}}(s) = \int \limits_0^s (Z_G(s) + e_b \cos \alpha_i)A_i 
    \,\text{d}s
\]
Need to verify the beam: useful to evaluate the shear flow 
$q(s) \equiv N_{Xs} = -\frac{V_Z \overline{ES_{Y_G}} }{\overline{EI_{Y_{G}}}}$.
Assume a unit correction factor.
Since this is a branched cross section it is more convenient to split 
integration in different ranges starting from a free edge where $q(s)=0$.
\end{frame}
}

\mode<article>{
\item having the integration ending where $q(s)$ is not equal to zero may cause 
a lot of problems
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{columns}[c,onlytextwidth]
    \begin{column}{0.4\textwidth}
    Contribution of first branch ($s_1$):
    \[
        \overline{ES_{Y_G}}(s_1) = A_1 \frac{t-h}{2} s_1
    \]
    corresponding to a shear flow
    \[
        \textcolor{blue}{q}(s_1) = - \textcolor{Green}{V_Z} 
            \frac{A_1 \frac{t-h}{2} s_1}{\overline{EI_{Y_{G}}}}
    \]
    such that 
    \[
        q(B/2) = \SI{7.29}{\newton\per\milli\metre}
    \]
    \end{column}
    \begin{column}{0.3\textwidth}
    \centering
    \footnotesize
    \def\svgwidth{0.98\textwidth}
    \input{../../images/thin_walled_ibeam_shear_stiffness.pdf_tex}
    \end{column}
    \begin{column}{0.3\textwidth}
    \centering
    \footnotesize
    \def\svgwidth{0.98\textwidth}
    \input{../../images/thin_walled_ibeam_shear_flow.pdf_tex}
    \end{column}
\end{columns}
\medskip 
For the other flanges the expression is the same, only the sign of $Z_G$ changes
for branches 3 and 4. 
In the web immediately below point A, the shear flow accumulates and gets equal 
to $2q(B/2)=14.5$ N/mm.
For the web, where $Z_G=c-s_3$ with $c = \frac{t-h}{2}$, the static moment 
increases as 
\[
    \overline{ES_{Y_G}}(s_3) = A_1 c B + A_2 \int_{0}^s Z_G 
                                \text{d}s =
                               A_1 c B  + A_2 \left(c s - \frac{s^2}{2} \right)
\] 
\end{frame}
    }

\mode<article>{
\item of course as usual it is an hell with signs minus and so on so kee 
attention to every move.
For the web, the static moment has the value 
}

% \mode<presentation>{
\begin{frame}
\frametitle{}
so that the shear flow increases parabolically along $S_3$ and at $s_3=(t-h)/2$ 
(i.e. $Z_G=0$) it results
\[
    q(s_3 =(t-h)/2)=-21.3 \text{ N/mm}.
\]
with the flow obviously directed as the applied load.
Calculation of the shear stiffness $\overline{GA_{Y_G}}$ of the cross section 
requires to evaluate 
$I = \int_s \overline{ES_{Y_G}}^2(s) \frac{\text{d}s}{F_i}$. 
For the first part of the flange $\overline{ES_{Y_G}}(s_1) = A_1 c s_1 
\Rightarrow$:
\[
    I_1 =  \int_{0}^{B/2} \frac{(A_1 c s_1)^2}{F_1} \text{d}s
        =  \frac{b^3 c^2 A_{1}^2}{24 F_1}
\]
For the web, where 
$\overline{ES_{Y_G}}(s_3) = A_1 c B  + A_2 \left(c s - \frac{s^2}{2} \right)$, 
the contribution to $I$ is given by 
\[
    I_3 = \frac{2 c^3 \left(2 A_2^2 c^2 +10 c A_1 B A_2 + 15 A_1^2 B^2 
                        \right)}{15 F_2}
\]
so that
\[
    I = 4 I_1 + I_3 = \SI{248.1e9}{\mega\pascal \milli\metre^6}
\]
\end{frame}
% }

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
Finally the shear stiffness can be calculated:
\[
    \overline{GA_{Y_G}} = \frac{\overline{EI_{Y_G}}^2}{I} = 
                        \SI{0.378d6}{\mega\pascal\milli\metre\squared}
\]
If the shear stiffness were approximated as the product of the laminate 
equivalent shear modulus and of the geometrical area, it would have been 
estimated as \SI{0.428d6}{\mega\pascal\milli\metre\squared}, overestimating it 
by 13\%.
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Evaluation of the layer stresses}
The most critical section is the built in one where there act a shear force
$V_Z$ and a bending moment $H_Y=-V_Z l = \SI{-133.5e3}{\newton\milli\metre}$.
Stress evaluation must be conducted at the critical points A, A', B, C, C'.
\begin{columns}[onlytextwidth,T]
    \begin{column}{0.7\textwidth}
        For example for point A, the membrane strains are given by
        \[
            \eta_X^1(\text{A}) = Z_G(\text{A}) \frac{H_{Y}}{\overline{EI_{Y_G}}}
        \]
        with $Z_G(\text{A}) = (t-h)/2$ giving 
        $\eta_X^1(A)=\num{-5.11e-3}$.
        The bending curvature for a segment is obtained from
        \[
            \chi_X^1 = \chi_{Y_G} \cos \alpha_{\Psi}^1,
        \]
        so that $\chi_X^1(A)=\num{-0.436e-3}$. The midplane shear deformations
        are given by 
        \[
        \gamma_{Xs}^i = q_{Z_G}^i/F^i
        \]
        so that $\gamma_{XY}^1(A)=\num{422e-6}$
    \end{column}
    \begin{column}{0.3\textwidth}
        \begin{figure}
        \centering
        \footnotesize
        \def\svgwidth{0.98\textwidth}
        \input{../../images/thin_walled_ibeam_critical_points.pdf_tex}
        \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
Using the reduced constitutive equations 
\[
    \begin{bmatrix}
       \eta_x^i\\
       \chi_x^i\\
       \gamma_{xs}^i\\
       \chi_{xs}^i 
    \end{bmatrix}
    =
    \begin{bmatrix}
        \alpha_{11} & \beta_{11} & 0 & 0\\
        \beta_{11} & \delta_{11} & 0 & 0\\
        0 & 0 & \alpha_{66} & \beta_{66}\\
        0 & 0 & \beta_{66} & \delta_{66}
    \end{bmatrix}
    \begin{bmatrix}
        N_x^i\\
        M_x^i\\
        N_{xs}^i\\
        M_{xs}^i 
    \end{bmatrix}
\]
the generalised stresses for each segment can be evaluated. Summary of the 
results is given in the table.
\begin{center}
\scriptsize
\begin{tabular}{l|cccccc}
%line 1
\hline
Point&$\eta_X$&$\chi_X$&$\gamma_{Xs}$&$N_X 
[\si{\newton\per\milli\metre}]$&$N_{XY} [\si{\newton\per\milli\metre}]$&$M_X 
[\si{\newton\milli\metre\per\milli\metre}]$\\
\hline
A&\num{-5.11e-03}&\num{-4.36e-04}&\num{-4.22e-04}&\num{-3.47e+02}&\num{-7.23e+00
}&\num{-3.42e+00}\\
A'&\num{-5.11e-03}&\num{0}&\num{-8.45e-04}&\num{-3.47e+02}&\num{-1.45e+01}&\num{
0}\\
B&\num{0}&\num{0}&\num{-1.24e-03}&\num{0}&\num{-2.12e+01}&\num{0}\\
C'&\num{5.11e-03}&\num{0}&\num{-8.45e-04}&\num{3.47e+02}&\num{-1.45e+01}&\num{0}
\\
C&\num{5.11e-03}&\num{-4.36e-04}&\num{-4.22e-04}&\num{3.47e+02}&\num{-7.23e+00}
&\num{-3.42e+00}\\
\hline
\end{tabular}
\end{center}
This beam is relatively thin walled and the bending stresses 
$M_X$ are negligible with respect to the membrane ones, so a theory neglecting 
bending stresses at the wall level (such as the one presented 
in~\cite[Chap. 10]{Vasilev2001}) would have been perfectly adequate.
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
Given the generalised deformations, the stresses in the layer are obtained by 
the standard procedure and, by the maximum stress criterion, the failure index
can be calculated.
The results are:
\begin{center}
\small
\begin{tabular}{l|ccc}
\hline
Point&max F [-]&max F layer&max F component\\
\hline
A&\num{3.21}&\num{5}&\num{12}\\
A'&\num{3.15}&\num{0}&\num{12}\\
B&\num{0.89}&\num{1}&\num{2}\\
C'&\num{6.90}&\num{1}&\num{2}\\
C&\num{6.87}&\num{1}&\num{2}\\
\hline
\end{tabular}
\end{center}
The beam therefore would fail due to transverse tension in the layers at 
\SI{30}{\degree} where these stresses are positive, or due to shear . 
A Jupyter notebook with the calculations leading to the results can be obtained 
by clicking on the pin\attachfile{I-beam.ipynb}.
\end{frame}
}
\mode<article>{
\item
}