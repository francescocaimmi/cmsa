%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Introduction}
\begin{columns}[T]
\begin{column}{0.5\textwidth}
    Purpose: determine the 
    \begin{itemize}
        \item tensile
        \item compression
        \item shear 
    \end{itemize}
    behaviour of composites in the PMRS.
    Polymer matrix composites to a first approximation are linear elastic up to 
    failure $\Rightarrow$ measure:
    \begin{itemize}
        \item elastic constants 
        \item failure stress/strain
    \end{itemize}
\end{column}
\begin{column}{0.5\textwidth}
    \begin{block}{Normative reference}
        \begin{itemize}
        \item tensile tests: ISO 527-4 and ASTM D 5083
        \item compression tests:
        \begin{itemize}
            \item end loading: ASTM D 695
            \item shear loading: ASTM D 3410 and ISO 14126 
        \end{itemize}
        \item tensile test on $\pm45°$ laminates (for shear properties): 
        ASTM D 3518, ISO 14129 
        \end{itemize}
        For fabric reinforced composites look also at ASTM D 6856. Italian 
        speaking people may also be interested in the CNR-DT-200.
    \end{block}
\end{column}

\end{columns}
\end{frame}
}

\mode<article>{
\item We already know that knowing what happens in the PMRS we can deduce what 
happens whatever the orientation
}

\mode<presentation>{
\begin{frame}
\frametitle{Tensile tests: samples}
\begin{figure}
\includegraphics[width=0.8\textwidth]{iso_527_sample_shape.png}
\caption*{Typical sample shape for tensile tests. From ISO 527.}
\end{figure}
\end{frame}
}

\mode<article>{
\item The thickness of a single ply is usually very small, form 0.1 mm to 0.5 
mm depending on the reinforcement type. 
So the specimen is usually a laminate itself, made by stacking many 
unidirectional laminae in order to reach a sensible thickness.
}

\mode<presentation>{
\begin{frame}
\frametitle{Tensile tests}
\begin{itemize}
    \item testing the material along the fiber direction and along the 
    perpendicular direction can be used to easily measure $E_1$, $E_2$ due 
    to lack of coupling between shear and normal components in the PMRS.
    \item $\nu_{12}$ or $\nu_{21}$  can be measured by measuring the transverse 
    strain (strain gages).
    \item can be used to evaluate the material resistance (failure stress) in 
    the PMRS.
\end{itemize}

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Compression tests: fixtures}
\begin{itemize}
    \item the easiest routes to produce laminates result in thin 
    laminate $\Rightarrow$ test fixtures must avoid early failure by buckling
\end{itemize}
\begin{columns}[T]
   \begin{column}{0.6\textwidth}
        \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]
        {../../images/compression_shear_load.png}
        \caption*{shear-loaded compression fixture as per ASTM D 3410}
    \end{figure}
   \end{column}
   \begin{column}{0.4\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{../../images/compression_end_load.png}
    % compression_end_load.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \caption*{end-loaded compression fixture as per ASTM D 695}
    \end{figure}
   \end{column}

\end{columns}

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Off-axis tensile tests}
PMRS stress state
\[
     \sigma_1 = \sigma_X \cos^2 \theta, \sigma_2 = \sigma_X \sin^2 \theta,
     \sigma_{12} = -\sigma_X \sin \theta \cos \theta
\]
\vspace{-1em}
\begin{columns}[T]
\begin{column}{0.25\textwidth}
\begin{center}
\def\svgwidth{0.5\textwidth}
\input{../../images/ud_off_axis_test.pdf_tex}    
\end{center}
\end{column}
\begin{column}{0.75\textwidth}
\begin{tikzpicture}
        \begin{axis}[xlabel={$\theta$ [deg]}, ylabel = {Normalised Deformation},
                    xmin=0,xmax=90,
                    xtick={0,15,30,45,60,75,90},
                    width =\textwidth, height = 0.5\textwidth]
            %e_1/e_X 
            \addplot[no markers,thick] 
                    table [x index=0, y index=1]
                    {off_axis_test_data.txt};
            \node at (axis cs:12,0.9) {$\epsilon_1/\epsilon_X$};
            %e_2/e_Y 
            \addplot[no markers,thick,red] 
                    table [x index=0, y index=2]
                    {off_axis_test_data.txt};
            \node at (axis cs:82,1.2) 
            {\textcolor{red}{$\epsilon_2/\epsilon_X$}};
            %gamma_12/e_X
            \addplot[no markers,thick,blue] 
                    table [x index=0, y index=3]
                    {off_axis_test_data.txt};
            \node at (axis cs:37,2) 
            {\textcolor{blue}{$-\gamma_{12}/\epsilon_X$}};
        \end{axis}

    \end{tikzpicture}
\end{column}
\end{columns}
\begin{itemize}
    \item useful to evaluate the resistance dependence on orientation.
    \item given $E_1,E_2,\nu_{12}$ can be used to find $G_{12}$: best at 
    about 10°.
    \item actual grips restrain deformation so corrections are needed                
    \cite{Carlsson1987,Vasilev2001} since $\eta_{X,s}\neq0$.
\end{itemize}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{$\pm45°$ laminate tensile test}
Use laminate made of two UD laminae with fibres oriented at $+45^\circ$ and 
$-45^\circ$ w.r.t. the load. Similar to a biaxial reinforcement: 
$\eta_{x,s}(45^\circ)\simeq0,\Rightarrow$ can be used to evaluate $G_{12}$. 
\begin{columns}[T]
\begin{column}{0.35\textwidth}
    \includegraphics[width=\textwidth]{pm45_coupon.JPG}
\end{column}
\begin{column}{0.65\textwidth}
    \begin{itemize}
        \item to get the shear strain $\gamma_{12}$ one needs to measure the
        strain in two directions
        \item usually $X$ and $Y$ so that
        \[
            \gamma_{12}=\epsilon_X-\epsilon_Y, \tau_{12} = \sigma_X/2,
        \]
        hence
        \[
            G_{12} = \frac{\tau_{12}}{\gamma_{12}} = 
            \frac{\sigma_{X}}{2(\epsilon_X-\epsilon_Y)} 
        \]
        or
        \[
             G_{12} = \frac{E_X}{2(1+\nu_{XY})}
        \]
    \end{itemize}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item We will soon be able to prove that this assumption is actually correct.
}

\mode<presentation>{
\begin{frame}
\frametitle{Stress-strain curves: an example}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
    \begin{figure}
    \centering
    \begin{tikzpicture}
        \begin{axis}[xlabel={$\epsilon_1$ [\%]}, ylabel={$\sigma_1$ [MPa]},
                     width = \textwidth, height = 0.7\textheight,
                     xmin=0,ymin=0
                    ]
            \addplot[black,thick] coordinates {(0,0) (2.8421,2700)};
            \addplot[blue,thick] coordinates {(0,0) (0.4,380)};
            \node at (axis cs:2,1500)  {$\sigma_1^+$};  
            \node at (axis cs:0.6,200)  {$\textcolor{blue}{\sigma_1^-}$};
        \end{axis}
    \end{tikzpicture}
    \caption*{tension-compression along fibre direction}
    \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
    \begin{figure}
    \centering
    \begin{tikzpicture}
        \begin{axis}[xlabel={$\epsilon_2,\gamma_{12}$ [\%]}, 
                    ylabel={$\sigma_2,\tau_{12}$ [MPa]},
                     width = \textwidth, height = 0.7\textheight,
                     xmin=0,ymin=0
                    ]       
            \addplot[blue,thick,smooth] coordinates 
                {(0,0) (2,102) (2.4,118) (2.9,125)};
            \addplot[red,thick,smooth] coordinates 
                {(0,0) (1,20) (2,30) (2.5,31)};
            \addplot[black,thick] coordinates {(0,0) (0.686274509804,35)};
            \node at (axis cs:0.8,30)  {$\sigma_2^+$};  
            \node at (axis cs:1.5,100)  {$\textcolor{blue}{\sigma_2^-}$};
            \node at (axis cs:2.2,38)  {$\textcolor{red}{\tau_{12}}$};
        \end{axis}
    \end{tikzpicture}
    \caption*{transverse tension-compression and shear}
    \end{figure}        
    \end{column}
\end{columns}
\begin{center}
{\footnotesize data for an aramid-epoxy composite, from~\cite{Vasilev2001}}
\end{center}
\end{frame}
}

\mode<article>{
\item nothing
}