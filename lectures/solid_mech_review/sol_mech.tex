%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Preliminaries\footnote{
Reference material: books by Lubliner and 
Papadopoulos~\cite{Lubliner2014} or by Reddy 
\cite{reddy2003mechanics}. Italian speaking people may 
also consult~\cite{dell2010meccanica}
}}
\begin{center}
\includegraphics[width=0.5\textwidth,keepaspectratio=true]
{Displacement_of_a_continuum.png}
    % Displacement_of_a_continuum.png: 0x0 pixel, 0dpi, 0.00x0.00 cm, bb=
\end{center}

continuum: a “body” which can be subdivided indefinitely and in the limit is 
locally homogeneous.
For deformable continua the \textcolor{red}{relative distances} can change via 
a motion $\chi$, described by the displacement field
\[
    \bm{s}=\bm{x}-\bm{X}
\]
\end{frame}
} 
\mode<article>{
\item nothing
}
\mode<presentation>{
\begin{frame}
\frametitle{Postulates of continuum mechanics}
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{center}
        \includegraphics[width=0.65\textwidth,keepaspectratio=true]
        {postulates_200dpi.png}
        \newline
        \includegraphics[width=0.45\textwidth,keepaspectratio=true]
        {split_continuum_200dpi.png}
        \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{enumerate}
            \item adding a constraint does not alter equilibrium.
            Hence by adding a rigid body constraint we deduce:
            If the body is in equilibrium then
            $\textcolor{red}{\bm{R}}=\bm{0} $ and 
            $\textcolor{red}{\bm{M}}=\bm{0}$
            \item the same holds for every subdivision of the continuum, 
            even if indefinitely small
            \item the actions exchanged between every two subdivisions of the 
            continuum are equal and opposite (Newton’s third law)
        \end{enumerate}
    \end{column}
\end{columns}
\end{frame}
}
\mode<article>{
\item the second and third postulate are very important: they allows us to 
    explore what happens inside the material by performing arbitrary cuts 
    through the volume of the body and studying the forces exchanged by 
    different parts of our continuum, which otherwise would be unknown to us.
}

\mode<presentation>{
\begin{frame}
\frametitle{Stress (tractions)}
\begin{columns}
    \begin{column}{0.45\textwidth}
        \begin{center}
        \includegraphics[width=0.95\textwidth,keepaspectratio=true]
        {stress_definition_200dpi.png}
        \end{center}
    \end{column}
    \begin{column}{0.55\textwidth}
        \begin{itemize}
            \item stress vector $\bm{T}(\bm{x})$: a surface action whose 
            resultant is $\textcolor{red}{\bm{R}}$, such that
            \[
            R_i(S_{\sigma})= \iint\limits_{S_{\sigma}} T_i(\bm{x}) 
            \textrm{d}\bm{S_{\sigma}} = \iint\limits_{S_{\sigma}} 
            T_i(\bm{x})n_i\, \textrm{d}x \textrm{d}y
            \]
            \item it depends on the position $\bm{x}$
            \item if $S_{\sigma}$ is varied or a subset is considered, $\bm{R}$
            varies in such a way that one can write
            \[
                \bm{T}(\bm{x}) = \lim_{S_{\sigma} \to 0} 
                \frac{\delta\bm{R}}{\delta S_{\sigma}}
            \]
            \item Cauchy's solids: no stress analogue for $\bm{M}$
            \[
                \lim_{S_{\sigma} \to 0} 
                \frac{\delta\bm{M}}{\delta S_{\sigma}} = \bm{0}
            \]
        \end{itemize}
    \end{column}
\end{columns}
\end{frame}
}
            
\mode<article>{
\item Let now turn our attention to a continuum body on which there are some 
external 
actions whose resultants are M and R, acting over a surface Ssigma and balanced 
by some reactions acting over the surface Su where, for instance in some way we 
force the displacements to assume some prescribed value. We call stress the 
actions point-wise acting over some area whose resultant is R so that the stress 
is defined by this integral relation. R is a resultant so it does not depend on 
the position, i.e. on X. It depends only on the extension of the area we are 
considering to calculate itself. In this sense, if we take ever decreasing areas 
and take a sort of derivative of the resultant force what is left is exactly the 
stress. 

As to the resultant moment, in what are called Cauchy solids by hypothesis there 
is no analogous of the stress: there are no moments per unit area acting. The 
resultant moment is thus given only by the moment of the stress distribution 
about some pole. Continua with moments per unit area acting are called 
Micropolar or Cosserat Continua and can be used to model to some extent the 
microstructure in some kinds of solid, but we shall not deal with them in this 
course.

Now, as the stress are defined by this integral relationship, it is rather clear 
that to the concept of stress a normal vector is in some sense always attached: 
the normal to a surface where the stresses are defined. On the external surface 
of a body everything is rather smooth, as there is no possible confusion about 
what the normal is, at least as long as the boundary is well behaved from a 
mathematical viewpoint.
}      

\mode<presentation>{
\begin{frame}
\frametitle{Stresses inside bodies}
To investigate the stress state inside a body we can use the third postulate and
simply cut it \ldots
\begin{center}
    \includegraphics[width=\textwidth]{cube_stress_vector_orientations.png}
    % cube_stress_vector_orientations.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
\end{center}
\ldots but inside bodies $\bm{T}=\bm{T}(\bm{x},\bm{n})$: a function of both 
\textcolor{red}{position} and
of the normal \textcolor{red}{$\bm{n}$}
\begin{columns}
 \begin{column}{0.45\textwidth}
    \begin{center}
    \input{../../images/direction_cosine}
        % direction_cosine.tex
    \end{center}
  \end{column}
  \begin{column}{0.55\textwidth}
    \begin{block}{normals}
        can be specified using direction cosines
        \[
          \bm{n} = \left[ \cos \alpha, \, \cos \beta, \, \cos \gamma \right]  
        \]
        where e.g. 
        \[
            \cos \alpha = \frac{\bm{n.e_x}}{\left| \bm{n} \right|}
        \]

    \end{block}
  \end{column}
\end{columns}



\end{frame}}

\mode<article>{
\item Now, let’s think of what’s going on inside the body, rather than on the 
outside, 
which is much more interesting as we cannot directly observe that, and it would 
be pretty cool to be able to know what’s happening there. We may in fact  
imagine that the actions inside the body are the cause for things such as 
fracture. To do this we can imagine to cut the body with a plane, or even a more 
complicate surface, and use the postulates stated before to study the stress 
state there. In fact we can try to determine form the external actions the 
stress inside the body by using equilibrium and newton’s third law by 
considering different succession of cuts that describe the whole body.

But inside the body… we can define an indefinite number of normals for each 
point or, which is the same, cut the body with whatever arbitrary plane. 
Point is that for each plane we chose, we would end up with a 
different stress state, as a simple example can show. (Example of the strip)

We would need to know an infinite number of stress vectors to carachterize the 
stress state. This is not very practical.
}

\mode<presentation>{
\begin{frame}
\frametitle{Cauchy’s stress theorem}
\begin{columns}
 \begin{column}{0.35\textwidth}
    \def\svgwidth{\textwidth}
    \input{../../images/Cauchy_tetrahedron.pdf_tex}  
 \end{column}
 \begin{column}{0.65\textwidth}
    \begin{itemize}
        \item positive stress component: has the same verse as the corresponding
        coordinate system axis
        \item for every point $\bm{x}$, force equilibrium requires
        \[
            \bm{T}(\bm{n}) \textrm{d}A = \bm{T}(\bm{e_x})\textrm{d}A_x + 
            \bm{T}(\bm{e_y})\textrm{d}A_y +\bm{T}(\bm{e_z})\textrm{d}A_z 
        \]
        \item project the area $\textrm{d}A$ to get
        \[
            \textrm{d} A_i = n_i \textrm{d}A 
        \]
        \item let
        \[
          \bm{T}(\bm{e_x})= \sigma_{xx} \bm{e_x} + \sigma_{xy} \bm{e_y} + 
            \sigma_{xz} \bm{e_z}
        \]
        \begin{itemize}
            \item first index:  normal to the surface ($\bm{e_i}$)
            \item second index: component direction
        \end{itemize}
    \end{itemize}

 \end{column}
\end{columns}

\end{frame}

}

\mode<article>{
    \item So $\sigma_{12}$ is the component of a stress vector acting on a 
    plane with normal 1 along direction 2.
}

\mode<presentation>{
\begin{frame}
\frametitle{Cauchy’s stress theorem (cont.)}
\begin{columns}
 \begin{column}{0.35\textwidth}
    \def\svgwidth{\textwidth}
    \input{../../images/Cauchy_tetrahedron.pdf_tex}  
 \end{column}
 \begin{column}{0.65\textwidth}
    \begin{itemize}
        \item it follows that 
        \[
        \bm{T}(\bm{n}) =  
        \begin{bmatrix}
        \sigma_{nx}\\ \sigma_{ny}\\ \sigma_{nz}
        \end{bmatrix} 
        =
        \begin{bmatrix}
        \sigma_{xx} n_x + \sigma_{yx} n_y + \sigma_{zx} n_z \\ 
        \sigma_{xy} n_x + \sigma_{yy} n_y + \sigma_{zy} n_z \\
        \sigma_{xz} n_x + \sigma_{yz} n_y + \sigma_{zz} n_z \\
        \end{bmatrix} 
        \]
        \item $\bm{n}$ arbitrary $\Rightarrow$ only three stress vectors are 
        needed to describe a general stress state
        \item in compact form
        \[
           \bm{T}(\bm{n}) = \bm{n}.\bm{\sigma} 
        \]
    \end{itemize}
 \end{column}

\end{columns}
 \[
     \bm{\sigma} = 
     \begin{bmatrix}
     \sigma_{xx} & \sigma_{xy} & \sigma_{xz} \\
     \sigma_{yx} & \sigma_{yy} & \sigma_{yz} \\
     \sigma_{zx} & \sigma_{zy} & \sigma_{zz}
     \end{bmatrix}
 \]
  is a second order tensor called \textcolor{red}{Cauchy's stress tensor}
\end{frame}
}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Cauchy stress tensor}
\begin{columns}
    \begin{column}{0.4\textwidth}
        \def\svgwidth{\textwidth}
        \input{../../images/Components_stress_tensor_cartesian_latex.pdf_tex}  
    \end{column}
    \begin{column}{0.6\textwidth}
    \begin{itemize}
    \item terms on the principal diagonal are called normal stresses; off 
    diagonal terms are called shear stresses and often written also as 
    $\tau_{ij}$
     \[
     \bm{\sigma} = 
     \begin{bmatrix}
     \sigma_{xx} & \tau_{xy} & \tau_{xz} \\
     \tau_{yx} & \sigma_{yy} & \tau_{yz} \\
     \tau_{zx} & \tau_{zy} & \sigma_{zz}
     \end{bmatrix}
    \]
     \item moment equilibrium requires that $\bm{\sigma}$ be 
     \textcolor{red}{symmetric}: 
    $\tau_{ij}=\tau_{ji}$
    \end{itemize}
    \end{column}
\end{columns}
\end{frame}

}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Change of basis}
\begin{columns}
    \begin{column}{0.55\textwidth}
      \def\svgwidth{1.05\textwidth}
      \input{../../images/Components_stress_tensor_transformation_latex.pdf_tex}
    \end{column}
    \begin{column}{0.45\textwidth}
        \begin{itemize}
            \item change the reference system from ($x,y,z$) $\to$ 
            ($x',y',z'$): basis vectors transform according to
            \[
                \bm{e'}_i=\bm{\textcolor{blue}{A}.e_i}
            \]
            \item $\textcolor{blue}{\bm{A}}$ is the direction cosines matrix:
            \[
                \textcolor{blue}{A_{ij}}
                =\cos(i, j'), \, i,j \text{ in } \{ 
                x,y,z \}
            \]
            \item stress tensor invariance requires that its\ 
            components tranform according to
            \[
                \bm{\sigma'}=\bm{A}^T\bm{.\sigma.A}
            \]
        \end{itemize}

    \end{column}
\end{columns}
\end{frame}
}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Principal stresses}
The eigenvalues of the stress tensor, i.e. the solutions $\lambda$ of the 
equation
\begin{align*}
    & \det \left( \bm{\sigma}-\lambda\bm{I} \right) = \\
    =& \lambda^3 - J_I \lambda^2 + J_{II} \lambda -J_{III} = 0    
\end{align*}

    
are denoted as $\sigma_I,\sigma_{II},\sigma_{III}$ and called 
\textcolor{red}{principal stresses}.
\begin{itemize}
    
    \item the coefficients of the characteristic polynomial $J_i$ 
    \begin{align*}
        J_I &= \sigma_{xx}+\sigma_{yy}+\sigma_{zz},\\
        J_{II}&=\sigma_{xx}\sigma_{yy}+\sigma_{yy}\sigma_{zz}+
        \sigma_{xx}\sigma_{zz}
        -\tau_{xy}\tau_{yx}-\tau_{xz}\tau_{zx}-\tau_{zy}\tau_{yz},\\
        J_{III} &= \det (\bm{\sigma}),
    \end{align*}
    
    are \textcolor{red}{invariant} w.r.t. the coordinate system
    
    \item $\bm{\sigma}$ is a symmetric tensor over the real field 
        \begin{itemize}
            \item[$\Rightarrow$] the principal stresses are real
            \item[$\Rightarrow$] can be diagonalised, i.e. there is a basis 
            such that
            $\bm{\sigma}=\text{diag}(\sigma_I,\sigma_{II},\sigma_{III})$
        \end{itemize}
\end{itemize}

\end{frame}
}

\mode<article>{
We shall now recall some basic linear algebra results applied to the stress 
tensor
}
\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{columns}
    \begin{column}{0.52\textwidth}
      \def\svgwidth{\textwidth}
      \input{../../images/principal_stresses_latex.pdf_tex}
    \end{column}
    \begin{column}{0.48\textwidth}
        \begin{itemize}
            \item the principal basis can be obtained from the eigenvectors 
            $\bm{\lambda_i}$, i.e. by the solutions of the equations
            \[
                \left( \bm{\sigma}-\sigma_{i} \bm{I} \right).\bm{\lambda_i} = 
                \bm{0}, i \text{ in } \{I,II,III\}
            \]
            by forming a matrix $\Lambda$ with the  eigenvectors as 
            its columns. It allows the transformation
            \[
              \text{diag}(\sigma_I,\sigma_{II},\sigma_{III})=
              \bm{\Lambda}^T\bm{.\sigma.\Lambda}
            \]
            \item in the principal basis there are \textcolor{red}{no shear 
            stresses}
            acting
        \end{itemize}

    \end{column}
\end{columns}
\end{frame}
}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Hydrostatic and deviatoric decomposition}
Let a material point be under hydrostatic pressure $\overline{p}$
\begin{itemize}
    \item[$\Rightarrow$] the stress state is such that 
    $\overline{p}=\sigma_I=\sigma_{II}=\sigma_{III}$; the first stress invariant 
    is $J_1=3\overline{p}$
    \item[$\Rightarrow$] the stress tensor (in any basis) is given by
    $\sigma=\text{diag}(\overline{p},\overline{p},\overline{p})$
\end{itemize}
by extension 
\[
    p=J_1/3
\]
is called the \textcolor{red}{hydrostatic pressure} 
and $p\bm{I}$ the hydrostatic component the stress tensor while
\[
    \bm{d} = \bm{\sigma}-p\bm{I}
\]
is called the \textcolor{red}{deviatoric} stress tensor.

\end{frame}
}

\mode<article>{
We can separate the stress into a deviatoric and an 
hydrostatic component; the name comes from the fact that a material element 
subject to uniform pressure, such as those in static fluids, is subject to such 
a stress state. 
These are associated with shape and volumetric  changes 
respectively, at least in isotropic solids.
Anyway, these concepts do not find wide application in the realm of composite 
materials
}

\mode<presentation>{
\begin{frame}
\frametitle{Plane stress state}
\begin{columns}[T]
\begin{column}{0.35\textwidth}
    \def\svgwidth{\textwidth}
    \input{../../images/plane_stress.pdf_tex}  
\end{column}
\begin{column}{0.65\textwidth}
    \begin{itemize}
    \item if one of the principal stresses is zero, since
    \[
        \bm{T}(\bm{n}) = \bm{\sigma}.\bm{n} = 
        \begin{bmatrix}
        \sigma_{I} & 0 & 0 \\
        0 & \sigma_{II} & 0 \\
        0 & 0 & \sigma_{III}
     \end{bmatrix}.\bm{n}
    \]
    $\Rightarrow \bm{T}$ must lie in a plane perpendicular to the 
    corresponding principal direction. This is called \textcolor{red}{plane 
    stress}. 
    \item important for thin bodies (laminae): through the thickness
    stress often negligible
    \item general form of the stress tensor ($\sigma_{III}=0$) 
    \[
      \bm{\sigma} = 
     \begin{bmatrix}
     \sigma_{xx} & \tau_{xy} & 0 \\
     \tau_{yx} & \sigma_{yy} & 0 \\
     0 & 0 & 0
     \end{bmatrix}
    \]
\end{itemize}
\end{column}
\end{columns}
\end{frame}

}
\mode<article>{
In this case only three stress components are needed to describe the stress 
state
}
\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{columns}[T]
\begin{column}{0.35\textwidth}
    \def\svgwidth{\textwidth}
    \input{../../images/stress_decomposition.pdf_tex}  
\end{column}
\begin{column}{0.65\textwidth}
    \begin{itemize}
    \item the stress vector can be decomposed into a normal component 
    $\sigma\bm{n}$ and into a tangential component $\bm{t}\perp\bm{n}$
    \item it can be shown that, at varying $\bm{n}$
    \begin{itemize}
        \item the extremum values of the normal component are the principal 
        stresses
        \item the maximum values of the shear components are found at $\pm\pi/4$
        w.r.t. the principal directions
    \end{itemize}

\end{itemize}
\end{column}
\end{columns}
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Mohr's Circle}
a graphical representation of the plane stress state (combining three circles 
one can get a representation for 3D stress field)
\newline
\def\svgwidth{\textwidth}
\input{../../images/mohr_circle.pdf_tex}  
\end{frame}
}
\mode<article>{
Cahuchy rule is the basis for the construction of Mohr's circle, that allows an 
easy graphic determination of plane stresses on every direction, besides 
principal stresses and directions. 
Recall that the conventions for drawing Mohr's circle are strange because the 
shear stresses are assumed to be positive when they induce a clockwork rotation 
of the material, which is the opposite of what happens with right-handed 
reference systems.
}

\mode<presentation>{
\begin{frame}
\frametitle{Equations of motion}
Let $\bm{b}$ be a force per unit volume acting on the body and $\rho$ its 
density. The equation of motion for each material point $\bm{x}$ is given by
\[
 \nabla \bm{.\sigma}+\bm{b}=\rho \frac{d^{\,2}\bm{s}}{dt^2}.
\]
Under quasi static conditions and neglecting body forces, it simply states that 
the stress tensor is incompressible:
    \[
        \nabla \bm{.\sigma} = 
        \begin{bmatrix}
         \frac{\partial\sigma_{xx}}{\partial x} +
         \frac{\partial\sigma_{xy}}{\partial y} +
         \frac{\partial\sigma_{xz}}{\partial z} \\[0.3em]
         \frac{\partial\sigma_{yx}}{\partial x} +
         \frac{\partial\sigma_{yy}}{\partial y} +
         \frac{\partial\sigma_{yz}}{\partial z} \\[0.3em]
         \frac{\partial\sigma_{zx}}{\partial x} +
         \frac{\partial\sigma_{zy}}{\partial y} +
         \frac{\partial\sigma_{zz}}{\partial z} 
        \end{bmatrix}
        =
        \begin{bmatrix}
         0\\[0.3em]0\\[0.3em]0 
        \end{bmatrix}
    \]
\end{frame}
}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Deformation}
\begin{center}
    
\includegraphics[width=0.8\textwidth,keepaspectratio=true]
{deformation_large.png}
    % deformation_large.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
\end{center}

\end{frame}
}

\mode<article>{
\item A motion of a deformabale body as we said may change the relative 
position of point with respect to some configuration assumed as a reference for 
the study of the motion itself. Let’s consider some small segments. Such small 
segments conventionally are called fibres, but they have nothing to do with 
fibres in fibre composites, even if the origin of the name traces back to the 
use of woods. We want to know how the length and the relative angles between 
these fibres change. We shall show that there is a tensor we can use to describe 
these changes.
}

\mode<presentation>{
\begin{frame}
\frametitle{The small strain tensor}
Consider a motion $\chi$ that introduces \textcolor{red}{small} (infinitesimal)
displacements w.r.t. the undeformed configuration. Let 
\[
    \bm{\epsilon} = 
     \begin{bmatrix}
     \epsilon_{xx} & \epsilon_{xy} & \epsilon_{xz} \\
     \epsilon_{yx} & \epsilon_{yy} & \epsilon_{yz} \\
     \epsilon_{zx} & \epsilon_{zy} & \epsilon_{zz}
     \end{bmatrix}
    = \textsf{Symm}(\nabla \otimes \bm{s}),\quad
    \epsilon_{ij} = \frac{1}{2}\left( \frac{\partial s_i}{\partial x_j}+
                                     \frac{\partial s_j}{\partial x_i} \right)
\]

be a \textcolor{red}{symmetric second order tensor}, called the small strain 
tensors.
\begin{itemize}
    \item components are pure numbers
    \item diagonal components $\epsilon_{ii}\,$: normal strains
    \item off-diagonal components $\epsilon_{ij}=\epsilon_{ji}, i\neq j\,$: 
    shear strains
    \item common engineering measure for the shear strain: $\gamma_{ij}= 2  
    \epsilon_{ij}, i\neq j \Rightarrow$
    \[
     \bm{\epsilon} = 
     \begin{bmatrix}
     \epsilon_{xx} & \frac{1}{2} \gamma_{xy} &  \frac{1}{2} 
     \gamma_{xz} \\[0.2em]
     \frac{1}{2} \gamma_{yx} & \epsilon_{yy} & \frac{1}{2}  
     \gamma_{yz} \\[0.2em]
     \frac{1}{2} \gamma_{zx} & \frac{1}{2} \gamma_{zy} & \epsilon_{zz}
     \end{bmatrix}
     \]
    \item \textcolor{red}{can be used to describe deformation}
\end{itemize}
\end{frame}

}
\mode<article>{
\item It is commonplace to actually express it using the so called engineering 
strains, denoted by gamma, which as we shall show are the differential angle 
between two fibres which are orthogonal in the deformed configurations
}

\mode<presentation>{
\begin{frame}[t]
\frametitle{Meaning of the normal strains}
\begin{columns}[T]
 \begin{column}{0.5\textwidth}
    \begin{center}
    \input{../../images/normal_strain_meaning}
    \end{center}
 \end{column}
 \begin{column}{0.5\textwidth}
    \begin{block}{Theorem}
        normal strain component $i$ gives the relative increment in length of 
        a \textcolor{blue}{fibre originally lying along $i$} whose initial 
        length is $\textrm{d}x_i$ and whose final length is 
        $\textrm{d}\xi_i$, i.e.
        \[
            \epsilon_{ii} = \frac{\partial s_i}{\partial x_i} = 
            \frac{\textrm{d}\xi_i -\textrm{d}x_i}{\textrm{d}x_i}
        \]
    \end{block}

 \end{column}
   
\end{columns}
\begin{block}{Proof}
e.g. for $\epsilon_{11}$
\[
    \textrm{d}\xi_i = \sqrt{(AB+BC-AD)^2+(Cb-Da)^2}
\]
trick: express the displacements of point $B=(x_{1A}+\textrm{d}x_1,x_{2A})$, 
using the displacement $\bm{s}$ of point $A=(x_{1A},x_{2A})$ 
(first order Taylor expansion)
\end{block}

\end{frame}

}

\mode<article>{
\item The hypotesis of small displacements allows us to confuse the undeformed 
and the deformed configuration for most purposes.
The trick here is to express the displacement of point B, that is located 
at $(x_A+\textrm{d}x_i,y_A) $ using the displacement $\bm{s}$ of point $A$, by 
using a first order Taylor expansion, which is allowed owing to the assumption 
of small displacements.
Then the displacemnt is given by
}

\mode<presentation>{
\begin{frame}
\frametitle{Meaning of the normal strains (cont.)}
\begin{columns}[T]
 \begin{column}{0.5\textwidth}
    \begin{center}
        \input{../../images/normal_strain_meaning}
    \end{center}
 \end{column}
 \begin{column}{0.5\textwidth}
    \begin{block}{Proof (cont.)}
        \small
        \begin{align*}
            &\bm{s}(x_1+\textrm{d}x_1,x_2) = \bm{s}+
            \frac{\partial \bm{s}}{\partial x_1}\textrm{d}x_1\\
            \Rightarrow &\textrm{d}\xi_i = \textrm{d}x_1
            \sqrt{1+ 2 \frac{\partial s_1}{ \partial x_1}+
                  \left(\frac{\partial s_1}{\partial x_1}\right)^2+
                  \left(\frac{\partial s_2}{\partial x_1}\right)^2}
        \end{align*}
        neglect second order terms and expand in Taylor series to the first 
        order to get
        \[
            \textrm{d}\xi_i = \left( 1+\frac{\partial s_1}{\partial x_1} \right)
            \textrm{d}x_1
        \]
        \begin{flushright}
            q.e.d.
        \end{flushright}
    \end{block}
 \end{column}
\end{columns}
\end{frame}
}
        
\mode<article>{
\item nothing
}
        
\mode<presentation>{
\begin{frame}
\frametitle{Meaning of the shear strains}
\begin{columns}
 \begin{column}{0.5\textwidth}
    \begin{center}
        \input{../../images/shear_strain_meaning}
    \end{center}
 \end{column}
 \begin{column}{0.5\textwidth}
    \begin{block}{Theorem}
        shear strain component $ij$ equals the differential angle made by 
        \textcolor{blue}{two fibres originally lying along perpendicular 
        directions} $i$ and $j$, i.e.
        \begin{align*}
            2 \epsilon_{ij} &= \left( 
                \frac{\partial s_i}{\partial x_j} + 
                \frac{\partial s_j}{\partial x_i}\right)=\\ 
                & = \gamma_{ij}= \pi/2 - \theta = \alpha_1+\alpha_2
        \end{align*}
    \end{block}

 \end{column}
   
\end{columns}
\end{frame}

}
\mode<article>{
\item nothing
}
            
\mode<presentation>{
\begin{frame}
\frametitle{Meaning of the shear strains (cont.)}
\vspace{-2ex}
\begin{columns}[T]
 \begin{column}{0.5\textwidth}
    \begin{center}
        \input{../../images/shear_strain_meaning}
    \end{center}
 \end{column}
 \begin{column}{0.5\textwidth}
    \begin{block}{Proof}
        as before, use a Taylor expansion of the displacement field about $A$ to
        get the displacement of $B\Rightarrow$ 
        \[
            \textrm{d}\xi_1 \sin \alpha_1 =
            \left( s_2 + \frac{\partial s_2}{ \partial x_1} \textrm{d}x_1 
            \right) - s_2
        \]
        for small deformations
        \[
            \sin \alpha_1 \sim \alpha_1 \quad \textrm{and} \quad
            \frac{\textrm{d}x_1 }{\textrm{d}\xi_1} = 
            \frac{1}{1+\epsilon_{11}} \sim 1
        \]
        hence
        \[
            \alpha_1 = \frac{\partial s_2}{ \partial x_1};
        \]
        repeat for $\alpha_2$.
        \begin{flushright}
            q.e.d.
        \end{flushright}
    \end{block}
 \end{column}
\end{columns}
\end{frame}

}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Virtual displacements}
\begin{columns}
    \begin{column}{0.45\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth,keepaspectratio=true]
            {stress_definition_200dpi.png}
        \end{center}
    \end{column}
    \begin{column}{0.55\textwidth}
        \begin{itemize}
            \item deformed configurations complying with $\bm{s}=\bm{\hat{s}}$
            on $\textcolor{red}{S_s}$ are called \emph{admissible 
            configurations}; an infinitesimal, admissible displacement (field) 
            is called a \emph{virtual displacement} (field), indicated as 
            $\delta \bm{s}$
            \item on $\textcolor{red}{S_s}$, $\delta \bm{s}=0$, 
                and arbitrary elsewhere (but compatible!)
            \item  virtual strains corresponding to  $\delta \bm{s}$:
                $\delta \epsilon_{ij}= \frac{1}{2} \left( 
                \frac{\partial \delta s_i}{\partial x_j} +
                \frac{\partial \delta s_j}{\partial x_i} \right)$
            \item \emph{virtual work}: done by actual forces moving through 
                $\delta \bm{s}$.
                E.g. for a body force $\bm{b}(\bm{x})$ field:
                \[
                  \delta W = -\int_{\textcolor{blue}{\Omega_0}} \bm{b.\delta 
                    \bm{s}}  \, \textrm{d} \Omega_0
                \]
        \end{itemize}
    \end{column}
    
\end{columns}

\end{frame}

}
                  
\mode<article>{
\item LEt's consider once again a continuum body Admissible configurations do 
not necessary respect equilibrium equations. 
Virtual displacements need not have any relationship to the 
actual displacements that might occur due to a change in the applied loads.
The displacements are called virtual because they are imagined to take place 
(i.e., hypothetical) while the actual loads acting at their fixed values.
On $S_s$ by definition the variation of the displacement field is zero, because 
there the displacement are prescribed and therefore cannot be varied. Elsewhere 
they are arbitrary,but the must satisfy compatibility, that is the motion of 
the body must not involve tearing or matter self-penetration.
}

\mode<presentation>{
\begin{frame}
\frametitle{Virtual works for continua}
\begin{block}{Virtual work of external forces}
    \[
        \delta W = -\left( \int_{\textcolor{blue}{\Omega_0}} \bm{b.}\delta 
                    \bm{s}  \, \textrm{d} \Omega_0  +
                    \int_{\textcolor{blue}{S_{\sigma}}} \bm{T.} \delta 
                    \bm{s}  \, \textrm{d} S  
        \right)
    \]
    i.e. the work of body forces $\bm{b}$ and surface tractions $\bm{T}$. 
    Neglect reaction forces over $\textcolor{red}{S_{s}}$ for there $\delta 
    \bm{s}=\bm{0}$.
\end{block}

\begin{block}{Virtual work of internal forces}
    \[
        \delta U = \int_{\textcolor{blue}{\Omega_0}} \bm{\sigma}:\delta 
        \bm{\epsilon}\, \textrm{d} \Omega_0 
    \]
    i.e. the work of the stresses through the strains.
\end{block}
\begin{itemize}
    \item virtual works are functionals over the space of admissible functions, 
        i.e. mappings from the set of all possible $\lbrace \delta 
        \bm{s}\,|\,\bm{s}=\bm{\hat{s}} \text{ in } \textcolor{red}{S_s}\rbrace$ 
        to $\mathbb{R}$
    \item the general expression for $\delta U$ is independent of the 
    constitutive law
\end{itemize}

\end{frame}

}

\mode<article>{
    \item the virtual work of intenal forces come from the work of tractions 
    over some area times the infinitesimal dispalcmente which is expressed in 
    terms of strain as 
}

\mode<presentation>{
\begin{frame}
\frametitle{Principle of virtual works}
\begin{block}{Statement}
    For a body \emph{in equilibrium} the total virtual work done due to virtual 
    displacement is zero~\cite{reddy2003mechanics}, that is
    \[
        \delta U + \delta V = 0
    \]
    or the first variation of the functional $\Pi=\Pi[\bm{s},\bm{\nabla \otimes 
    s}]= U+V$ is equal to zero: 
    \[
        \delta \Pi = 0
    \]
\end{block}
in Cartesian coordinates this is equivalent to
\[
    \int_{\Omega_0} \left( \sigma_{ij} \delta 
        \epsilon_{ij}- b_i \delta s_i \right) \, \textrm{d} \Omega_0 -
    \int_{S_{\sigma}} T_i \delta s_i  \, \textrm{d} S = 0 
\]

\end{frame}
}
    
\mode<article>{
    \item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Equilibrium from the virtual work principle}
\begin{columns}[T]
    \begin{column}{0.55\textwidth}
        The variational statement $\delta \Pi =0 $ implies the equilibrium
        equations: using the strain definition
        \begin{align*}
        &\int_{\Omega_0} \left[ \frac{1}{2} \sigma_{ij} \left( \frac{\partial 
        \delta s_i}{\partial x_j} + \frac{\partial \delta s_j}{\partial x_i}
         \right)- b_i \delta s_i \right] \, \textrm{d} \Omega_0 +\\
             - & \int_{S_{\sigma}} T_i \delta s_i  \, \textrm{d} S=0
        \end{align*}
        using the divergence theorem with $g_i=\sigma_{ij}\delta s_j$:
        \begin{align*}
            &\int_{\Omega_0} \frac{\partial \sigma_{ij}}{\partial x_i} \delta 
            s_j + \sigma_{ij} \frac{\partial \delta s_j}{\partial x_i} 
            \textrm{d} \Omega = 
            \int_{S} \sigma_{ij}n_i\delta s_j \textrm{d} S
        \end{align*}
    \end{column}
    \begin{column}{0.45\textwidth}
        \begin{block}{divergence theorem}
            given a vector field $\bm{g}(\bm{x})$ and a 3D domain 
            $\Omega$ with boundary $\partial \Omega$ and 
            normal $\bm{n}$,
            under very general conditions:
            \[
                \int_{\Omega} \nabla \bm{.g} \, \textrm{d}
                \Omega 
                = 
                \int_{\partial \Omega} \bm{g.n} \, \textrm{d}
                \partial \Omega 
            \]
            in Cartesian components 
            \[ 
                \int_{\Omega} \partial g_i/\partial x_i \, \textrm{d}
                \Omega 
                = 
                \int_{\partial \Omega} g_i n_i \, \textrm{d}
                \partial \Omega 
            \]
        \end{block}
    \end{column}
\end{columns}
\vspace{0.2ex}
repeat with $g_j=\sigma_{ji}\delta s_i$ to get
\[
    -\int_{\Omega_0} \left(\frac{\partial \sigma_{ij}}{\partial x_j} + 
    b_i \right) \delta s_i \, \textrm{d} \Omega -\int_{S_{\sigma}} T_i \delta 
s_i  \,     \textrm{d} S + \int_{S} \sigma_{ij} n_i \delta s_j  \, \textrm{d} S 
= 0
\]
\end{frame}

}
    
\mode<article>{
    \item the last passage requires using the fact that 
    $\sigma_{ij}=\sigma_{ji}$ and an exchange of dummy indexes
}

\mode<presentation>{
\begin{frame}
\frametitle{}
since $S=S_s \cup S_{\sigma}$ and $\delta \bm{s}=0$ on $S_s$ it follows that
\[
    - \int_{\Omega_0} \left(\frac{\partial \sigma_{ij}}{\partial x_j} + 
    b_i \right) \delta s_i \, \textrm{d} \Omega + 
    \int_{S_{\sigma}} \left(\sigma_{ij}n_j- T_i\right) \delta s_i  \, 
    \textrm{d} S = 0;
\]
an extended version of the fundamental lemma of calculus of 
variations~\cite[pag. 173]{hjelmstad2007fundamentals}  grants 
that the previous statement implies 
\begin{align*}
    \frac{\partial \sigma_{ij}}{\partial x_j} + b_i = 0 &\text{ in } \Omega_0
    \text{ and}\\
    \sigma_{ij} n_j - T_i=0 &\text{ in } S_{\sigma}\\
\end{align*}
\begin{itemize}
    \item the virtual work statement actually holds under conditions which are 
        more general than the local equilibrium equations (e.g. with 
        displacement fields that are just integrable)~\cite{Courant-1989}
    \item this method allowed identifying both the equilibrium equations and 
    their boundary conditions: this can be useful when these are not clear 
    \emph{a priori}
\end{itemize}

\end{frame}

}