%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Hooke's Law}
\begin{itemize}
    \item constitutive law: a mapping between two quantities $A$ and $B$ that 
    are work conjugate, i.e. such that $\int B(A) \textrm{d} A$ is an energy.
    \item stresses and strains: the simplest mapping from $\bm{\epsilon} \to 
    \bm{\sigma}$ is a     homogeneous function of degree one: a linear 
    function such that
    \[
        \bm{\sigma} = \bm{S:\epsilon}, \quad \sigma_{ij}=S_{ijhk}\epsilon_{hk}
    \]
    where $\bm{S}$ is a fourth order tensor called 
    \textcolor{red}{stiffness     tensor}. This law is called generalised 
    Hooke's law. 
    \item \textcolor{red}{homogeneous materials}: $\bm{S}$ does not depend on 
    the position $\bm{x}$.
    \item the mapping can be inverted:
    \[
        \bm{\epsilon} = \bm{C:\sigma}
    \]
    with $\bm{C}=\bm{S^{-1}}$ which is called the \textcolor{red}{compliance 
    tensor}.
    \item materials following Hooke's law are elastic, i.e. the work done on the
    material is reversible and the strain energy density (work done on the 
    material per unit volume), given by
    \[
        \mathfrak{E} = \int_{\bm{\epsilon}} \bm{\sigma:}\textrm{d}\bm{\epsilon}
                    = \int_{\bm{\epsilon}} 
                    (\bm{S:\epsilon)}\bm{:}\textrm{d}\bm{\epsilon},
    \]
    does not depend on the strain path.
\end{itemize}

\end{frame}

} 

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Restrictions on the stiffness matrix}
$\bm{S}$ and $\bm{C}$ are fourth order tensor, therefore
they posses 81 components. However~\cite{Vasilev2001}:
\begin{itemize}
    \item the symmetry of the stress and strain tensor implies that 
    $C_{ijhk}=C_{jihk}=C_{ijkh}$ (45 independent equations)
    \item from the definition of the strain energy and Hook's law
    \begin{align*}
        \Rightarrow &  \sigma_{ij} =\frac{\partial \mathfrak{E}}{\partial 
        \epsilon_{ij}}=S_{ijhk}\epsilon_{hk} \\
        \Rightarrow & S_{ijhk} = \frac{\partial \mathfrak{E}}{\partial 
        \epsilon_{ij} \partial \epsilon_{hk}} \\
        \Rightarrow & S_{ijhk}=S_{hkij} \quad (ij \neq hk)
    \end{align*}
    (15 equations)
    \item therefore there are only \textcolor{red}{21 independent constants} in 
    $\bm{S}$ or $\bm{C}$
    \item further restrictions can come from \textcolor{red}{material 
    symmetries}
\end{itemize}
\end{frame}
}
\mode<article>{
\item nothing
}
    
\mode<presentation>{
\begin{frame}
\frametitle{Voigt's Notation}
It is used to express symmetric tensors in compact notation as vectors. 
\begin{center}
\begin{tikzpicture}
    %stress part
    \matrix (m) [matrix of math nodes, ampersand replacement=\s,%
                 right delimiter=\rbrack,left delimiter=\lbrack,
                 label = above: for stress use]
    {
     \sigma_{xx} \s \tau_{xy} \s \tau_{xz} \\
     \tau_{yx} \s \sigma_{yy} \s \tau_{yz} \\
     \tau_{zx} \s \tau_{zy} \s \sigma_{zz} \\  
    };
    \matrix (v) [right = 6em of m, matrix of math nodes, %
                 right delimiter=\rbrack, left delimiter=\lbrack]
    {
    \sigma_{x}\\ \sigma_y \\ \sigma_z \\ \tau_{yz} \\ \tau_{xz} \\ \tau_{xy} \\
    };
    \node[right = 2em of m]{$\to$};
    \draw[red,-latex] (m-1-1.center) -- (m-3-3.center);
    \draw[red,-latex] (m-3-3.center) -- (m-1-3.center);
    \draw[red,-latex] (m-1-3.center) -- (m-1-2.center);
    %strain part
    \matrix (m1) [matrix of math nodes, ampersand replacement=\s,%
                 right delimiter=\rbrack,left delimiter=\lbrack,
                 label = above: for strain use,right = 3em of v ]
    {
     \epsilon_{xx} \s \gamma_{xy} \s \gamma_{xz} \\
     \gamma_{yx} \s \epsilon_{yy} \s \gamma_{yz} \\
     \gamma_{zx} \s \gamma_{zy} \s \epsilon_{zz} \\  
    };
    \matrix (v1) [right = 6em of m1, matrix of math nodes, %
                 right delimiter=\rbrack, left delimiter=\lbrack]
    {
    \epsilon_{x}\\ \epsilon_y \\ \epsilon_z \\ \gamma_{yz} \\ \gamma_{xz} \\ 
    \gamma_{xy} \\
    };
    \node[right = 2em of m1]{$\to$};
\end{tikzpicture}
\end{center}

\end{frame}

}
\mode<article>{
\item Note that we are placing gammas in our compact notation
}

\mode<presentation>{
\begin{frame}
\frametitle{Stress-strain relationship in compact form}
the stiffness (compliance) tensor $\bm{S}$ ($\bm{C}$) can be expressed as a 
\textcolor{red}{symmetric matrix}
with 36 entries mapping a 6 component vector into a 6 component vector
\[
 \bm{\sigma} =  
 \begin{bmatrix}
    \sigma_{x}\\ \sigma_y \\ \sigma_z \\ \tau_{yz} \\ \tau_{xz} \\ \tau_{xy}
 \end{bmatrix}
 =
 \bm{S.\epsilon}
 =
  \left[
 \begin{array}{cccccc}
    S_{11} & S_{12} & S_{13} & S_{14} & S_{15} & S_{16}\\  
    S_{12} & S_{22} & S_{23} & S_{24} & S_{25} & S_{26}\\
    S_{13} & S_{23} & S_{33} & S_{34} & S_{35} & S_{36}\\
    S_{14} & S_{24} & S_{34} & S_{44} & S_{45} & S_{46}\\  
    S_{15} & S_{25} & S_{35} & S_{45} & S_{55} & S_{56}\\
    S_{16} & S_{26} & S_{36} & S_{46} & S_{56} & S_{66}
 \end{array}
 \right]
  \bm{.}
   \begin{bmatrix}
    \epsilon_{x}\\ \epsilon_y \\ \epsilon_z \\ \gamma_{yz} \\ \gamma_{xz} \\ 
    \gamma_{xy}
    \end{bmatrix}
\]

$\bm{S}$ and $\bm{C}$ being tensors, the values of their components 
\textcolor{red}{depend on the reference system} and transform according to the 
appropriate transformation rule upon a basis change.
\end{frame}

}
  
\mode<article>{
\item Note that we used symmetry, i.e. we wrote off diagonal terms with the 
same indices
}

\mode<presentation>{
\begin{frame}
\frametitle{Starting from the end: isotropic materials}
The properties of isotropic materials \textcolor{red}{do not depend on the 
direction or on the reference system}
\begin{itemize}
    \item[$\Rightarrow$] $\bm{S}$ and $\bm{C}$ are invariant upon a basis change
    \item[$\Rightarrow$] $\bm{S}$ and $\bm{C}$ depend only on two independent 
    constants~\cite{reddy2003mechanics}. We take the \textcolor{red}{elastic 
    moduls} $E$ and \textcolor{red}{Poisson's ratio} $\nu$:
\end{itemize}
\begin{footnotesize}
\[    
\bm{\sigma} =  
    \frac{E}{(1+\nu)(1-2\nu)}
      \left[
 \begin{array}{cccccc}
    1-\nu & \nu & \nu & 0 & 0 & 0\\  
    \nu & 1-\nu & \nu & 0 & 0 & 0\\
    \nu  & \nu  & 1-\nu & 0 & 0 & 0\\
    0 & 0 & 0 & \frac{1}{2} (1-2\nu) & 0 & 0\\  
    0 & 0 & 0 & 0 & \frac{1}{2} (1-2\nu) & 0\\
    0 & 0 & 0 & 0 & 0 & \frac{1}{2} (1-2\nu)
 \end{array}
 \right]
 \bm{.\epsilon}
\]
\end{footnotesize}
inverse form
\begin{footnotesize}
\[
\bm{\epsilon} =  
    \frac{1}{E}
      \left[
 \begin{array}{cccccc}
    1 & -\nu & -\nu & 0 & 0 & 0\\  
    -\nu & 1 & -\nu & 0 & 0 & 0\\
   -\nu  & -\nu  & 1 & 0 & 0 & 0\\
    0 & 0 & 0 & 2 (1+\nu) & 0 & 0\\  
    0 & 0 & 0 & 0 & 2 (1+\nu) & 0\\
    0 & 0 & 0 & 0 & 0 & 2 (1+\nu)
 \end{array}
 \right]
 \bm{.\sigma}
\]
\end{footnotesize}
\end{frame}
}

\mode<article>{
\item We now look at isotropic materials. This should actually be the last step 
of our voyage, as this is the less general case that can be imagined. However 
as it is the simplest and you are already accustomed to this kind of work, we 
will go the other way around and start by looking at this case
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{itemize}
    \item there is \textcolor{red}{coupling} between the normal strains and the 
    normal stresses and vice versa: e.g. for uniaxial tension 
    $\sigma_{x}=\sigma$, all other $\sigma_{ij}=0$, then
    \[
        \epsilon_{x} = \sigma/E, \epsilon_{y} = \epsilon_{z} = -\nu \sigma/E;
    \]
    a normal stress in one direction induces normal strains in other 
    directions
    \item no coupling between normal stresses and shear strains or 
    vice versa
    \item no coupling between shear stresses or strains
    \item the components are expressed using the so called engineering 
    constants, i.e. simple quantities whose physical meaning is easily 
    understood:
    \begin{itemize}
        \item elastic modulus $E$: the ratio of normal stresses and normal 
        strain to the corresponding strain $E=\sigma_i/\epsilon_i$
        \item alternatively, the shear modulus $G$: the ratio of a shear stress
        over the corresponding shear deformation: $G=\tau_{ij}/\gamma_{ij}$; 
        for isotropic materials $G=\frac{E}{2(1+\nu)}$
        \item Poisson's ratio $\nu$: the ratio of the normal deformation along 
        some direction $j$ to the normal deformation along some other 
        direction $i$:
        $\nu = - \epsilon_j/\epsilon_i$
    \end{itemize}
    these can be determined easily under simple loading (only one stress 
    component acting)
\end{itemize}
\end{frame}
}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Anisotropic materials: engineering constant generalisation}
\begin{columns}
    \begin{column}{0.35\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width=0.95\textwidth,keepaspectratio=true]
    {unidirectional_lamina.png}
    % unidirectional_lamina.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \\{\small UD lamina: an example of anisotropic continuum}
    \end{figure}
    \end{column}
    \begin{column}{0.65\textwidth}
        For anisotropic continua, the properties depend on the direction, so
        there must be:
        \begin{itemize}
            \item an elastic modulus $E_i$ for 
            each direction $x,y,z$ and 
            \[
                \sigma_x/\epsilon_{x} = E_x  \neq E_y = \sigma_y/\epsilon_{y} 
                \neq E_z
            \]
            \item  a shear modulus for each plane
            (and no guarantee it is linked with the $E_i$s in any way)
            \[
                \tau_{xy}/\gamma_{xy} = G_{xy}  \neq G_{yz} = 
                \tau_{yz}/\gamma_{yz} \neq G_{xz}
            \]
            \item a Poisson coefficient for each pair of directions:
                $\nu_{xy} \neq \nu_{yx} \neq \nu_{yz} \neq \nu_{zy} \neq 
                \nu_{zx} \neq \nu_{xz} $
            \item generalised \textcolor{red}{coupling} is to be expected:
            there will be coefficients for  \textcolor{red}{extension-shear}
            coupling $\mu_{i,hk}=\epsilon_i/\tau_{hk}$,  
            \textcolor{red}{shear-extension} coupling             
            $\mu_{ij,k}=\gamma_{ij}/\sigma_{k}$ and \textcolor{red}{shear-shear} 
            coupling $\mu_{ij,hk}=\gamma_{ij}/\tau_{hk}$
        \end{itemize}
    \end{column}
\end{columns}
\end{frame}
}
\mode<article>{
\item nothing
}
            
\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{center}
\begin{tikzpicture}

\node (A) at (0,0) {};
\node (B) at (0,1) {};
\node (C) at (1,1) {};
\node (D) at (1,0) {};
\draw[thick,dashed] (A.center)--(B.center)--(C.center)--(D.center)--cycle;
\draw[-latex] (0.5,1.5) -- +(0,0.5) node[left]{$\sigma$};
\draw[-latex] (0.5,-0.5) -- +(0,-0.5) node[left]{$\sigma$};
\draw[thick] ($(A.center) + 
(0.1,-0.3)$)--++(0,1.5) -- ++(0.8,0) --++(0,-1.5)--cycle;

\node (A1) at (3,0) {};
\node (B1) at (3,1) {};
\node (C1) at (4,1) {};
\node (D1) at (4,0) {};
\draw[thick,dashed] (A1.center)--(B1.center)--(C1.center)--(D1.center)--cycle;
\draw[-latex] (3.2,1.3) --  node[above]{$\tau$} ++(0.5,0);
\draw[-latex] (3.7,-0.3) -- node[below]{$\tau$} ++(-0.5,0);
\draw[thick] 
($(A1.center) + (-0.1,-0.1)$)--++(0.2,1) -- ++(1,0.2) --++(-0.2,-1)--cycle;
\draw[very thin] (A1.center)--(C1.center);
\draw[very thin] (B1.center)--(D1.center);
%label
\node at (1.75,2.5) {isotropic};

\node (A) at (6,0) {};
\node (B) at (6,1) {};
\node (C) at (7,1) {};
\node (D) at (7,0) {};
\draw[thick,dashed] (A.center)--(B.center)--(C.center)--(D.center)--cycle;
\draw[-latex] (6.5,1.5) -- +(0,0.5) node[left]{$\sigma$};
\draw[-latex] (6.5,-0.5) -- +(0,-0.5) node[left]{$\sigma$};
\draw[thick] ($(A.center) + 
(0.1,-0.3)$)--++(0,1.5) -- ++(0.8,0.2) --++(0,-1.5)--cycle;

\node (A2) at (9,0) {};
\node (B2) at (9,1) {};
\node (C2) at (10,1) {};
\node (D2) at (10,0) {};
\draw[thick,dashed] (A2.center)--(B2.center)--(C2.center)--(D2.center)--cycle;
\draw[-latex] (9.2,1.3) --  node[above]{$\tau$} ++(0.5,0);
\draw[-latex] (9.7,-0.3) -- node[below]{$\tau$} ++(-0.5,0);
\draw[thick] 
($(A2.center) + (-0.1,-0.1)$)--++(0.2,1.1) -- ++(1,0.3) --++(-0.2,-1.1)--cycle;
\draw[very thin] (A2.center)--(C2.center);
\draw[very thin] (B2.center)--(D2.center);
%label
\node at (8,2.5) {generic anisotropic};
\end{tikzpicture}
\end{center}
\begin{itemize}
    \item for isotropic materials shear stresses induce no dilatation
    \item for a generic anisotropic material, normal stresses induce shear
    deformation and shear stresses induce dilatation
\end{itemize}

\end{frame}
}
\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Engineering constants for a generic anisotropic solid}
\begin{columns}
\begin{column}{0.7\textwidth}  
{\large
\[
\bm{C} =  
\begin{bmatrix}
    %line 1
     \frac{1}{E_x} & -\frac{\nu_{xy}}{E_y} & -\frac{\nu_{xz}}{E_z} 
     & \frac{\mu_{x,yz}}{G_{yz}} & \frac{\mu_{x,xz}}{G_{xz}} & 
    \frac{\mu_{x,xy}}{G_{xy}} \\[0.2ex]
    %line 2     
     -\frac{\nu_{yx}}{E_x} & \frac{1}{E_y} & -\frac{\nu_{yz}}{E_z} 
     & \frac{\mu_{y,yz}}{G_{yz}} & \frac{\mu_{y,xz}}{G_{xz}} & 
    \frac{\mu_{y,xy}}{G_{xy}} \\[0.2ex]
    %line 3
    -\frac{\nu_{zx}}{E_x} & -\frac{\nu_{zy}}{E_y} & \frac{1}{E_z} 
     & \frac{\mu_{z,yz}}{G_{yz}} & \frac{\mu_{z,xz}}{G_{xz}} & 
    \frac{\mu_{z,xy}}{G_{xy}} \\[0.2ex]
    %line 4
    \frac{\mu_{yz,x}}{E_x} & \frac{\mu_{yz,y}}{E_y} & \frac{\mu_{yz,z}}{E_z} 
    & \frac{1}{G_{yz}} &  \frac{\mu_{yz,xz}}{G_{xz}} & 
    \frac{\mu_{yz,xy}}{G_{xy}}\\[0.2ex]
    %line 5
    \frac{\mu_{xz,x}}{E_x} & \frac{\mu_{xz,y}}{E_y} & \frac{\mu_{xz,z}}{E_z} 
    & \frac{\mu_{xz,yz}}{G_{yz}} &  \frac{1}{G_{xz}} & 
    \frac{\mu_{xz,xy}}{G_{xy}}\\[0.2ex]  
    %line 6
    \frac{\mu_{xy,x}}{E_x} & \frac{\mu_{xy,y}}{E_y} & 
    \frac{\mu_{xy,z}}{E_z} 
    & \frac{\mu_{xy,yz}}{G_{yz}} &  \frac{\mu_{xy,xz}}{G_{xz}} & 
    \frac{1}{G_{xy}}\\ 
\end{bmatrix}
\]
}
\begin{itemize}
    \item symmetry holds: $\frac{\nu_{ij}}{E_j}=\frac{\nu_{ji}}{E_i}$, 
    $\frac{\mu_{ij,k}}{E_k}=\frac{\mu_{k,ij}}{G_{ij}}$ and so on
\end{itemize}
\end{column}
\begin{column}{0.3\textwidth}
\begin{block}{Conventions}
\begin{itemize}
\item moduli: each \textcolor{red}{column} is associated with a direct modulus
\item Poisson ratio: $\nu_{ij}=-\epsilon_i/\epsilon_j$ is at row
$i$, column $j$ 
\item $\mu$ coefficients: first index group gives the resulting deformation
\end{itemize}
\end{block}
\end{column}
\end{columns}


\end{frame}
}
\mode<article>{
    Recall we are using Voigt's notation so yz, xz and come before xy.
    We also use this convention for the poissons ratio, which is the ones found
    mostly in russian or german literature. The anglo-saxon literature uses the 
    opposite convention
    I find this one more sensible, because it 
    simply states that poisson coefficient xy is found in line one column 2.
    using this convention the poisson ratio fist index is the one of the normal 
    strain induced by a strain along the direction of the second index
    One should always specify the convention, but that does not always happen.
    Anyway one can always be sensible and think about what he is doing for 
    instance, as fibers are very stiff the poisson coefficient along the 
    fibre direction when there is a stress acting in the transverse direction
    will be small.
}