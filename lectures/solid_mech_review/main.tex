%kate: default-dictionary en_GB;
\usepackage{eulervm} %
\usepackage{fontspec}
\usepackage{polyglossia}
\setmainlanguage{english}
%math
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[intlimits]{mathtools}
\usepackage{bm}
\usepackage{xfrac}
\usepackage{empheq}
\usepackage[mode=tex]{standalone}

%beamer options 
\usepackage{beamerthemebars}
\usetheme{CambridgeUS}
\setbeamertemplate{footline}[page number]{}

%graphics
\usepackage{xcolor}
\usepackage{graphicx}
\graphicspath{ {../../images/} }
\usepackage{tikz}
\usetikzlibrary{positioning,3d,intersections,angles}
\usetikzlibrary{arrows,shapes,backgrounds,calc, matrix}
\usetikzlibrary{quotes}
\pgfdeclarelayer{low}  % declare background layer
\pgfdeclarelayer{up}   %declare upper layer
\pgfsetlayers{low,main,up}  % set the order of the layers (main is the 
                        %standard layer)
% For every picture that defines or uses external nodes, you'll have to
% apply the 'remember picture' style. To avoid some typing, we'll apply
% the style to all pictures.
\tikzstyle{every picture}+=[remember picture]
% By default all math in TikZ nodes are set in inline mode. Change this to
% displaystyle so that we don't get small fractions.
\tikzset{execute at begin node={\everymath{\displaystyle}}} 
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{minibox}
%captions 
\usepackage[compatibility=false]{caption}
\captionsetup{font=scriptsize}

%bibliography
\mode<presentation>{
\usepackage[hyperref=true,style=alphabetic,url=false,backend=biber]{biblatex}
}
\mode<article>{
\usepackage[hyperref=true,style=numeric, url=false,backend=biber]{biblatex}
}
\addbibresource{../../corso_compositi.bib}


\makeatletter
\tikzoption{canvas is xy plane at z}[]{%
\def\tikz@plane@origin{\pgfpointxyz{0}{0}{#1}}%
\def\tikz@plane@x{\pgfpointxyz{1}{0}{#1}}%
\def\tikz@plane@y{\pgfpointxyz{0}{1}{#1}}%
\tikz@canvas@is@plane
}
\makeatother  



%Preamble
\author{Francesco Caimmi}
\date{}
\title{Composite Materials For Structural Applications}
\subtitle{Solid mechanics review}
\institute{Politecnico di Milano - CMIC Department}
\begin{document}

\mode<presentation>{
\begin{frame}[plain]
    \titlepage
\end{frame}}
\mode<article>{
    \begin{enumerate}
        \item nothing

}

\mode<presentation>{
\begin{frame}[plain]
    \tableofcontents
\end{frame}
}
\mode<article>{
    \item nothing
}
\mode<presentation>{
  \AtBeginSection[]
    {
    \begin{frame}<beamer>
    \tableofcontents[currentsection]
    \end{frame}
    }
}

\section{Introduction}

\mode<presentation>{
\begin{frame}
\frametitle{Course: overview of part B}   
\begin{itemize}
    \item basic solid mechanics concepts: review
    \item elastic constitutive equations for anisotropic materials
    \item classical lamination theory (CLT)
    \item CLT applications 
    \item failure criteria for laminates
\end{itemize}

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>
{
\begin{frame}
\frametitle{Notation}
\begin{columns}[T]
\begin{column}{0.5\textwidth}
    \begin{itemize}
        \item[]
        \item scalars - lower-case italic letters: $u$
        \item vectors and tensor - bold upper or lower case letters: 
        $\bm{N},\bm{A}$
        \item $X,Y,Z$ or $X_1,X_2,X_3$: a generic cartesian coordinate system 
        for the reference configuration
        \item $x,y,z$ or $x_1,x_2,x_3$: a generic cartesian coordinate system 
        for the deformed configuration
        \item $1,2,3$: principal material reference system
        \item summation convention for repeated indexes: 
        $\sum\limits_{i=1}^{n} a_i b_i \to a_i b_i$
    \end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
    \begin{itemize}
        \item index contraction (matrix product, scalar product) - a single 
        bold dot: if $\bm{A}=[A_{ij}]$ and $\bm{b}=[b_i]$ then 
        $\bm{c}=[c_i]=\bm{A.b}=[A_{ij} b_j]$
        \item double contraction - a double bold dot: if $\bm{A}=[A_{ij}]$ and 
        $\bm{B}=[B_{ij}]$ then $c=\bm{A:B}=A_{ij}B_{ij}$ 
        \item index expansion (outer product) - $\otimes$: if $\bm{a}=[a_i]$ and
        $\bm{b}=[b_i]$ then $\bm{C}=[C_{ij}]=\bm{a}\otimes\bm{b}=[a_ib_j]$
        \item vector product:$\times$
    \end{itemize}
\end{column}
\end{columns}

\end{frame}
}
\mode<article>{
\item nothing
}
\mode<presentation>
{
\begin{frame}
\frametitle{Hierarchical nature of composites structures:multiscale analysis}
\begin{center}
\begin{footnotesize}    
    \def\svgwidth{0.8\textwidth}
    \input{../../images/mulitscale_composites.pdf_tex}
\end{footnotesize}
\end{center}
\end{frame}
}
\mode<article>{
\item nothing
}
\mode<presentation>
{
\begin{frame}
\frametitle{}
\begin{itemize}
    \item composites can be viewed and analyzed at different 
    levels and at different scales: \textcolor{red}{micromechanics} or 
    \textcolor{red}{macromechanics}.
    \item \textcolor{red}{micromechanics}: study of the interactions of the 
constituents of a    composite material  (matrix and fiber). Allows:
    \begin{itemize}
        \item homogenisation: the transformation of 
            \textcolor{red}{a heterogeneous material into a 
            homogeneous one} equivalent to the first (under some respect).
            \begin{center}
            \includegraphics[width=0.4\textwidth]{omogeneizzazione.png}
            % omogeneizzazione.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
            \end{center}
            As the original material is heterogenous, the homogenised material
            will be generally speaking \textcolor{red}{anisotropic}, i.e. whose
            properties depend on the direction.
        \item the design of materials micro-structure to obtain specific 
        material properties
    \end{itemize}
    \item \textcolor{red}{macromechanics}: study of the behaviour of composite 
    materials considering only global mechanical properties of a quasi 
    homogeneous anisotropic material.
\end{itemize}
\end{frame}
}
\mode<article>{
\item nothing
}
\mode<presentation>
{
\begin{frame}
\frametitle{Course Objective and Strategy}
\emph{Objective}: find a way to describe the \textcolor{red}{behaviour of 
structures and structural parts}. \newline
These are large scale parts and mostly made with \textcolor{red}{laminates}, 
i.e. \textcolor{red}{stacks of \emph{thin} laminae} made from fibre reinforced 
materials $\Rightarrow$
\begin{columns}[T]
    \begin{column}{0.55\textwidth}
        \begin{center}
        \begin{tikzpicture}[every node/.style={draw,text width=0.8\textwidth}]
                \node (a) at (0,0) {    results from 
                micromechanics$\to$ constitutive equations 
                for homogeneous equivalent materials};
                \node[below=of a] (b)  {small laminae thickness $\to$ 
                plane stress $\to$ constitutive laws for laminae};
                \node[below=of b] (c) {develop constitutive equations for 
                laminae assemblies (laminates)};
                \draw[-latex] (a)--(b);
                \draw[-latex] (b)--(c);
        \end{tikzpicture}        
        \end{center}

    \end{column}
    \begin{column}{0.45\textwidth}
        \begin{center}
        \includegraphics[width=\textwidth,keepaspectratio=true]{GLARE.png}
        % GLARE.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
        \end{center}
    \end{column}
\end{columns}
\end{frame}
}
\mode<article>{
\item nothing
}

\section{Solid Mechanics Basics}
\input{sol_mech.tex}

\section{Elastic Constitutive Laws}
\input{constitutive_laws.tex}
\mode<presentation>{
\begin{frame}[allowframebreaks]
\frametitle{References}
\printbibliography
\end{frame}
\input{../copyrightframe.tex}
}

\mode<article>{
\end{enumerate}
\printbibliography
}

\end{document}




