# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 19:28:27 2017

@author: fcaimmi

This function creates the data needed for the objective function interpolation
which is then used to solve the optimisation problem for the wrapped barrel.
The data are saved as plain text and can be post-processed using the
wrapped_barrel_optimisation.ipnb jupyter python book.

Run as::

    abaqus cae noGUI=wrapped_barrel_create_obj.py -- <options>

Note the `--` to separate script option from abaqus cae options.

Tested and working with ABAQUS 2017.

NOTE
W.r.t. the slides data, the material axis in the CAE are rotated by 90°, so
as to cope with the way material orientation is defined by ABAQUS using
continuum shell elements (possibly a bug?)
"""
#python imports
import argparse
import numpy as np
import sys
#reset the standard output to console, otherwise it will go into the ABAQUS
#internal console and be invisible.
sys.stdout = sys.__stdout__
#abaqus imports
from abaqus import *
from abaqusConstants import *
import regionToolset
import odbAccess

#globals
#units are N, mm, kg
NS = 7 #sample number
STEEL_YIELD = 1650
RHO_STEEL = 7800.0e-9
X_S = 5280.0 #lenght of the steel region
X_C = 1200.0 #lenght of the region c
R_i = 60.0 #internal half calibre
H_C = 10.0#thickness of region c
#design space bounds
#steel thickness
MINH_S = 8.0#
MAXH_S = 35.0
#region a thickness
MINH_A = 12.5
MAXH_A = 50.0
#orientation in region a
MINPHI_A = 15.0
MAXPHI_A = 54.0
#lenght of region b
MINX_B = 1400.0
MAXX_B = 5000.0
LOAD_DEFLECTION=1000.0#load applied for the beding stiffness case
OUTFILE_HEADER='\t'.join(['h_s','h_a','phi_a','X_b',
                          'F_s','F_c','F_i','D','W']) + '\n' + \
                '\t'.join(['#[mm]','[mm]','[deg]','[m]',
                          '[-]','[-]','[-]','[N/mm],[kg]']) +'\n'



def half_model_pp():
    """
    Analyses the results of the half model

    Parameters
    -----------

    None


    Returns
    ---------

    D: float
        the model compliance

    """
    odb = odbAccess.openOdb('comp-d.odb', readOnly=TRUE)
    frame = odb.steps['Step-1'].frames[-1]
    #this set 'DEF' must be defined in the model
    region1 = odb.rootAssembly.nodeSets['DEF']
    f = frame.fieldOutputs['U'].getSubset(region=region1)
    #first element in set, second component
    delta = f.values[0].data[1]
    odb.close()
    #the model is only half, so we need twice the load
    return 2*LOAD_DEFLECTION/delta

def quarter_model_pp(strength):
    """
    Analyses the results of the quarte model

    Parameters
    -----------

    strength: array like
        the interlaminar material strength parameters, in the order
        Z^+, I, where the first i s the normal separation strength and the
        second one the shear one

    Returns
    ---------

    F_s: float
        the steel failure index

    F_c: float
        the intralaminar failure index

    F_i: float
        the interlaminar failure index

    """
    odb = odbAccess.openOdb('comp-p.odb', readOnly=TRUE)
    frame = odb.steps['Step-1'].frames[-1]
    #analyse the composite failure index
    f = frame.fieldOutputs['TSAIW']
    F_c = 0
    for value in f.values:
        R = value.data
        F_c = np.max((F_c,R))

    #analyse the steel liner

    f = frame.fieldOutputs['S']
    f1=f.getSubset(region = odb.rootAssembly.elementSets['REGION-S'])
    F_s = 0
    for value in f1.values:
        R = value.mises/STEEL_YIELD
        F_s = np.max((R,F_s))

    #interlaminar failure criterion
    F_i = 0
    f = frame.fieldOutputs['SSAVG']
    for region in ['REGION-A','REGION-B','REGION-C']:
        f1=f.getSubset(region = odb.rootAssembly.elementSets[region])
        for value in f1.values:
            sigmaz = value.data[5]
            if sigmaz <0:#only positive values contribute
                sigmaz = 0
            taur = np.sqrt(value.data[3]**2+value.data[4]**2)
            R = sigmaz/strength[0]+(taur/strength[1])**2
            F_i = np.max((F_i,R))

    odb.close()
    return F_s, F_c, F_i

def redraw_half_model(mat,model,u):
    """
    Modifies the quarter model for temperature and pressure loading according
    to the paramters specified in u

    Parameters
    ------------

    mat: string
        the string identifying the material. Can be 'Carbon/PEEK' or 'SiC'

    model: ABAQUS model object
        the model to operate on

    u: array like
        the list of paramters given as
        h_s, h_a, phi_a,X_b,h_muzzle,rh_90,rh_phi


    """
    h_s, h_a, phi_a,X_b,h_muzzle,rh_90,rh_phi = u
    L_b = X_b-X_C
    #######################################################################
    #Modify the geometry
    #######################################################################
    #set the values of the sketch cross section
    p = model.parts['cylinder']
    s = p.features['Solid revolve-1'].sketch
    model.ConstrainedSketch(name='__edit__', objectToCopy=s)
    s1 = model.sketches['__edit__']
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    #h_muzzle
    d[5].setValues(value=h_muzzle, )
    #h_b==h_a
    d[4].setValues(value=h_a, )
    #X_b
    d[2].setValues(value=L_b, )
    p = model.parts['cylinder']
    p.features['Solid revolve-1'].setValues(sketch=s1)
    del model.sketches['__edit__']
    p = model.parts['cylinder']
    p.regenerate()
    #modify the face partition defining h_s
    p = model.parts['cylinder']
    s = p.features['Partition face-1'].sketch
    model.ConstrainedSketch(name='__edit__', objectToCopy=s)
    s1 =  model.sketches['__edit__']
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    d[1].setValues(value=h_s, )
    p = model.parts['cylinder']
    p.features['Partition face-1'].setValues(sketch=s1)
    del mdb.models['composite-deflection'].sketches['__edit__']
    p.regenerate()

    #######################################################################
    #Modify the stacking sequence
    #######################################################################
    #modify the lamination sequence a
    #this stuff defines the regions where the layers are
    cells = p.allSets['region-a'].cells
#    c = p.cells
#    cells = c.getSequenceFromMask(mask=('[#8 ]', ), )
    region1=regionToolset.Region(cells=cells)

    compositeLayup = p.compositeLayups['CompositeLayup-a']
    compositeLayup.deletePlies()
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-1',
                                region=region1, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_90,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=90.0,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-2',
                                region=region1, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-3',
                                region=region1, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=-phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    #modify the lamination sequence b
    compositeLayup = p.compositeLayups['CompositeLayup-b']
    compositeLayup.deletePlies()
    cells = p.allSets['region-b'].cells
    region3=regionToolset.Region(cells=cells)

    compositeLayup.CompositePly(suppressed=False, plyName='Ply-1',
                                region=region3, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_90,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=90,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-2',
                                region=region3, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-3',
                                region=region3, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=-phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)

    #######################################################################
    #Mesh
    #######################################################################
    #mesh the assembly
    a = model.rootAssembly
    a.regenerate()
    partInstances =(a.instances['cylinder-1'], )
    a.generateMesh(regions=partInstances)
    #assign the stack direction
    c1 = model.rootAssembly.sets['region-a'].cells
    f1 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f1[15], cells=c1)
    c1 = model.rootAssembly.sets['region-b'].cells
    f11 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f11[17], cells=c1)
    c1 = model.rootAssembly.sets['region-c'].cells
    f1 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f1[19], cells=c1)
    c1 = model.rootAssembly.sets['region-s'].cells
    f11 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f11[13], cells=c1)
    return None




def redraw_quarter_model(mat,model,u):
    """
    Modifies the quarter model for temperature and pressure loading according
    to the paramters specified in u

    Parameters
    ------------

    mat: string
        the string identifying the material. Can be 'Carbon/PEEK' or 'SiC'

    model: ABAQUS model object
        the model to operate on

    u: array like
        the list of paramters given as
        h_s, h_a, phi_a,X_b,h_muzzle,rh_90,rh_phi


    """
    h_s, h_a, phi_a,X_b,h_muzzle,rh_90,rh_phi = u
    L_b = X_b-X_C
    #######################################################################
    #Modify the geometry
    #######################################################################
    #set the values of the sketch cross section
    p = model.parts['cylinder']
    s = p.features['Solid revolve-1'].sketch
    model.ConstrainedSketch(name='__edit__', objectToCopy=s)
    s1 = model.sketches['__edit__']
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    #h_muzzle
    d[5].setValues(value=h_muzzle, )
    #h_b==h_a
    d[4].setValues(value=h_a, )
    #X_b
    d[2].setValues(value=L_b, )
    p = model.parts['cylinder']
    p.features['Solid revolve-1'].setValues(sketch=s1)
    del model.sketches['__edit__']
    p = model.parts['cylinder']
    p.regenerate()
    #modify the face partition defining h_s
    s = p.features['Partition face-1'].sketch
    model.ConstrainedSketch(name='__edit__', objectToCopy=s)
    s2 = model.sketches['__edit__']
    g, v, d, c = s2.geometry, s2.vertices, s2.dimensions, s2.constraints
    d[0].setValues(value=h_s, )
    p =model.parts['cylinder']
    p.features['Partition face-1'].setValues(sketch=s2)
    del model.sketches['__edit__']
    p = model.parts['cylinder']
    p.regenerate()
    #######################################################################
    #Modify the stacking sequence
    #######################################################################
    #modify the lamination sequence a
    #this stuff defines the regions where the layers are
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#8 ]', ), )
    region1=regionToolset.Region(cells=cells)
    p = mdb.models['composite'].parts['cylinder']
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#8 ]', ), )
    region2=regionToolset.Region(cells=cells)
    p = mdb.models['composite'].parts['cylinder']
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#8 ]', ), )
    region3=regionToolset.Region(cells=cells)
    compositeLayup = p.compositeLayups['CompositeLayup-a']
    compositeLayup.deletePlies()
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-1',
                                region=region1, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_90,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=90,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-2',
                                region=region2, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-3',
                                region=region3, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=-phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    #modify the lamination sequence b
    cells = c.getSequenceFromMask(mask=('[#2 ]', ), )
    region1=regionToolset.Region(cells=cells)
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#2 ]', ), )
    region2=regionToolset.Region(cells=cells)
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#2 ]', ), )
    region3=regionToolset.Region(cells=cells)
    compositeLayup = p.compositeLayups['CompositeLayup-b']
    compositeLayup.deletePlies()
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-1',
                                region=region1, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_90,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=90,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-2',
                                region=region2, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)
    compositeLayup.CompositePly(suppressed=False, plyName='Ply-3',
                                region=region3, material=mat,
                                thicknessType=SPECIFY_THICKNESS,
                                thickness=rh_phi,
                                orientationType=SPECIFY_ORIENT,
                                orientationValue=-phi_a,
                                additionalRotationType=ROTATION_NONE,
                                additionalRotationField='',
                                axis=AXIS_3, angle=0.0, numIntPoints=3)

    #######################################################################
    #Mesh
    #######################################################################
    #mesh the assembly
    a = model.rootAssembly
    a.regenerate()
    partInstances =(a.instances['cylinder-1'], )
    a.generateMesh(regions=partInstances)
    #assign the stack direction
    c1 = model.rootAssembly.sets['region-a'].cells
    f1 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f1[15], cells=c1)
    c1 = model.rootAssembly.sets['region-b'].cells
    f11 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f11[17], cells=c1)
    c1 = model.rootAssembly.sets['region-c'].cells
    f1 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f1[19], cells=c1)
    c1 = model.rootAssembly.sets['region-s'].cells
    f11 = a.instances['cylinder-1'].faces
    a.assignStackDirection(referenceRegion=f11[13], cells=c1)

    return None

def evaluate(mymdb, mat, u, rho, strength):
    """
    Evaluate the model response for given parameter

    Parameters
    -----------
    mymdb: Abaqus mdb object
        the model object

    mat: string
        material identifier string

    rho: float
        the material density

    u: list
        the list of the model parameters given as
        h_s, h_a, phi_a,X_b,h_muzzle,rh_90,rh_phi

    strength: array like
        the interlaminar material strength parameters, in the order
        Z^+, I, where the first i s the normal separation strength and the
        second one the shear one

    Returns
    -------
    F_s: float
        the steel failure index

    F_c: float
        the intralaminar failure index

    F_i: float
        the interlaminar failure index

    bstiff: float
        the bending stiffness

    W: float
        the model weigth

    """

    #calculate the weigth of the barrel
    W = RHO_STEEL*np.pi*X_S*((R_i+u[0])**2-R_i**2)#steel region
    W += rho*np.pi*X_S*((R_i+u[0]+u[1])**2-(R_i+u[0])**2)#region a
    W += rho*np.pi*u[3]*((R_i+u[0]+2*u[1])**2-(R_i+u[0]+u[1])**2)#region b
    W += rho*np.pi*X_C*((R_i+u[0]+2*u[1]+H_C)**2-(R_i+u[0]+2*u[1])**2)#region c

    #prepare and run the simulation for the quarte model with pressure and
    #temperature load
    model = mymdb.models['composite']#model name defined in the cae
    redraw_quarter_model(mat,model,u)

    #run the quarter model analyss analysis
    print "Running quarter model analysis"
    job=mymdb.jobs['comp-p']#this job name should be defined in the cae file
    job.submit()
    job.waitForCompletion()

    #result analisys

    F_s, F_c, F_i = quarter_model_pp(strength)

    #prepare and run the simulation for the stiffness model
    model = mymdb.models['composite-deflection']#model name defined in the cae
    redraw_half_model(mat,model,u)

    #run the half model analyss analysis
    print "Running half model analysis"
    job=mymdb.jobs['comp-d']#this job name should be defined in the cae file
    job.submit()
    job.waitForCompletion()

    #result analisys
    bstiff = half_model_pp()
    #result output
    print "Results"
    print "F_s, F_c, F_i, bstiff,W:", F_s, F_c, F_i, bstiff,W
    return F_s, F_c, F_i, bstiff, W

def main(cae='barrel.cae', material='carbon', nstart=0, parameters=None,
         runonce=False, save=False):
    """
    Parameters
    -----------

    cae: string, optional
        Cae file storing the models to be analysed. Defaults to `barrel.cae`.

    material: string, optional
        Material for which the objective function should be built. Defaults to
        `carbon`.

    nstart:int, optional
        The index of the parameter table, the cartesian product of the
        parameter ranges recast with shape (NS**nparam,nparam), from which
        the simulations should start. Defaults to 0. Is ignored if `runonce` is
        `True`.

    parameters: list, optional
        A list of parameters in the design space to drive the simulation.
        Used only if `runonce == True`, ignored otherwise. The paramters shall
        be given in the following order: h_s, h_a, phi_a,X_b. Defaults to None,
        which is equivalent to setting all the parameters to the lower bound
        in the design space.

    runonce: bool, optional
        If `True`, runs a single analysis rather than a complete parametric
        analisys. The parameters in the design space can be passed in using
        the `parameters` option. Defaults to False. Using runonce the
        script can be used to solve directly the optimisation problem
        without using the interpolation.

    save: bool, optional
        if True, saves the cae file at the end. Useful to check if everything
        works properly. Defaults to False.

    Returns
    --------

    out: list
        a list containing the ouput results and the design variables in the
        following order:
        h_s, h_a, phi_a,X_b,F_s, F_c, F_i, bstiff,W

    """
    #process options
    if material == 'carbon':
        mat = 'Carbon/PEEK'
        strength = [30.0,35.0]
        rho = 1580.0e-9
    elif material == 'sic':
        mat = 'SiC'
        strength = [150.0,250.0]
        rho = 4207.0e-9
    else:
        print "Unsupported material. Can be carbon or sic"

    if runonce == True:
        ns = 1
        nstart = 0
        if parameters is None:
            #they must be encapsulated in a list so we do not need handle
            #the following cycles in a different way if runonce==True
            h_s = [MINH_S]#steel thickness
            h_a = [MINH_A]#composite region a thickness
            phi_a =[MINPHI_A]#angle in region a
            X_b = [MINX_B]#lenght of region b
        else:
            h_s = [parameters[0]]
            h_a = [parameters[1]]
            phi_a = [parameters[2]]
            X_b = [parameters[3]]
    else:
        ns = NS
        h_s = np.linspace(MINH_S,MAXH_S,num=ns)#steel thickness
        h_a = np.linspace(MINH_A,MAXH_A,num=ns)#composite region a thickness
        phi_a = np.linspace(MINPHI_A,MAXPHI_A,num=ns)#angle in region a
        X_b = np.linspace(MINX_B,MAXX_B,num=ns)#lenght of region b
    #create a parameter table; ABAQUS ships a fucking ancient
    #numpy version, we cannot use a mesh grid to create the parameter grid
    #everybody hates ABAQUS
    ptable = np.zeros((ns**4,4))
    p = 0
    for i in range(len(h_s)):
        for j in range(len(h_a)):
            for k in range(len(phi_a)):
                for l in range(len(X_b)):
                    ptable[p,:] = [h_s[i],h_a[j],phi_a[k],X_b[l]]
                    p += 1
    #open the abaqus file
    mymdb = openMdb(cae)
    #prepare output database
    out = []

    for i in range(nstart, ptable.shape[0]):
        #give names to the parameters
        h_si, h_ai, phi_ai, X_bi = ptable[i]
        #calculate auxiliary variabls
        factor = 3*(np.cos(np.deg2rad(phi_ai)))**2-1
        #relative thicnkess of layer at 90° in regions a,b
        rh_90 =  factor/(4 + factor)
        #relative thickness of layers at +/- phi_a in regions a,b
        rh_phi = 2 /(4 + factor)
        h_muzzle = h_si+h_ai
        #parameter vector
        u = [h_si, h_ai, phi_ai,X_bi,h_muzzle,rh_90,rh_phi]
        print 80*'-'
        if not runonce: print 'Run index:', i
        print "Parameter set"
        print u
        F_s, F_c, F_i, bstiff,W = evaluate(mymdb, mat, u, rho, strength)
        out.append(u[:4]+[F_s, F_c, F_i, bstiff,W])
        #file ouput
        #we save at each iteration overewriting in case something goes wrong,
        #so that we can restart keeping some results
        fname = 'wrapped_barrel_obj_'+material+'.txt'
        with open(fname,'w') as of:
            of.write(OUTFILE_HEADER)
            np.savetxt(of, out, delimiter='\t')

    if save:
        print "Saving cae file"
        mymdb.save()

    mymdb.close()
    return out


if __name__=="__main__":
    #how ancient is this argparse version? It does not even prints defaults...
    parser = argparse.ArgumentParser(description=
                                    'Creates the data needed for the \
                                    optimisation program for the wrapped \
                                    cylinder exercise.')
    parser.add_argument('-m','--material',help = "material",
                        default = 'carbon',
                        choices=['carbon','sic'])
    parser.add_argument('--nstart',
                        help='The index in the parameter table (the cartesian\
                        product of the design variables space cast in shape\
                        (nsamples**nparam,nparam)) from which simulations should\
                        start. Useful for restarting. Ignored if runonce is \
                        given. Defaults to zero (first element)',
                        type=int, default=0)
    parser.add_argument('-i','--input',help='CAE file',
                        default="barrel.cae")
    parser.add_argument('-s','--save',
                          help = "Wheter or not to save the cae file at the end",
                          action='store_true', default=False
                          )
    parser.add_argument('-o','--runonce',
                        help = 'If selected, runs a single analisys rather the\
                                whole parametric study. Uses the lower bounds\
                                of the design space as parameters, or the ones\
                                set by the switch -p. Defaults to False',
                        action='store_true', default=False)
    parser.add_argument('-p','--parameters',
                        help = "Given with the option -o, is a list of\
                        comma separated values to be used as parameters in the\
                        design space",
                        type=lambda x: [float(i) for i in x.split(',')],
                        default=None
                        )
    options,_ = parser.parse_known_args(sys.argv[1:])
    main(cae=options.input,
         material=options.material,
         nstart=options.nstart,
         parameters=options.parameters,
         runonce=options.runonce,
         save = options.save)