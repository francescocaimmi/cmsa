%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Design of a L-shaped beam}
\begin{columns}
\begin{column}{0.5\textwidth}
    \centering
    \def\svgwidth{\textwidth}
    \input{../../images/Lshaped-beam-isometric.pdf_tex}
\end{column}
\begin{column}{0.5\textwidth}
Design a L-shaped beam, made of unidirectional Carbon/PEEK prepregs (cured 
ply thickness 0.3 mm), for strength using Tsai-Hill criterion 
(safety factor $\eta=1.4$).
The beam has a rectangular cross section (width $B=100$ mm ), is built-in at 
one end and the load is $P=1000$ N. Arms lenghts: $l_1=10$ m and 
$l_2=1$ m.  
\begin{block}{Material Data}
 $E_1=140$ GPa, $E_2=10$ GPa, $v_{12}=0.03$, $G_{12}=5.1$ GPa
 $X=2100$ MPa, $Y=75$ MPa, $S=90$ MPa
\end{block}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
    \item recall that we are using a criterion that makes no distinction between
    tension and compression.
}
\mode<presentation>{
\begin{frame}
\frametitle{Reaction Forces}
\begin{columns}
\begin{column}{0.45\textwidth}
    \centering
    \def\svgwidth{\textwidth}
    \input{../../images/Lshaped-beam-reaction-forces.pdf_tex}
\end{column}
\begin{column}{0.55\textwidth}
\[
     \bm{\Phi}+\bm{P}=0 \Leftrightarrow \bm{\Phi}=\begin{bmatrix}
                                                  0\\0\\P\\
                                                 \end{bmatrix}
\]
and

\begin{align*}
    &\bm{M}+\bm{l} \times \bm{P}=\bm{0} \\
    \Leftrightarrow & \bm{M} = - \begin{vmatrix}
                                  \bm{e_x} & \bm{e_y} & \bm{e_z}\\
                                  l_1 & -l_2 & 0\\
                                  0 & 0 & -P\\
                                 \end{vmatrix}\\
    \Leftrightarrow & \bm{M} = \begin{bmatrix}
                                -P l_2 \\ -P l_1 \\ 0\\
                               \end{bmatrix}\\
\end{align*}
\end{column}
\end{columns}
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Stress Resultants 1}
\begin{columns}
\begin{column}{0.5\textwidth}
    \centering
    \def\svgwidth{\textwidth}
    \input{../../images/Lshaped-beam-internal-actions.pdf_tex}
\end{column}
\begin{column}{0.5\textwidth}
    Convention: positive stress resultants directed as the 
    corresponding positive coordinate vector
    \begin{align*}
        V(s) & = -P\\
        M_f (s) & = \begin{cases}
                    P (l_1 - s) & \text{if } s<l_1\\
                    P (l_2 - s - l_1) & \text{if } l_1<s<l_2\\
                    \end{cases}\\
        M_t (s) & = \begin{cases}
                    P l_2 & \text{if } s<l_1\\
                    0 & \text{if } l_1<s<l_2\\
                    \end{cases}\\
    \end{align*}
\end{column}
\end{columns}
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Stress Resultants 2}
\begin{columns}
\begin{column}{0.5\textwidth}
    \centering
    \def\svgwidth{\textwidth}
    \input{../../images/Lshaped-beam-stress-res.pdf_tex}
\end{column}
\begin{column}{0.5\textwidth}
    Convention: positive stress resultants directed as the 
    corresponding positive coordinate vector
    \begin{align*}
        V(s) & = -P\\
        M_f (s) & = \begin{cases}
                    P (l_1 - s) & \text{if } s<l_1\\
                    P (l_2 - s - l_1) & \text{if } l_1<s<l_2\\
                    \end{cases}\\
        M_t (s) & = \begin{cases}
                    P l_2 & \text{if } s<l_1\\
                    0 & \text{if } l_1<s<l_2\\
                    \end{cases}\\
    \end{align*}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
    \item the stress resultatn $V$ gives interlaminar shear stresses, and we 
    will neglect it for now
}

\mode<presentation>{
\begin{frame}
\frametitle{Design strategy}
\begin{block}{formal problem statement}
Formally the design problem can be specified as 
\begin{empheq}[box=\fbox]{alignat*=2}
    %line 1
    \bm{x}=\underset{\bm{x}}{\operatorname{arg\,min}} \quad &  
    h\,(\bm{x}) \\
    %line 2
    \text{s.t.} \quad &  F \,(\bm{x},z,s) \leq 1/\eta\\
\end{empheq}
with $\bm{x}$ giving the lamination sequence variables ($L,h_i$ and $\theta_i$ 
for $i \in [1, \ldots, L]$) and $F$ the Tsai-Hill failure criterion. 
This is very complicated.
\end{block}
\begin{block}{simplifying assumptions}
    \begin{itemize}
        \item   check only at $s=0$
        \item   bending (dominant) and torsion $\Rightarrow$ use only $0$ and 
                $\pm45$ layers
        \item   use symmetric laminates
    \end{itemize}

\end{block}

\end{frame}
}

\mode<article>{
\item in this case we shall note that the failure criterion must be satisfied 
at every point along the beam, i.e. at varying $s$, and through the thickness, 
i.e. at varying $z$. 
The most stressed cross section is at s=0, so we will check F only there.
Moreover the stress state is dominated by bending, with some torsion; we will 
therefore only consider laminates with $0$ and $\pm45$ layers.
}

\mode<presentation>{
\begin{frame}
\frametitle{}
In this case the relevant equations reduces to:\\
\begin{itemize}
    \item global equations
    \[
        \bm{\mu} = \left[Mf/B,0,Mt/B\right],\quad \bm{\chi} = \bm{D^{-1}.\mu}, 
        \quad \bm{\epsilon} = Z\bm{\chi}
    \]
    \item equations for each layer $k$
    \begin{align*}
        \max \bm{\epsilon_k} &= \max (Z_k \bm{\chi},Z_{k+1} \bm{\chi}) 
        \text{ for each strain component },\\
        \quad \max \bm{\sigma_k} &= \bm{Q^{'}_k} . (\max \bm{\epsilon_k}),\\
        \max F &= F(\max \bm{\sigma_k}, X,Y,S)
    \end{align*}
\end{itemize}


Adopt a iterative design procedure.
\begin{enumerate}
    \item $0$ layers only. For a UD laminate $\bm{D}$ is simply given 
    by $\bm{Q(0)}h^3/12$ $\Rightarrow$ the $h$ value needed to 
    bear the loads is $h=29.3$. Since c.p.t. is 0.3 we actually need $h=29.4$ 
    with $L=98$. 
    \item neglect the torsion moment $\Rightarrow$ $h=18.3$ to 
    bear the bending moment; the maximum value of Tsai-Hill criterion would be
    $F=4.6$, which means failure.
    \item vary the lamination sequence adding layers at $45$. 
\end{enumerate}

\end{frame}

}

\mode<article>{
\item Although the torsion moment is only one tenth of the bending moment, 
neglecting it causes failure, and easily. 
}

\mode<presentation>{
\begin{frame}
\frametitle{selection of a lamination sequence}
\begin{columns}[T]
    \begin{column}{0.7\textwidth}
        \begin{tikzpicture}[spy using outlines={rectangle,%
                magnification=4,connect spies}]
            \pgfplotsset{every axis plot post/.append style={thick}}

            \pgfplotsset{set layers} 
            \begin{axis}[xlabel={Laminate height, $h$ [mm]},
                        ylabel = {$\max F$ [ ]},
                        xmin =18,xmax=30,ymin=0,ymax=5.5,
                        scale only axis,xtick pos=left,
                        width = 0.75\textwidth
                        ]
                \addplot[color=black]%
                table[x index=0,y index=1, col sep=tab]%
                {lbeam_iterative_Fvsh.txt} node%
                [coordinate,pos=0.1,%
                pin={10:{\footnotesize $[0_{30} / \pm 45_?]_s$}}]{};
                \addplot[color=blue] table[x index=2,y index=3, col sep=tab]%
                {lbeam_iterative_Fvsh.txt} node%
                [coordinate,pos=0.3,pin={270:{\footnotesize$[0_{10} / %
                                    \pm 45_?/0_{20}]_s$}}]{};
                \addplot[color=red] table[x index=4,y index=5, col sep=tab]%
                {lbeam_iterative_Fvsh.txt}%
                node[coordinate,pos=0.6,pin={75:{\footnotesize$[0_{7} / %
                                    \pm 45_?/0_{23}]_s$}}]{};
                \addplot[color=orange] table[x index=6,y index=7, col sep=tab]%
                {lbeam_iterative_Fvsh.txt}%
                node[coordinate,pos=0.3,pin={30:{\footnotesize$[0_{14} / %
                                    \pm 45_?/0_{16}]_s$}}]{};
                \addplot[color=purple] table[x index=8,y index=9, col sep=tab]%
                {lbeam_iterative_Fvsh.txt}%
                node[coordinate,pos=0.2,pin={10:{\footnotesize$[0_{19} / %
                                    \pm 45_?/0_{11}]_s$}}]{};;
                \draw[dashed,thick] (axis cs:18,0.7142857142857143) --%
                            (axis cs:30,0.7142857142857143);
                \node at (axis cs:29,1) {$1/\eta$};
            \end{axis}

            \begin{axis}[axis x line*=top,
                         axis y line=none, filter discard warning=false,
                         xmin =0, xmax=3,ymin=0,ymax=5.5,
                         xtick = {0,0.6,...,3.6},
                         xlabel={ $h_{45}$ [mm]},
                         scale only axis,
                         width = 0.75\textwidth]
%                 \coordinate (spypoint) at (axis cs:1.5,1.1);
%                 \coordinate (spyviewer) at (axis cs:2.4,3);
%                 \spy[width=2cm,height=2cm] on (spypoint)% 
%                 in node at (spyviewer);
            \end{axis}
            \begin{axis}[width=3.5cm, height=3.5cm,
                        xmin =23, xmax=25,ymin=0.5,ymax=1.5,
                        ticks=none,
                        xshift=.5\textwidth, yshift=0.35\textwidth
                        ]
                \addplot[color=blue] table[x index=2,y index=3, col sep=tab]%
                {lbeam_iterative_Fvsh.txt};
                \addplot[color=orange] table[x index=6,y index=7, col sep=tab]%
                {lbeam_iterative_Fvsh.txt};
                \addplot[color=purple] table[x index=8,y index=9, col sep=tab]%
                {lbeam_iterative_Fvsh.txt};
                \draw[dashed,thick] (axis cs:18,0.7142857142857143) --%
                            (axis cs:30,0.7142857142857143);
            \end{axis}
        \end{tikzpicture}
    \end{column}
    \begin{column}{0.3\textwidth}
        $h_{45}=h_{-45}$: the height of layers at $45$ in sequences like
        $[0,\pm 45, 0]$
        \begin{itemize}
            \item $[0_{30} / \pm 45_?]_s$: not smart
            \item \textcolor{blue}{$[0_{10} /\pm 45_?/0_{20}]_s$}: away from 
            neutral axis
            \item \textcolor{red}{$[0_{7} /\pm 45_?/0_{23}]_s$}: closer to the 
            bottom
            \item  \textcolor{orange}{$[0_{14} /\pm 45_?/0_{16}]_s$}: not 
            better
            \item  \textcolor{purple}{$[0_{19} /\pm 45_?/0_{11}]_s$}: not 
            better
        \end{itemize}
    \end{column}
\end{columns}

\end{frame}
}

\mode<article>{
\item we can use different lamination sequences. Let's start by taking a value 
of layers at zero equal to a total thickness of 18 mm.
\begin{itemize}
    \item first consider a lamination sequence  of $[0_{30} / \pm 45_?]_s$. This
    is not a very smart choice, since the stress vary linearly with $Z$ and thus
    we are placing the $45$ layers where they carry very low stresses. We can 
    get a better result, as the toal hegith is less than that of a ud laminate,
    but we can probably do better
    \item let's try a lamination sequence with layers at 45 away from the 
    mid plane, say $[0_{10} /\pm 45_?/0_{20}]_s$. This is much better: we can 
    reduce the thickness of the laminate to about 24 mm. However, since we must 
    use discrete thickness increments, we end up having to set $h_{45}$, the 
    thickness of the layer at 45, to 1.8 mm, thus having a total thickness of 
    25.2
    \item we can now see what happens if we move the 45 layers closer to the
    lower bottom, using a sequence like $[0_{7} /\pm 45_?/0_{23}]_s$: this does 
    not work. With $0_10$ we are close to the limit: if we move the layers at 
    45 further closer to the bottom, the laminate will fail by bending.
    \item what if we move them slightly further away, using a lamination 
    sequence like $[0_{14} /\pm 45_?/0_{16}]_s$? We get slighlty better results,
    but due to the fact that the increments in thickness are discrete, we have 
    to use the same thickness as for the $[0_{10} /\pm 45_?/0_{20}]_s$ case
    \item if we now move the 45 layers closer to the neutral axis, using a 
    sequence like $[0_{19} /\pm 45_?/0_{11}]_s$, the results start becoming 
    worse 
\end{itemize}

}

\mode<presentation>{
\begin{frame}
\frametitle{What's next?}
\begin{enumerate}
    \item be satisfied with the results ($h=24$ mm)
    \item start looking for alternative lamination sequences with different 
    values for $\theta_k$, e.g. 30.
    \item use optimisation techniques to tackle the problem
\end{enumerate}
\end{frame}
}


\begin{frame}[fragile]
\frametitle{Solution algorithm (pseudo code)}
\begin{columns}[onlytextwidth,c]
\begin{column}{0.7\textwidth}
\begin{small}
\begin{Verbatim}[numbersep=4pt]
define material, load and geometry data

define objective function f(L, theta_k)
    return the heigth of the laminate L*cpt
    
define the constraint function g(L,theta_k)
    assembly ABD matrix
    calculate the generalised deformation
    for each layer
        calculate the maximum/minimum stresses
        calculate the maximum value of F
        
    calculate the maximum value of F (laminate)
    
    return the maximum value of F
    
solve the optimisation problem min f s.t. g
\end{Verbatim}
\end{small}
\end{column}
\begin{column}{0.3\textwidth}
\scriptsize
Click on the pin to view an actual Python implementation of the problem solution
using the \href{http://www.pyopt.org/}{pyopt} optimisation package
\attachfile{lbeam_optm.py}.\\
A \href{http://www.jupyter.org/}{Jupyter} notebook with the same code and 
some additional interactive exploration of the design space can be obtained by 
clicking on this other pin
\attachfile{l-shape-beam.ipynb}.
\end{column}
\end{columns}
\vspace{0.5ex}
Safety (i.e. the actual value of $F<1/\eta$) has no influence on
the design objective.
\end{frame}

\mode<article>{
\item formulated in this way, for given height  but as long as $F<1/\eta$ is 
less than one, the actual value of the safety coefficient has no influence on
the objective function. That is laminates with different $F$ are considered 
equal.
}

\mode<presentation>{
\begin{frame}
\frametitle{A particle swarm solution}
\begin{itemize}
    \item Design variables: $L \in [40,100]$, $\theta_k \in 
    \pm \lbrace   0,15,30,\ldots,90\rbrace$, $k \in 
    \lbrace1,\ldots,L\rbrace$.
    \item The preliminary design helps restricting the design space
    simplifying the solution procedure.
    \item Problem solved using a particle swarm 
    optimiser~\cite{Jansen20111352}.
\end{itemize}
\begin{columns}[T]
    \begin{column}{0.4\textwidth}
        \begin{block}{solution}
            \begin{itemize}
                \item $L=66$, $h=19.8$ mm
                \item $[$  (0/  15)$_3$/ 0$_4$/-75/
                15/   0/  15$_2$/   0/  15/   0$_2$/  15/  90/  60/
                15/ -75/   0/  90/  15/   0/  15$_3$/  90/  15$]_s$
                \item smart use of layers at $15^\circ$
                \item no guarantee this is the only and optimal solution!
            \end{itemize}
        \end{block}
    \end{column}
    \begin{column}{0.6\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{./lbeam-Fmax-contour.pdf}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item the layer close to the mid-plane do not influence significantly the 
}