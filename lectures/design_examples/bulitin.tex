%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Design of a beam built-in at both ends}
\begin{block}{Problem}
Design a beam built-in at both ends using a carbon/epoxy composite in 
order to provide the minimum beam weight. 
\end{block}
\begin{columns}
    \begin{column}{0.55\textwidth}
        \begin{block}{Data}
             $E_1=142$ GPa, $E_2=10.3$ GPa, $v_{12}=0.0196$, $G_{12}=7.2$ GPa
             $X^+=1830$ MPa, $X^-=1096$ MPa, $I=35$ MPa,  $L=200$ mm, $F=2$ kN
             $b=10$ mm,safety factor $\eta=1/0.65$, cured ply thickness 0.25 mm
        \end{block}

    \end{column}
    \begin{column}{0.45\textwidth}
        \begin{figure}
            \centering
            \def\svgwidth{0.9\textwidth}
            \input{../../images/beam_clamped_both_ends.pdf_tex}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}

}

\mode<presentation>{
\begin{frame}
\frametitle{Beam statics}
The beam is doubly hyperstatic and loaded by point forces.
It's convenient to use Euler-Bernoulli equation in the form
$\frac{\mathrm{d}^2 w}{\mathrm{d}X\,^2} = -\frac{M}{I_{YY} \, E_{XX}}$ (watch 
out sign conventions).\newline
Let $M_A,V_A$ be the reaction forces at point $A\quad(X=0)$, $M_B,V_B$ at 
$B\quad(X=L)$. By equilibrium, $M_A=M_B$ and $V_A+V_B=F$.
The distribution of the stress resultants is readily obtained as:

\begin{columns}[onlytextwidth,T]
    \begin{column}{0.5\textwidth}     
     \[
        V = 
        \begin{cases}
            V_A & \text{if } 0 < X < L/2 \\
            V_A + F & \text{if } L/2 < X < L
        \end{cases}
      \]
     
      \[
        M =
        \begin{cases}
            -V\, X -M_A & \text{if } 0 < X < L/2 \\
            F\frac{L}{2}-V\, X -M_A & \text{if } L/2 < X < L
        \end{cases}        
      \]
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \def\svgwidth{0.9\textwidth}
            \input{../../images/beam_builtin_stress_resultants.pdf_tex}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{}
The solution of the Euler-Bernoulli equation can be written as
\[
    w(X) =  \begin{cases}
            \int \int M(X)\textrm{d}X + C_1 X+C_2 & \text{if } 0 < X < L/2 \\
            \int \int M(X)\textrm{d}X + C_3 X+C_4 & \text{if } L/2 < X < L,
            \end{cases}
\]
where $C_i$ are integration constants.
Using built-in boundary conditions (no rotation, no displacement) the system
\[  
    \begin{cases}
    w(0) = 0,\\ 
    w(L) = 0, \\ 
    \frac{\textrm{d}w}{\textrm{d}X}(0)=0,  \\
    \frac{\textrm{d}w}{\textrm{d}X}(L)=0,  \\
    \underset{X \to (L/2)^-}{\lim} w(X) = \underset{X \to (L/2)^+}{\lim}  
    w(X),\\ 
    \underset{X \to (L/2)^-}{\lim} \, \frac{\textrm{d}w}{\textrm{d}X} = %
    \underset{X \to (L/2)^+}{\lim} \, \frac{\textrm{d}w}{\textrm{d}X}, 
    \end{cases}   
\]
can be used to find the $C_i$, $M_A$ and $V_A$. It turns out that 
\[
    M_A = \frac{F L}{8}, \quad V_A = -\frac{F}{2}.
\]
\end{frame}
}


\mode<presentation>{
\begin{frame}
\frametitle{Preliminary design}
\begin{itemize}
 \item failure criterion: maximum stress
 \item for given $B,L$ and for a given material, minimum weight means minimum 
        thickness
 \item a beam loaded in pure bending $\Rightarrow$ optimal stacking sequence is 
       trivial: fibres directed along the beam axis ($0^\circ$)!
\end{itemize}

Therefore we can start by choosing a beam with fibre angle $\theta=0^\circ$
for all layers. 
The stresses in a layer are given by 
    \[
       \bm{\sigma_k}=
       \frac{M Z}{b} 
        \begin{bmatrix}
            Q'^k_{11} D^*_{11}+Q'^k_{12} D^*_{12}+Q'^k_{16} D^*_{16}\\
            Q'^k_{12} D^*_{11}+Q'^k_{22} D^*_{12}+Q'^k_{26} D^*_{16}\\
            Q'^k_{16} D^*_{11}+Q'^k_{26} D^*_{12}+Q'^k_{66} D^*_{16}\\
        \end{bmatrix}
       \text{ for each layer }k,
    \]
but of course for monolithic (although orthotropic) beams these reduce to
\[
    \sigma_x = \frac{M}{I_{YY}} z = \frac{12 M}{b h^3} z
\]

\end{frame}

}

\mode<presentation>{
\begin{frame}
\frametitle{}
the maximum of $M(X)$ is at $X=0,L/2$ or $L$ and is $F L/8$.
Since bending involves compressive stresses, the compressive resistance must be
used in the failure criterion in order to find the minimum $h$ by:
\[
    |\min \sigma| = \frac{3}{4} \frac{F L}{b h^2} \leq \frac{X^-}{\eta}
\]
giving
\[
    h = 6.489 \text{ mm,}
\]
rounding up to the nearest multiple of 0.25 one gets:
\[
    h = 6.5 \text{ mm (26 layers),}
\]
corresponding to a minimum tensile stress
\[
    \sigma_X = -710.1 \text{ MPa};
\]
however \ldots{} w.r.t. homogeneous material composites usually have much 
lower $S/X^-$ ratios. Need to \textcolor{red}{check interlaminar stresses}!

\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Interlaminar stresses}
Interlaminar shear stresses ($\textcolor{red}{\tau_{XZ}}$ and 
$\textcolor{red}{\tau_{YZ}}$) can be recovered using the equilibrium equations 
$\nabla \bm{.\sigma}=\bm{0}$;in our case:

\begin{align*}
&\frac{\partial\textcolor{red}{\tau_{XZ}}}{\partial Z} =-\left( 
\frac{\partial\sigma_X}{\partial X}  + \frac{\partial\tau_{XY}}{\partial Y}  
\right), \\
%line 2
&\frac{\partial\textcolor{red}{\tau_{YZ}}}{\partial Z} 
=-\left( \frac{\partial\sigma_Y}{\partial Y}  + 
\frac{\partial\tau_{XY}}{\partial X}  \right).
\end{align*}
Beams: no dependence on $Y$ and in the present case there are no 
intralaminar stresses ($\tau_{XY}=0$) $\Rightarrow$
\[
    \frac{\partial \tau_{XZ}}{\partial Z} = -\frac{\partial 
    \sigma_{X}}{\partial X}
\]


\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{}
for the $k$-th layer:
\[
    \tau_{XZ}^k = -\int_{Z_k}^{Z} 
    \frac{ \partial \sigma_{X}}{\partial X} \textrm{d} Z + C_k.
\]
The constants $C_k$, one for each layer, can be determined by equilibrium
between the layers ($\tau_{XZ}^k (Z_{k+1})=\tau_{XZ}^{k+1} (Z_{k+1})$) and by 
the boundary conditions.
For beams, where $\textrm{d}M/\textrm{d}X=V$,
\[
    \tau_{XZ}^k = -\frac{V}{b} (Q'^k_{11} D^*_{11}+Q'^k_{12} D^*_{12}+Q'^k_{16} 
    D^*_{16}) \left( \frac{Z^{\,2}-Z_{k}^{\,2}}{2} \right) + C_k(X).
\]
In the present monolithic beam case (rectangular cross section), where all the 
layers are equal, using the boundary condition $\tau_{XZ}(-h/2)=0$, the shear 
stress distribution is ($x<L/2$):
\[
    \tau_{XZ}= \frac{3 F}{b h^3} \left( Z^{\,2}-\frac{h^{\,2}}{4} \right) , 
    \quad \max |\tau_{XZ}| = \frac{3 F}{4 b h} \text{ for } Z = 0
\]

With the given data 
\[
    \max |\tau_{XZ}| = 23.10 \text{ MPa } > I/\eta = 22.75 \text{ MPa !} 
\]

\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{}
In a homogeneous material these shear stresses would be negligible w.r.t. the 
normal stresses (which are above 700 MPa). However in composites they bring the 
laminate on the brink of interlaminar failure.\newline

Since $\bm{D^*}=\bm{D}^{-1}$ and
\[
    \tau_{XZ}^k = -\frac{V}{b} (Q'^k_{11} D^*_{11}+Q'^k_{12} D^*_{12}+Q'^k_{16} 
    D^*_{16}) \left( \frac{Z^{\,2}-Z_{k}^{\,2}}{2} \right) + C_k(X)
\]
to reduce the interlaminar stresses we can try to reduce $D^*_{12}$ and 
$D^*_{16}$ without reducing $D^*_{11}$ much. To do so we can try replacing 
layers close to the core with layers disposed in a angle-ply fashion at some 
angle $\theta\neq0$.\newline
We can consider a stacking sequence like $[0/\pm\theta]_S$ with thickness 
$[h_0,h_{\theta},h_{-\theta}]_S$. To begin with we can keep $h=6.5$ mm fixed.
There are now two additional design variables $h_{\theta}$ and $\theta$.
\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{The analytical way \ldots}
In principle the problem could be tackled using an analytical formulation 
analogous to the one discussed for monothropic materials, specifying it as
\begin{empheq}[box=\fbox]{alignat*=2}
    %line 1
    \bm{x}=\underset{\bm{x}}{\operatorname{arg\,min}} \quad &  
    h\,(\bm{x}) \\
    %line 2
    \text{s.t.} \quad &  \max |\sigma_X| \leq X^-/\eta\\
                      &  \max |\tau_{XZ}| \leq I/\eta\\ 
\end{empheq}
with $h=h_0+2h_{\theta}$ and $\bm{x}=[h_{\theta},\theta]$.\newline
However, even for this very simple system, for example: 
\begin{align*}
D_{11}=&0.67 Q_{11} h_0^{3} + 4.0 Q_{11} h_0^{2} h_{\theta} + 8.0 
Q_{11} h_0 h_{\theta}^{2} + 5.3 Q_{11} h_{\theta}^{3} 
\cos^{4}{\left (\theta \right )} + \\
& 1.3 Q_{12} h_{\theta}^{3} \left(- \cos{\left (4 \theta \right )} + 1\right) 
+ 5.3 Q_{22}  h_{\theta}^{3} \sin^{4}{\left (\theta \right )} + 5.3 Q_{66} 
 h_{\theta}^{3} \sin^{2}{\left (2 \theta \right )} 
\end{align*}

Go figure what can happen when one has to calculate stuff like the inverse of 
$\bm{D}$ in order to find the stresses\ldots{} even using a CAS the 
analytical way looks a bit impervious

\end{frame}
}

\mode<presentation>{
\begin{frame}
\frametitle{Tackling the problem numerically}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
        condense the maximum stress criterion into a one parameter failure 
        index:
        \[ \max(\max |\sigma_X \eta/X^-|, \max |\tau_{XZ} \eta/I|)\]
        A map of the failure index against the design variables $h_{\theta}$ and
        $\theta$ can help to decide what to do.
        (\textcolor{red}{inside} the red lines:
        \textcolor{red}{safe regions}).

    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{center}
        \includegraphics[width=\textwidth,keepaspectratio=true]
        {./builtin_contours.pdf}
        % builtin_contours.pdf: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
        \end{center}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
of course condensation means that we lose information, since looking at this 
parameter we no longer know if failure takes place by interlaminar shear or by
bending stresses
}

\mode<presentation>{
\begin{frame}
\frametitle{}
As usually happens with composite materials, there are 
\textcolor{red}{multiple solutions } for a given problem (actually 
infinite).
Possible choices:
\begin{itemize}
    \item  $h_{\theta}=0.25$ and $\theta=45^\circ \Rightarrow$
            \[
            |\sigma_X(-h/2)| = 711.9<X^-/\eta, 
            |\tau_{XZ}(0)| = 22.65<I/\eta, 
            \] 
    \item  $h_{\theta}=0.5$ and $\theta=10^\circ \Rightarrow$
            \[
            |\sigma_X(-h/2)| = 711.4<X^-/\eta, 
            |\tau_{XZ}(0)| = 22.01<I/\eta, 
            \] 
\end{itemize}
The small modifications needed to satisfy the failure criterion implied no 
modifications to the thickness determined assuming $[0_{26}]$ stacking 
sequence, but in general this may not be true.
In addition, since now for some layers $\bm{Q}$ is fully populated and $\bm{D}$ 
is fully populated, additional stresses are to be expected in the beam! 

\end{frame}
}


\mode<presentation>{
\begin{frame}
\frametitle{Stress distribution at $X=(L/2)^-$}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
    \centering
    in-plane stresses
    \begin{tikzpicture}
        \begin{axis}[ylabel={$Z$ [mm]},
                     xlabel = {stress [MPa]},
                     width = \textwidth,
                     xmin = -715, xmax=715,
                     ymin=-3.25,ymax = 3.25]
        \addplot[color=black,dashed]%
                table[x index=1,y index=0, col sep=tab]%
                {builtin_UD_bending_stresses_midplane.txt};
        \addplot[color=black]%
                table[x index=1,y index=0, col sep=tab]%
                {builtin_laminate_bending_stresses_midplane.txt}
                node[pos=0.8]{$\sigma_X$};
        \addplot[color=red,dashed]%
                table[x index=2,y index=0, col sep=tab]%
                {builtin_UD_bending_stresses_midplane.txt};
        \addplot[color=red]%
                table[x index=2,y index=0, col sep=tab]%
                {builtin_laminate_bending_stresses_midplane.txt}
                node[pos=0.9,right]{$\sigma_Y$};
        \addplot[color=blue,dashed]%
                table[x index=3,y index=0, col sep=tab]%
                {builtin_UD_bending_stresses_midplane.txt};
        \addplot[color=blue]%
                table[x index=3,y index=0, col sep=tab]%
                {builtin_laminate_bending_stresses_midplane.txt}
                node[pos=0.05,left]{$\tau_{XY}$};
        \end{axis}
        \begin{axis}[width = 3.5cm, %heigth = 3.5cm,
                     ytick = {-1,1},
                     xtick = {-5,5},
                     xshift=.5\textwidth, yshift=0.08\textwidth,
                     xmin = -5, xmax=5,
                     ymin=-1,ymax = 1]
        \addplot[color=red,dashed]%
                table[x index=2,y index=0, col sep=tab]%
                {builtin_UD_bending_stresses_midplane.txt};
        \addplot[color=red]%
                table[x index=2,y index=0, col sep=tab]%
                {builtin_laminate_bending_stresses_midplane.txt};
        \addplot[color=blue,dashed]%
                table[x index=3,y index=0, col sep=tab]%
                {builtin_UD_bending_stresses_midplane.txt};
        \addplot[color=blue]%
                table[x index=3,y index=0, col sep=tab]%
                {builtin_laminate_bending_stresses_midplane.txt};
        \end{axis}
    \end{tikzpicture}
    \end{column}
    \begin{column}{0.5\textwidth}
    \centering
        interlaminar stresses
        \begin{tikzpicture}
        \begin{axis}[ylabel={$Z$ [mm]},
                     xlabel = {stress [MPa]},
                     width = \textwidth,
                     ymin=-3.25,ymax = 3.25]
        \addplot[color=black,dashed]%
                table[x index=1,y index=0, col sep=tab]%
                {builtin_UD_interlaminar_stresses_midplane.txt};
        \addplot[color=black]%
                table[x index=1,y index=0, col sep=tab]%
                {builtin_laminate_interlaminar_stresses_midplane.txt}
                node[pos=0.8,below]{$\tau_{XZ}$};
        \addplot[color=red,dashed]%
                table[x index=2,y index=0, col sep=tab]%
                {builtin_UD_interlaminar_stresses_midplane.txt};
        \addplot[color=red]%
                table[x index=2,y index=0, col sep=tab]%
                {builtin_laminate_interlaminar_stresses_midplane.txt}
                node[pos=0.5,left]{$\tau_{YZ}$};
        \end{axis}
    \end{tikzpicture}
    \end{column}
\end{columns}
dashed lines $[0_{26}]$; continuous lines $[0_{11},\pm45]_S$\newline
For the $[0_{11},\pm45]_S$ there is also a very small $\tau_{YZ}$ component in 
the $\pm45$ layers.

\end{frame}
}