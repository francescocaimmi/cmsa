#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Author: Francesco Caimmi francesco dot caimmi at polimi dot it

Solves the optimization problem for the plate under hydrostatic load
exercise.


"""
from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import pyOpt

#this stuff imports modules that are used to performe the needed trasformation
#of stresses and strains and to calculate the plate elastic properties
#theese needs to be defined separartely
from lib4g.composites.elastic import mat_trans_2Dvec_rot
from lib4g.composites.elastic import elastic_compliance_2D_pstress
from lib4g.composites.lamination_theory import clt_ABD
from lib4g.composites.failure import tsai_wu
#this stuff is imported from the plate file
from plate import w0
from plate import stress

#geometry and loads
a = 5e3
b = 1e3
q0 = 0.49
cpt = 0.15 #cured ply thickness
eta = 1.23 #security coeffcient
Khat = 59.3e3 #target stiffness
P = b*a*q0/2 #total load

materials = ['carbon','aramid','glass']

#materials elastic constants
moduli =[
        [ 67e3, 66e3, 0.1, 5.8e3],#carbon composite
        [35e3, 34e3, 0.15, 5.6e3],#aramid composite
        [26e3, 22e3, 0.13, 7.2e3]#glass composite
        ]
#resitances vectors
Rs = [
        [876.0, 924.0, 800.0, 840.0, 70.0, None],#carbon composite
        [600.0, 150.0, 500.0, 150.0, 44.0, None],#aramid composite
        [520.0, 490.0, 400.0, 340.0, 40.0, None]#glass composite
    ]
#densities
rhos =[1580,1250,1900] #kgm^
#foam properties
E1f,E2f,v12f,G12f = 95, 95, 0.33, 35
Rf = [3.5,3.5,3.5,3.5,1.6,None]
Cfoam = elastic_compliance_2D_pstress(E1f,E2f,v12f,G12f)
Qfoam = np.linalg.inv(Cfoam)
rhofoam = 100 #kgm^
#problem boundarise
nmin=30
nmax=50
hmin=2.0
hmax=60.0

def cplot(ofun):
    """
    Plots the wrapped ofunction as a contour plot and saves the data toa file

    """
    print('Plotting the objective function for aramid composite')
    layers = np.arange(nmin,nmax)
    heights = np.linspace(hmin,hmax,num=len(layers))
    N,H = np.meshgrid(layers,heights)
    O = np.empty_like(N, dtype='float')
    for i in range(len(layers)):
        for j in range(len(layers)):
            O[i,j] = ofun([N[i,j],H[i,j]])[0]

    plt.figure(figsize=(6,6))
    levels = [0,0.25,0.5,0.75,1,2,3,4,5]
    mymap=plt.cm.gnuplot
    plt.contourf(N,2*H,O,cmap=mymap, levels = levels)
    plt.xlabel('$n$ [ ]')
    plt.ylabel('$h_{foam}$ [mm]')
    plt.colorbar()
    plt.savefig('./plate/objfunc_aramidf.pdf', bbbox_inches = 'tight')
    plt.figure(figsize=(6,6))
    plt.contourf(N,2*H,O,cmap=mymap, levels = levels)
    plt.xticks([])
    plt.yticks([])
    plt.savefig('plate/objfunc_aramid.pdf',bbox_inches='tight',pad_inches=0)
    shape = len(layers)*len(layers)
    out = np.column_stack((N.reshape(shape),
                           2*H.reshape(shape),
                           O.reshape(shape)
                           ))
    np.savetxt('./plate/objfunc_aramid.txt', out)
    print('done')

def tw(n,hf,Q,Qfoam,R,Rf):
    """
    Calculates the tsai wu failure index for the laminate
    """
    X = a/2.0
    Y = b/2.0
    L = int((2*n+1)*2)
    #remember that for list + is concacatenate and * is repeat n times
    hs = [2*cpt,cpt]*n +[hf] #laminate heights
    hs = hs+hs[::-1]
    htot = np.sum(hs)
    Qs = [Q,Q]*n+[Qfoam]#list of stiffness matrices
    Qs = Qs + Qs[::-1]
    Rs = [R,R]*n + [Rf]
    Rs = Rs + Rs[::-1]
    thetas = [0,45]*n + [0]
    thetas = thetas + thetas[::-1]
    #layer interface positions
    Z = np.zeros(L+1)
    Z[0] = -htot/2.0
    for j in range(L):
        Z[j+1] = hs[j]+Z[j]
    A,B,D = clt_ABD(hs, Qs, thetas, symmetric = False)
    #calculate the tsai-wu failure index
    maxF = 0
    for k,theta in enumerate(thetas):
        ztop, zbottom = Z[k+1], Z[k]
        Qk = Qs[k]
        Rk = Rs[k]
        T = mat_trans_2Dvec_rot(np.deg2rad(theta))
        #top of the layer
        sigmatop = stress(X,Y,ztop,Qk,D, q0)
        #rotate to the PMRS
        sigmap = np.dot(np.linalg.inv(T), sigmatop)
        #calculate F
        tw = tsai_wu(sigmap, Rk)
        maxF = max(tw,maxF)
        #bottom of the layer
        sigmabot = stress(X,Y,zbottom,Qk,D, q0)
        #rotate to the PMRS
        sigmap = np.dot(np.linalg.inv(T), sigmabot)
        #calculate F
        tw = tsai_wu(sigmap, Rk)
        maxF = max(tw,maxF)

    return maxF,D

def objwrapp(Q,Qfoam,R,Rf):
    """
    Wrapper araound the objective function

    Parameters
    -----------

    Q: nofun = objwrapp(Q,Qfoam,R,Rf)darray
        composite stiffness matrix

    Qfoam:ndarray
        foam stiffness matrix

    R,Rf: array like
        the list of resistance constants appearing in the Tsai-Wu criterion
        for composite and foam respectively

    """
    X = a/2.0
    Y = b/2.0

    def objfunc(x):
        """
        The objective function of the problem that solves in a least square
        sense the two simultanoeus stiffness and resistance equations for
        h_{foam} and the number of layers
        """
        n = int(x[0])
        hf = x[1]
        maxF,D = tw(n,hf,Q,Qfoam,R,Rf)

        #calculate the stiffness
        w = w0(a/2.0,b/2.0,D, q0)
        K = abs(P/w)
        #objective function
        f = 0.5*((eta*maxF-1)**2+(Khat/K-1)**2)
        #contraints and fail
        g=[0.0]
        fail=0
        return f,g,fail


    return objfunc



def main(plot=False):
    """
    Main program
    """
    outfile = open('plate/solution.txt','w')
    header = '\t'.join(['material','obj','n','hfoam', 'total height','mass'])+\
            '\n'
    outfile.write(header)

    for i in range(3):
        print('Solving for '+materials[i]+' composite')
        E1,E2,v12,G12 = moduli[i]
        R = Rs[i]
        rho = rhos[i]
        C = elastic_compliance_2D_pstress(E1,E2,v12,G12)
        Q = np.linalg.inv(C)

        #instantiate the objective function
        ofun = objwrapp(Q,Qfoam,R,Rf)
        if plot and i==1:
            cplot(ofun)
        #create the problem
        problem = pyOpt.Optimization('Composite Plate', ofun)
        problem.addObj('f')
        problem.addVar('n','i',lower = nmin, upper= nmax)
        problem.addVar('hf','c',lower = hmin, upper = hmax)
        #solve
        solver = pyOpt.pyALPSO.ALPSO()
        #set the solver options, see
        #http://www.pyopt.org/reference/optimizers.alpso.html for details
#        solver.setOption('SwarmSize',50)
#        solver.setOption('maxInnerIter',8)
        #solve the optimization problem
        [fstr, xstr, inform] = solver(problem)
        n = xstr[0]
        hfoam = xstr[1]
        hcomp = n*cpt*3*2
        Vcomposite = hcomp*a*b/1e9
        Vfoam = hfoam*2*a*b/1e9
        mass = Vfoam*rhofoam+Vcomposite*rho
        #print the results to stdout and to a file
        print(problem.solution(0))
        outfile.write('\t'.join([materials[i],
                                 str(fstr),
                                 str(xstr[0]),
                                 str(xstr[1]*2),
                                 str(hcomp+hfoam),
                                 str(mass)
                                ])+'\n'
                                )

    outfile.close()


    return None

if __name__=="__main__":

    if '-p' in sys.argv:
        plot = True
    else:
        plot=False
    main(plot=plot)