#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script used to make the calcluations for the problem of the doubly built-in
beam loaded by a central force
"""
from __future__ import print_function
from lib4g.composites import elastic
from lib4g.composites.lamination_theory import clt_ABD
from lib4g.composites.lamination_theory import beam
import numpy as np
import matplotlib.pyplot as plt

#properties, units in N,mm,MPa
E1 = 142000
E2 = 10300
G12 = 7200
nu12 = E2*0.27/E1
#strenght data
Xplus = 1830.0
Xminus = 1096.0
S = 35.35
F = 2000.0  #external load, N
L = 200.0 #beam lenght
B = 10.0 #beam width
Mmax = F*L/8.0
sf = 1/0.65#safety factor
cpt = 0.25#cured ply thickness

C = elastic.elastic_compliance_2D_pstress(E1,E2,nu12,G12)
Q = np.linalg.inv(C)

def w(x,Eb,Iyy):
    """
    The vertical beam displacement

    """
    if x<=L/2.0:
        return F*x**2*(3*L - 4*x)/(48*Eb*Iyy)
    else:
        return F*(-L**3 + 6*L**2*x - 9*L*x**2 + 4*x**3)/(48*Eb*Iyy)

#parte 1 scelta dello spessore e del carico di un UD perchè ceda per taglio

plt.figure(figsize=(6,6))
ax = plt.gca()
hs = np.arange(2,18,0.25)
ws = np.zeros_like(hs)
i = 0
for h in hs:
    Iyy = B*h**3/12.0
    A,_,D, = clt_ABD([h],Q,[0],symmetric= False)
    Dstar = np.linalg.inv(D)
    Eb = 12/((h**3)*Dstar[0,0])
    sigmaxmax = -Mmax*(h/2.0)*np.dot(Q[0,:],Dstar[0,:])/B
    taumax = F*(h**2)*np.dot(Q[0,:],Dstar[0,:])/(16*B)
    print(h,taumax,sigmaxmax)
    plt.plot(h,sigmaxmax,'ko')
    plt.plot(h,taumax,'ro')
    ws[i] = w(L/2.0,Eb,Iyy)
    i = i + 1

plt.ylabel('stress [MPa]')
plt.xlabel('$h$ [mm]')
ax.plot(hs,-np.ones_like(hs)*Xminus/sf,'k')
ax.plot(hs,np.ones_like(hs)*S/sf,'r')
ax2 = ax.twinx()
ax2.plot(hs,ws,'bo')
#ax2.ylabel('$w(L/2)$ [mm]')
plt.savefig('preliminiary_dimensioning.pdf', bbox_inches = 'tight')

#calculate the minimium height for bending alone
hmin = np.sqrt(3*F*L*sf/(4*B*Xminus))
print('minumum height', hmin)
hmin = 6.5
#evaluate the correspoding maximum shear stress
taumax = 3*F/(4*B*hmin)
print ('maximum shear stress', taumax, 'critical shear stress', S/sf)

#assume a lamination sequence like [0,\pm \theta]
#see what appens at varying the angle
step = 0.05
hthetas = (1.5)*np.arange(0,1+step,step)
thetas = 90*np.arange(0,1+step,step)

X, Y = np.meshgrid(hthetas, thetas)
Z = np.empty_like(X)

for i in range(X.shape[0]):
    for j in range(X.shape[1]):
        h_th = X[i,j]
        th = Y[i,j]
        mybar = beam(B,L,[hmin/2-2*h_th,h_th,h_th],Q,[0,th,-th])
        bend = mybar.bending_stresses(Mmax)
        sigma_max = min(bend[:,0])
        inter = mybar.interlaminar_stresses(-F/2,0,zs=[0])[0]
        Z[i,j] = max(abs(sigma_max*sf/Xminus),abs(inter[0]*sf/S))
#        Z[i,j] =abs(inter[0]*sf/S)

plt.figure(figsize=(6,6))
plt.contour(X,Y,Z,[0,1],colors='r')
plt.contourf(X,Y,Z,[0.8,1,1.2,1.6,2,2.4,2.8,3.2,3.6,4],cmap=plt.cm.coolwarm)
plt.colorbar()
plt.xticks(np.arange(0,1.5,cpt))
plt.yticks(np.arange(0,100,10))
plt.xlabel('$h_{\\theta}$ [mm]',fontsize='x-large')
plt.ylabel('$\\theta$ [deg]',fontsize='x-large')
plt.title('$\\max(\\max |\\sigma_X \\eta/X^-|,\
               \\max |\\tau_{XZ} \\eta/I|)$', fontsize='x-large')
plt.savefig('builtin_contours.pdf',bbox_inches='tight')

#samples for the stress distribution
sstep = 0.125
zs = np.arange(-3.25,3.25+sstep,sstep)
#calculate the stress distribution for the case at zero
mybar = beam(B,L,[hmin],Q,[0],symmetric=False)
bend0 = mybar.bending_stresses(Mmax,zs=zs)
inter0 = mybar.interlaminar_stresses(-F/2,0,zs=zs)
#calculate tha values for theta=45, h_th=0.25
h_th = 0.25
th = 45
mybar = beam(B,L,[hmin/2-2*h_th,h_th,h_th],Q,[0,th,-th])
bend = mybar.bending_stresses(Mmax,zs=[-6.5/2.0])[0]
inter = mybar.interlaminar_stresses(-F/2,0,zs=[0])[0]
positions = []
for z in zs:
    for k in range(mybar.L):
        if z >= mybar.Zs[k] and z <= mybar.Zs[k+1]:
                    #the position z is in this layer
                    positions.append(z)
positions = np.array(positions)
bend45 = mybar.bending_stresses(Mmax,zs=zs)
inter45 = mybar.interlaminar_stresses(-F/2,0,zs=zs)
print('maximum bending stress', bend, 'maximum shear stress',inter)

#calculate tha values for theta=10, h_th=0.5
h_th = 0.5
th = 10
mybar = beam(B,L,[hmin/2-2*h_th,h_th,h_th],Q,[0,th,-th])
bend = mybar.bending_stresses(Mmax,zs=[-6.5/2.0])[0]
inter = mybar.interlaminar_stresses(-F/2,0,zs=[0])[0]
print('maximum bending stress', bend, 'maximum shear stress',inter)
#save the data
out = np.column_stack((zs,bend0))
np.savetxt('builtin_UD_bending_stresses_midplane.txt', out, delimiter ='\t')

out = np.column_stack((zs,inter0))
np.savetxt('builtin_UD_interlaminar_stresses_midplane.txt', out, delimiter ='\t')

out = np.column_stack((positions, bend45))
np.savetxt('builtin_laminate_bending_stresses_midplane.txt', out, delimiter ='\t')

out = np.column_stack((zs, inter45))
np.savetxt('builtin_laminate_interlaminar_stresses_midplane.txt', out, delimiter ='\t')
