# -*- coding: utf-8 -*-
"""
Performs the calculations for the design of the L-shaped beam example
"""
from __future__ import print_function
import numpy as np
from sympy import Eq
from sympy import symbols, solve
from sympy.matrices import Matrix
import matplotlib.pylab as plt

from lib4g.composites.elastic import mat_trans_2Dstrain_rot
from lib4g.composites.elastic import elastic_compliance_2D_pstress
from lib4g.composites.lamination_theory import clt_ABD
from lib4g.composites.failure import tsai_hill

def eval_seq(h0,ht):
    """
    Evaluates a lamination sequence

    """
    F_lamination = np.zeros_like(heights)
    total_heigth = np.zeros_like(heights)
    Ms = np.array([Mx,0,Mxy])
    for i, h45 in enumerate(heights):
        hs = [h0,h45,h45,ht-h0]
        h = 2*np.sum(hs)
        L = 2*len(hs)
        hf = hs+hs[::-1]
        total_heigth[i] = h
        Z = np.zeros(L+1)
        Z[0] = -h/2.0
        for j in range(L):
            Z[j+1] = hf[j]+Z[j]

        t = [0.0, 45.0, -45.0,0.0]
        A,_,D = clt_ABD(hs,Q,t)
        chi = np.dot(np.linalg.inv(D),Ms)
        #for each layer lets calulate the maximum value of the Tsai-Hill index
        Flayers = np.zeros(len(t))
        print('#'*40)
        print('h45=',h45)
        for k,theta in enumerate(t):
            print("layer",k+1)
            eps_top = Z[k+1]*chi
            eps_bot = Z[k]*chi
            #transform the strains in the PMRS
            Te = mat_trans_2Dstrain_rot(np.deg2rad(theta))
            eps_top12 = np.dot(Te,eps_top)
            eps_bot12 = np.dot(Te,eps_bot)
            #get the stresses
            sigma_top = np.dot(Q,eps_top12)
            sigma_bot = np.dot(Q,eps_bot12)
            #get Tsai-Hill index
            Ftop = tsai_hill(sigma_top,[X,Y,S])
            Fbot = tsai_hill(sigma_bot,[X,Y,S])
            print(Fbot,Ftop,np.max([Ftop,Fbot]))
            Flayers[k] = np.max([Ftop,Fbot])
        F_lamination[i] = np.max(Flayers)
        layer = np.argmax(Flayers)

        print("For h45=",h45,"the most dangerous layer is",layer+1)
    return F_lamination, total_heigth

#material data
E1,E2,v12,G12 = 140e3, 10e3, 0.03, 5.1e3
X, Y, S = 2100, 75, 90
#geometry
l1, l2 = 10e3, 1e3
B = 100
#load
P = 1000
#safety factor
eta = 1.4

###############################################################################
#ud laminate: find the thickness needed to whistand the loads and
#finde the thicnkness needed to withstand the axial loads
###############################################################################
C = elastic_compliance_2D_pstress(E1,E2,v12,G12)
Q = np.linalg.inv(C)
Qsymb = Matrix(C).inv()
h = symbols('h')
hs = [h]
t = [0.0]
A,_,D = clt_ABD(hs,Q,t, symmetric=False, symbolic=True)
Mxy = P*l2/B
Mx = P*l1/B
Ms = Matrix([Mx,0,Mxy])
chi = D.inv().dot(Ms)
epsilon = Matrix([h*chi[i]/2 for i in range(3)])#max epsilon
sigma = Qsymb.dot(epsilon)
F = tsai_hill(sigma,[X,Y,S])
result = solve(Eq(F,1/eta),h)
print("Height of a ud laminate", result)
###############################################################################
#ud laminate: neglect the torsion load, find the thickness needed to
#bear the normal stress; verify it cannot bear the actual load state
###############################################################################
F = tsai_hill([sigma[0],0.0,0.0],[X,Y,S])
result = solve(Eq(F,1/eta),h)
print("Height of a ud laminate neglecting torsion moment", result)
F = tsai_hill(sigma,[X,Y,S])
print( "Tsai Hill index  for a UD laminate dimensioned neglecting torsion",
      F.subs(h,result[1]))
###############################################################################
#Case 1
#start from the previous result and add a couple of layers at plus/minus phi
#near the neutral axis, ie. a lamination sequence like
# 0_30/ 45_?/-45_?
#analytic solutions anyway cannot be obtained
##############################################################################
heights = np.arange(0,11*0.3,0.3)
print('#'*40)
print("Lamination sequence 0_30/ 45_?/-45_?")
h0 = 9 #30 layers at zero now with a symmetric lamination sequence
F_lamination1, total_heigth1 = eval_seq(h0,9)
plt.plot(total_heigth1,F_lamination1,'r',label = '$[0_{30}/\\pm 45_?]_s$')

###############################################################################
#Case 2
#start from the previous result and add a couple of layers at plus/minus phi
#near the neutral axis, ie. a lamination sequence like
# 0_10/ 45_?/-45_?/0_20
#analytic solutions anyway cannot be obtained
##############################################################################
print('#'*40)
print("Lamination sequence 0_10/ 45_?/-45_?/0_20")
h0 = 3 #10 layers at zero now with a symmetric lamination sequence
F_lamination2, total_heigth2 = eval_seq(h0,9)

plt.plot(total_heigth2,F_lamination2,'b',label = '$[0_{10}/\\pm 45_?/0_{20}]_s$')

###############################################################################
#Case 3
#start from the previous result and add a couple of layers at plus/minus phi
#near the neutral axis, ie. a lamination sequence like
# 0_10/ 45_?/-45_?/0_20
#analytic solutions anyway cannot be obtained
##############################################################################
print('#'*40)
print("Lamination sequence 0_7/ 45_?/-45_?/0_23")
h0 = 2.1 #7 layers at zero now with a symmetric lamination sequence

F_lamination3, total_heigth3 = eval_seq(h0,9)

plt.plot(total_heigth3,F_lamination3,'g', label = '$[0_{7}/\\pm45_?/0_{23}]_s$')
###############################################################################
#Case 4
#start from the previous result and add a couple of layers at plus/minus phi
#near the neutral axis, ie. a lamination sequence like
# 0_10/ 45_?/-45_?/0_17
#analytic solutions anyway cannot be obtained
##############################################################################
print('#'*40)
print("Lamination sequence 0_14/ 45_?/-45_?/0_16")
h0 = 4.2 #10 layers at zero now with a symmetric lamination sequence

F_lamination4, total_heigth4 = eval_seq(h0,9)

plt.plot(total_heigth4,F_lamination4,'m', label = '$[0_{14}/\\pm 45_?/0_{16}]_s$')

###############################################################################
#Case 5
#start from the previous result and add a couple of layers at plus/minus phi
#near the neutral axis, ie. a lamination sequence like
# 0_10/ 45_?/-45_?/0_17
#analytic solutions anyway cannot be obtained
##############################################################################
print('#'*40)
print("Lamination sequence 0_19/ 45_?/-45_?/0_11")
h0 = 5.7 #10 layers at zero now with a symmetric lamination sequence

F_lamination5, total_heigth5 = eval_seq(h0,9)

plt.plot(total_heigth5,F_lamination5,'c', label = '$[0_{19}/\\pm 45_?/0_{11}]_s$')

#finalize the plot
plt.plot(total_heigth1,[1/eta for i in heights],'k')
plt.xlabel('Laminate Height, $h$ [mm]',fontsize = 'xx-large')
plt.ylabel('max $F$ [ ]',fontsize = 'xx-large')
plt.legend(loc='best',fontsize='small')
ay2 = plt.twiny()
ay2.xaxis.tick_top()
ay2.xaxis.set_ticks(heights)
ay2.set_xlabel('$h_{45}$', fontsize = 'xx-large')
plt.savefig('lbeam_iterative_Fvsh.pdf',bbox_inches='tight')
plt.show()
#output the data
output = np.column_stack((total_heigth1,F_lamination1,
                          total_heigth2,F_lamination2,
                          total_heigth3,F_lamination3,
                          total_heigth4,F_lamination4,
                          total_heigth5,F_lamination5,
                          ))
np.savetxt('lbeam_iterative_Fvsh.txt', output, delimiter = '\t')
