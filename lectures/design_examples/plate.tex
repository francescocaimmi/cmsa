%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{A simply supported plate under hydrostatic load}
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{block}{Problem}
            A rectangular steel bulkhead with sides $a$ and $b$ and thickness 
            $h$ is subject to an hydrostatic load $q=q_0 \frac{X}{a}$; the      
              plate can be assumed to be simply supported. 
            Design a composite replacement providing the same stiffness with  
            minimum weight. Apply the same security coefficient that was 
            applied to the steel plate.
        \end{block}
        \begin{block}{Data}
            $a=$\SI{5}{\meter}, $b=$\SI{1}{\meter}, $h=$\SI{20}{\milli\meter}
            $q_0=$\SI{0.49}{\mega\pascal}, $E_{steel}=$\SI{210}{\giga\pascal}
            $\nu_{steel}=$0.3
        \end{block}
    \end{column}
    \begin{column}{0.5\textwidth}
        \centering
        \def\svgwidth{\textwidth}
        \input{../../images/plate_hydroload.pdf_tex}
    \end{column}
\end{columns}

\end{frame}
}

\mode<article>{
\item bulkeahd \`e paratia. In this example we shall again use a relatively 
traditional design approach, meaning that, given some very simple design 
objective, we shall perform an initial analisyto select sensibly many of the 
possible design variables, and in particular the stacking sequence, and solve 
some equation to find the free geometric variables left.
}

\mode<presentation>{
\begin{frame}
\frametitle{Plate solution}
As long as it can be assumed that a plate is specially orthotropic 
($\bm{B}=\bm{0}, D_{16}=D_{26}=0$) and there are only normal loads transverse to
the midplane, the equation for $w(X,Y)$ reduces to
\[
    D_{11} \frac{\partial^4 w}{\partial X\,^4}
    + 2(D_{12}+2 D_{66}) \frac{\partial^4 w}{\partial X\,^2 \partial Y\,^2}
    + D_{22 } \frac{\partial^4 w}{\partial Y\,^4}-q=0
\]

\begin{columns}[T,onlytextwidth]
\begin{column}{0.6\textwidth}
Simply supported boundary conditions on all edges; the vertical displacement 
vanish:
    \[
        w(X,0)=w(X,b)=w(0,Y)=w(a,Y)=0;
    \]
    the moments along the edge directions must also vanish
    \[
        M_X(0,Y) = M_X(a,Y) = M_Y(X,0) = M_Y(X,b) = 0
    \]
\end{column}
\begin{column}{0.4\textwidth}
\centering
\def\svgwidth{0.8\textwidth}
\input{../../images/plate_hydroload.pdf_tex}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item the moment along the edge directions must vanish because there the 
surfaces are unloaded, so there can be only stresses acting on faces with 
normal transverse to the free edge. 
}

\mode<presentation>{
\begin{frame}
\frametitle{}
The solution of this equation can be expressed~\cite{reddy2003mechanics} as:
\[
    w(X,Y) = \sum_{n=1}^{\infty} \sum_{m=1}^{\infty} W_{mn} \sin \alpha X 
            \sin \beta Y,
\]
where $\alpha = m \pi /a$ and $\beta = n \pi /b$. This solution satisfies 
automatically the boundary conditions.
The Fourier double sine transform of the load $q$ can be written as 
\[
    q(X,Y) = \sum_{n=1}^{\infty} \sum_{m=1}^{\infty} q_{mn} \sin \alpha X 
    \sin \beta Y.
\]
In the present case:
\[
  q_{mn}  = \frac{8 q_0 \cos m\pi}{\pi^2 m n} \text{ with } m,n \text{ odd }.
\]
Plugging the expression for $q$ in the equilibrium equation it is possible to 
solve for the $W_{mn}$ coefficients; hence:
\[
    W_{mn} = \frac{q_{mn}}{d_{mn}} \text{ with }
    d_{mn} = \frac{\pi^4}{b^4} \left[ D_{11} m^4 s^4 + 2(D_{12}+2 D_{66}) m^2 
                                        n^2 s^2 + D_{22} n^4 \right],
    s = \frac{a}{b}
\]
\end{frame}
}

\mode<article>{
\item we can use this technique for whatever solution we may need
}

\mode<presentation>{
\begin{frame}
\frametitle{}
The bending moments are readily obtained from the constitutive equations
\[
    \begin{bmatrix}
        M_X\\M_Y\\M_{XY}
    \end{bmatrix} 
    = 
    \sum_{n=1}^{\infty} \sum_{m=1}^{\infty} W_{mn}
    \begin{bmatrix}
    (D_{11} \alpha^2 + D_{12} \beta^2) \sin \alpha X \sin \beta Y \\
    (D_{12} \alpha^2 + D_{22} \beta^2) \sin \alpha X \sin \beta Y \\
    -2 \alpha \beta D_{66} \cos \alpha X \cos \beta Y 
    \end{bmatrix} \text{ with } m,n \text{ odd }    
\]
and the stresses in each layer are given by 
\[
    \bm{\sigma_k}(X,Y,Z)=
    \begin{bmatrix}
        \sigma_{X} \\ \sigma_{Y} \\ \sigma_{XY}
    \end{bmatrix}_{k}
    =
    Z \sum_{n=1}^{\infty} \sum_{m=1}^{\infty} W_{mn}
    \begin{bmatrix}
        (Q_{11}^{'k} \alpha^2 + Q_{12}^{'k} \beta^2) 
            \sin \alpha X \sin \beta Y \\
        (Q_{12}^{'k} \alpha^2 + Q_{22}^{'k} \beta^2) 
            \sin \alpha X \sin \beta Y \\
            -2 \alpha \beta Q_{66}^{'k} \cos \alpha X \cos \beta Y 
    \end{bmatrix},
\]
with $m,n$ odd.
These equations of course hold also for a isotropic plate, where there is a 
single layer. $m,n\simeq31$ provide sufficient precision. The maximum normal 
stresses are at $(a/2,b/2)$; the maximum shear stresses are at the corners.
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Stress resultants in the isotropic case}
\centering
    \begin{tikzpicture}
    \begin{axis}[xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},
                 width=0.725\textwidth,
                 title = {$M_X$ [N mm/mm]},
                 colorbar]
    \addplot[point meta min=-12e3, point meta max=0] % 
    graphics[xmin=0,xmax=5,ymin=0,ymax=1] {./plate/MX.pdf};
    \end{axis}
    \end{tikzpicture}
\end{frame}
}

\mode<article>{
\item The first thing we want to do is to to look at the stress state 
of the homogeneous equivalent plate. 
This can give hints on what we are going to find in the composite arena.
Remember that $M_X$ is directed along the $Y$-axis and is generated 
by stresses whose normal is along the $X$-axis. Note that on the boundaries the 
moment is zero. It's negative elsewhere, meaning the the region above the 
midplane is in tension and the one above in compression. 
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\centering
    \begin{tikzpicture}
    \begin{axis}[xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},
                 width=0.725\textwidth,
                 title = {$M_Y$ [N mm/mm]},
                 colorbar]
    \addplot[point meta min=-32e3, point meta max=0] % 
    graphics[xmin=0,xmax=5,ymin=0,ymax=1] {./plate/MY.pdf};
    \end{axis}
    \end{tikzpicture}
\end{frame}
}

\mode<article>{
\item of course the moment $M_Y$, which is directed along $X$ and genrated by 
stresses directed along $Y$, is larger than $M_X$ because the plate is longer 
along the $X$ direction.
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\centering
    \begin{tikzpicture}
    \begin{axis}[xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},
                 width=0.725\textwidth,
                 title = {$M_{XY}$ [N mm/mm]},
                 colorbar]
    \addplot[point meta min=-12e3, point meta max=12e3] % 
    graphics[xmin=0,xmax=5,ymin=0,ymax=1] {./plate/MXY.pdf};
    \end{axis}
    \end{tikzpicture}
\end{frame}
}

\mode<article>{
\item there is a significant torsional moment, of the same order of magnitude 
than the bending moment perpendicular to the most slender side.
This is very important to note, since we have to expect a similar effect also
for composites. The origin of this torsion moment at the edges is clear? It is 
due to the fact that we have a non uniform distribution of shear forces along 
the edges.
}

\mode<presentation>{
\begin{frame}
\frametitle{Von Mises stress}
\centering
    \begin{tikzpicture}
    \begin{axis}[xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},
                 width=0.6\textwidth,
                 title = {$\sigma_v$ [MPa]},
                 colorbar]
    \addplot[point meta min=0, point meta max=406] % 
    graphics[xmin=0,xmax=5,ymin=0,ymax=1]{./plate/SMISES.pdf};
    \end{axis}
    \end{tikzpicture}\newline
The maximum Von Mises stress is at ($a/2$,$b/2$) and is equal to 
\SI{406}{\mega\pascal}. Security coefficient: 
$\eta=\sigma_{yield}/\sigma_v=1.23$. 
Corners: $\sigma_v=$\SI{300}{\mega\pascal}.
\end{frame}
}

\mode<article>{
\item Therefore there is a significant stress at the plate corners.
}

\mode<presentation>{
\begin{frame}
\frametitle{Steel plate deformed shape}
\centering
\begin{tikzpicture}
    \begin{axis}[%xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},zlabel = {$w$ [mm]},
                 width=0.6\textwidth,
                 colorbar]
    \addplot3[surf, mesh/rows=25,mesh/cols=25]%
    table[x index=0,y index=1,z index=2,col sep=tab]%
    {./plate/w_steel.txt};
    \end{axis}
\end{tikzpicture}\newline
The maximum displacement is at ($a/2$,$b/2$). If 
$P=\int_{0}^{a} \int_{0}^{b} q(X,Y) \,\text{d} X\, \text{d} Y$ then the 
stiffness $\hat{K}=P/\max(abs(w))$ is \SI{59.3}{\kilo\newton\per\milli\metre}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Design strategy and preliminary stacking sequence selection}
Classical design strategy: two equation (resistance and stiffness constraints) 
can be used to calculate two design variables. All the others need to be fixed
\emph{a priori}.
Reduction of the design space:
\begin{itemize}
    \item biaxial loading and torsion are expected $\Rightarrow$ it could be
        smart to use woven reinforcements which have $E_1 \simeq E_2$ and 
        $X \simeq Y$. They:
        \begin{itemize}
            \item can resist biaxial loads
            \item provide faster manufacturing
            \item in addition, when $E_1 \simeq E_2$ then $D_{16}$ and $D_{26}$ 
                are negligible for stacking sequences like the one which is
                going to be used
        \end{itemize}
    \item given the loads, a stacking sequence like $[0_{n1}/45_{n2}]_{n,S}$ 
    seems sensible
    \item as to the ratio $n1/n2$ no clear indication can be given a priori: it 
    shall be assumed $n2=n1/2$ so a lamination sequence of the type 
    $[0_2/45]_{n,S}$ will be sought
\end{itemize}
\end{frame}
}

\mode<article>{
\item The approach we use now is relatively classic. We have some design 
equations, the one regarding the load bearing capabilities and the one for 
stiffness, so that we have two design variables that can be determined 
by these equations.
Therefore we will try to fix all the other possible design variables \emph{a 
priori}, and then we will use the equations to calculate the free ones.

most unfortunately if the material is not isotropic, the 
stress state 
depends on the material constants, so it is not so easy to follow such a line 
of reasoning. When $E_1 \simeq E_2$ then $D_{16}$ and $D_{26}$ 
are negligible for stacking sequences like the one which is going 
to be used: we can therefore, how luck we are, use the solution for specially 
orthotropic materials.
}

\mode<presentation>{
\begin{frame}
\frametitle{Materials}
\scriptsize
\tabulinesep=1ex
\begin{center}
\begin{tabu} to \textwidth {X|XXXX|XXX|X}
\firsthline
\rowfont{\bfseries}
%top line
material & $E_1$ [GPa] & $E_2$ [GPa] & $\nu_{12}$ [ ] & $G_{12}$ [GPa] & 
$X^+(X^-)$ [MPa] & $Y^+(Y^-)$ [MPa] & $S$ [MPa] & $\rho$ 
[\si{\kilo\gram\per\cubic\deci\metre}]\\
\hline
%first material
carbon-epoxy 5HS & 67 & 66 & 0.1 & 5.8 & 876 (924) & 800 (840) & 70 & 1.58 \\
%second material
glass-phenolic 8HS & 26 & 22 & 0.13 & 7.2 & 520 (490) & 400 (340) & 40 & 
1.9\\
%thir material    
aramid-epoxy PW & 35 & 34 & 0.15 & 5.6 & 600 (150) & 500 (150) & 44 & 
1.25\\ \\
\lasthline
\end{tabu}
Assume a cpt of \SI{0.15}{\milli\metre}
\end{center}


\end{frame}
}

\mode<article>{
\item PW stands for plane weave
}

\mode<presentation>{
\begin{frame}
\frametitle{Equations}
The problem can be solved by inspection; consider the three material we 
selected, and solve the two design equations,which are strongly non-linear 
functions of $n$:
\begin{itemize}
    \item Tsai-Wu failure criterion
    \[
        F(\sigma,n)=\frac{1}{\eta} \Rightarrow n=n_a
    \]
    \item stiffness
    \[
        K(n) = \hat{K} \Rightarrow n=n_b
    \]
\end{itemize}
Once these have been solved numerically or graphically, $\max(n_a,n_b)$ is the 
required answer.
The numerical solution is non trivial as $n$ is an integer variable; the best 
choice may be to borrow integer optimisation techniques (see e.g. 
\cite{Balas2009} for an introduction) and solve the equivalent 
minimisation problems, for example
\[
    \min \left( K(n) - \hat{K} \right), n \in  \mathbb{N}
\]
\end{frame}
}

\mode<article>{
\item the numerical solution of minimisation problems involving integers is 
usually much more complicated than that of problems involving real 
variables~\cite{Balas2009}.
}

\mode<presentation>{
\begin{frame}
\frametitle{Where should $F$ be calculated?}
\centering
glass fibre composite
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
    \centering
    \begin{tikzpicture}
    \begin{axis}[xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},
                 width=0.85\textwidth,
                 title = {$F_{max}$ [ ], $n=1$},
                 colorbar horizontal,
                 %colorbar style={width=0.5cm}
                 ]
    \addplot[point meta min=0, point meta max=4.2e6] % 
    graphics[xmin=0,xmax=5,ymin=0,ymax=1]{./plate/Fdist_n1.pdf};
    \end{axis}
    \end{tikzpicture}
    \end{column}
    \begin{column}{0.5\textwidth}
    \centering
    \begin{tikzpicture}
    \begin{axis}[xmin=0,xmax=5,ymin=0,ymax=1,
                 xlabel= {$X$ [m]},ylabel={$Y$ [m]},
                 width=0.85\textwidth,
                 title = {$F_{max}$ [ ], $n=10$},
                 colorbar horizontal,
                 %colorbar style={width=0.5cm}
                 ]
    \addplot[point meta min=0, point meta max=560] % 
    graphics[xmin=0,xmax=5,ymin=0,ymax=1]{./plate/Fdist_n10.pdf};
    \end{axis}
    \end{tikzpicture}
    \end{column}    
\end{columns}
for low $n$ values the corners may be dangerous
\end{frame}
}
\mode<article>{
\item If we look at the distribution of the Tsai-Wu failure index in our 
laminate, what we can see is that the corners may be dangerous: for low $n$ in 
fact the most critical points in the sense of the Tsai-Wu criterion 
are there. 
This is strongly at variance with the case of isotropi material 
where the most critical point would be definitely in the middle, where the 
maximum stresses are. 
This is of course a consequence of the anisotropic material resistance.
It is therefore advisable to calculate the value of $F$ both at one corner and 
in the middle of the plate.

Here were cannot see the layer were failure takes place, but if we looked
    into that we would see that at the centre  failure would take place in the 
layer at 45, while near the corners in the layer at 0, where there are high 
torsion moments.
}

\mode<presentation>{
\begin{frame}
\frametitle{Graphical solution}
\begin{columns}[T]
    \begin{column}{0.375\textwidth} 
    \scriptsize
    \tabulinesep=1ex
    \begin{tabu} to \textwidth {>{\bfseries}X | XXX}
    \firsthline
    \rowfont{\bfseries}
    material & $n$ & $h$ [mm] & mass [kg]\\
    \hline
    steel & - & 20.0 & 781 \\
    carbon & 44 & 39.6 & 313 \\
    \textcolor{blue}{aramid} & 61 & 54.9 & 343 \\
    \textcolor{red}{glass} & 53 & 47.7 & 453\\
    \lasthline
    \end{tabu}
    \newline
    The \textbf{carbon} bulkhead is lightest, although carbon is not the 
    lighter material
    \end{column}
    \begin{column}{0.625\textwidth}
    \centering
    \begin{tikzpicture}
        \begin{axis}[xmin=30,xmax=70,ymin=0,ymax=4,
                    xlabel= {$n$ [ ]},
                    ylabel={$\eta F_{max},\, K/\hat{K}$ [ ]},
                    width=\textwidth,
                    grid=major
                    ]
        %carbon
        \addplot[black, mark=o]%
        table[x index=0,y index=1]%
        {./plate/monolithic_design.txt};
        \addlegendentryexpanded{$\eta F_{max}$}
        \addplot[black, mark=square]%
        table[x index=0,y index=2]%
        {./plate/monolithic_design.txt};
        \addlegendentryexpanded{$K/\hat{K}$}
        %aramid
        \addplot[blue, mark=o]%
        table[x index=0,y index=3]%
        {./plate/monolithic_design.txt};
        \addplot[blue, mark=square]%
        table[x index=0,y index=4]%
        {./plate/monolithic_design.txt};
        %glass
        \addplot[red, mark=o]%
        table[x index=0,y index=5]%
        {./plate/monolithic_design.txt};
        \addplot[red, mark=square]%
        table[x index=0,y index=6]%
        {./plate/monolithic_design.txt};
        %horizontal line at y=1
        \draw[thick] (axis cs:\pgfkeysvalueof{/pgfplots/xmin},1) -- %
              (axis cs:\pgfkeysvalueof{/pgfplots/xmax},1);
        \end{axis}
    \end{tikzpicture}
    \end{column}
\end{columns}
\end{frame}
}
        
\mode<article>{
\item 
The best choice seem s carbon, which however, note gives rise to a plate 
twice as thick as the steel one.
For all the materials, the limiting design parameter is strength, not 
stiffness: the incredibly high resistances for fibres get quickly knocked down 
when the load is complex, due to the need to reduce the effective volume 
fraction.
see how limiting in this case is the low compression resistance for aramid
composites.
In spite of the fact that it is much less thick than the aramid composite, the 
glass bulkhead would be much 
}

\mode<presentation>{
\begin{frame}
\frametitle{What about a sandwich?}
A sandwich structure may be helpful, since the main loading is bending.
\begin{columns}[T,onlytextwidth]
\begin{column}{0.7\textwidth}
    \begin{itemize}
        \item As to elastic properties, can be treated just as another layer
        in the stack~\cite{Vasilev2001}.
        \item Failure mechanism are however more 
            complicated~\cite{carlsson2011structural}:
        \begin{itemize}
            \item easier global buckling;
            \item wrinkling (local buckling);
            \item delamination/debonding;
            \item puncturing/compression crushing.
        \end{itemize}
        \item Dealing with buckling failure types is not 
            easy~\cite{carlsson2011structural,Vonach2000}: here buckling will 
            be neglected.
    \end{itemize}
\end{column}
\begin{column}{0.25\textwidth}
    \begin{block}{Properties of a isotropic PVC foam}
        $E$ = \SI{95}{\mega\pascal}, $G$ = \SI{35}{\mega\pascal},
        $\nu$ = 0.33, $\rho$~= \SI{0.1}{\kilo\gram\per\cubic\deci\metre},
        $X$~= $Y$~=~\SI{3.5}{\mega\pascal}, $S$~= \SI{1.6}{\mega\pascal}
    \end{block}
\end{column}
\end{columns}
\begin{itemize}
    \item Failure of the core will be approximated using Tsai-Wu criterion
    although depending on the material (foam, honeycomb, etc.) better criteria
    can be devised (see e.g.~\cite{gibson1999cellular}).
\end{itemize}

\end{frame}
}

\mode<article>{
\item 
Buckling of the whole panel is somewhat easier than buckling of similar 
constructions without a soft core.
assume the foam as isotropic and symmetric with respect to 
tension/compression. We use a closed cell foams so that it is resistant to water
penetration.
}

\mode<presentation>{
\begin{frame}
\frametitle{Design equations}
The lamination sequence can now be expressed as 
\[
    [(0_{2}/45)_n/0_{h_{foam}}]_{S} 
\]
with two unknowns: $n \in \mathbb{N}$ and $h_{foam} \in \mathbb{R}^+$. The 
resistance and stiffness equations $f_i$ can be expressed as an equivalent 
minimisation problem:
\[
\left\lbrace n,h_{foam} \right\rbrace = 
    \underset{n,h_{foam}}{\operatorname{arg\,min}} \quad   
    \begin{bmatrix}
    \eta F-1\\
    K/\hat{K}-1
    \end{bmatrix},
\]
which for easiness can be recast in a least square sum form for 
the function $O=\frac{1}{2} \sum_{i=1}^{2} f_{i}^{\,2}$~\cite{Xu2009}
\[
\left\lbrace n,h_{foam} \right\rbrace = 
    \underset{n,h_{foam}}{\operatorname{arg\,min}} \quad  O 
    =
    \underset{n,h_{foam}}{\operatorname{arg\,min}} \quad   
    \frac{1}{2} \left[ (\eta F-1)^2 + (K/\hat{K}-1)^2 \right]
\]
\end{frame}
}

\mode<article>{
\item With $h_{foam}$ one should understand the total tickness of the 
foam layer, which is twice the one in the lamination sequence.
Watch-out to try to keep the two functions close to one so that one is not 
more important than the other in the minimisation process. Of course to be 
summed they must be normalised, which is easy for both, so that they become 
pure numbers.
}

\mode<presentation>{
\begin{frame}
\frametitle{Solution}
\begin{columns}[T]
    \begin{column}{0.4\textwidth}
    limit the search region to $n \in [30,50]$ and $h_{foam} \in [4,120]$
    \vspace{2ex}
    \begin{scriptsize}
    \tabulinesep=1ex
    \begin{tabu} to \textwidth {>{\bfseries}X[l] | XXXX}
    \firsthline
    \rowfont{\bfseries}
     & $n$ & $h_{foam}$ [mm] & $h$ [mm] & mass [kg]\\
    \hline
    carbon & 36 & 4.5 & 34.6 & 258 \\
    \textcolor{blue}{aramid} & 50 & 6.1 & 48.1 & 284 \\
    \textcolor{red}{glass} & 47 & 4 & 44.3 & 404 \\
    \lasthline
    \end{tabu}
    \end{scriptsize}
    \end{column}
    \begin{column}{0.6\textwidth}
        \centering
        $O$ [ ], aramid composite
        \begin{tikzpicture}
            \begin{axis}[width=0.75\textwidth,
                         height=\textwidth,
                         enlargelimits=false,
                         axis on top,
                         xlabel= {$n$ [ ]},
                         ylabel={$h_{foam}$ [mm]},
                         colorbar
                         ]
            \addplot[point meta min=0, point meta max=5] % 
            graphics[xmin=30,xmax=50,ymin=4,ymax=120]
            {./plate/objfunc_aramid.pdf};
            \end{axis}
        \end{tikzpicture}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item here we are treating n as a continuous variable
}

\mode<presentation>{
\begin{frame}
\frametitle{Some annotations on the solution:}
\begin{itemize}
    \item the plates are actually less thick than the corresponding ones 
    without a sandwich core, which were oversized w.r.t. stiffness;
    \item in solving the problem using the design equations $K/\hat{K}-1=0$ and 
    $\eta F-1=0$ we \emph{assumed without justification} that they would lead
    to the minimum weight objective even for the new stacking sequence;
    \item no  constraint on the total height: there was room to have 
    $K>\hat{K}$ or $F>1/\eta$ and a lighter structure using larger values 
    for $h_{foam}$;
    \item however this would require an approach to design more based on 
    optimisation techniques.
\end{itemize}

\end{frame}
}

\mode<article>{
\item in fact the one which gains the most from the sandwich is the aramid 
composite, which was the less resistant and thus the one which was really much
stiffer than needed, while glass, for which the stiffness and the resistance 
design equations were satisfied almost for the same $n$, is the one that gets 
less benefits from the addition of a sandwich layer.
The use of a sandwich structure in this case allowed a better exploitation of 
the resistance properties of composite materials.
}