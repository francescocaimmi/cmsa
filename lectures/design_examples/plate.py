#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Author: Francesco Caimmi francesco dot caimmi at polimi dot it



provides the calculations needed to solve the problem of a simply supporte
plate under hydrostatic load q = q0 *x/a.
We assume a symmetric cross-ply lamination sequence.
"""
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt

#this stuff imports modules that are used to perform the needed transformation
#of stresses and strains and to calculate the plate elastic properties
#theese needs to be defined separartely
from lib4g.composites.elastic import mat_trans_2Dvec_rot
from lib4g.composites.elastic import elastic_compliance_2D_pstress
from lib4g.composites.lamination_theory import clt_ABD
from lib4g.composites.failure import tsai_wu

#Units: N,mm, MPa
#
#material data
#
#steel
E1s,E2s,v12s,G12s = 210e3, 210e3, 0.3, 80.769e3
sigmay = 500.0
hsteel = 20.0
rhos = 7810
#geometry
a = 5e3
b = 1e3
#load
q0 = 0.49
cpt = 0.15 #cured ply thickness


def Fmax(n,X,Y,R,Q, Kout = False):
    """
    Calculates the Tsai-Wu failure index map for monolithic laminates

    Paramters
    ----------
    n: int
        the number of repaeting [0_2/45]n in the laminate

    X,Y: ndarray
        the problem grid

    R: ndarray
        the vector containing the resistance in the principal maerial
        reference systems and the interaction parameter F12
        {Xplus, Xminus, Yplus, Yminus, S, F12}. If F12 is given as None, it is
        calculated automatically as -1/sqrt(Xplus*Xminus*Yplus*Yminus)

    Q: ndarray
        the material stiffness matrix in the material reference system

    Kout: bool
        wether or not to return the matrix K which contains the values of
        the layers  where maxF is located

    Returns
    --------
    F: ndarray
        the maximum value of F calcultated at grid points
    K: ndarray, optional
        the maximum value of F calcultated at grid points
    """
    n = int(n)#sanitize input
    F = np.empty_like(X)
    if Kout:
        K = np.empty_like(X)

    hs = [2*cpt,cpt]*n
    hs = hs + hs[::-1]
    thetas = [0,45]*n
    thetas = thetas + thetas[::-1]
    htot = np.sum(hs)
    L = int(4*n)
    A,B,D = clt_ABD(hs, Q, thetas, symmetric = False)
    #layer interface positions
    Z = np.zeros(L+1)
    Z[0] = -htot/2.0
    for j in range(L):
        Z[j+1] = hs[j]+Z[j]

    for i in range(len(X)):
        for j in range(len(Y)):
            #calculate the stress and the value of F
            maxFgivenX = 0
            maxFgivenXlayer = 0 #the layer were the maximum is
            for k,theta in enumerate(thetas):
                ztop, zbottom = Z[k+1], Z[k]
                T = mat_trans_2Dvec_rot(np.deg2rad(theta))
                #top of the layer
                sigmatop = stress(X[i,j],Y[i,j],ztop,Q,D, q0)
                #rotate to the PMRS
                sigmap = np.dot(np.linalg.inv(T),
                                sigmatop)
                #calculate F
                tw = tsai_wu(sigmap, R)
                #take the maximum
                if tw>maxFgivenX:
                    maxFgivenX = tw
                    maxFgivenXlayer = k

                #bottom of the layer
                sigmabot = stress(X[i,j],Y[i,j],zbottom,Q,D, q0)
                #rotate to the PMRS
                sigmap = np.dot(np.linalg.inv(T),
                                sigmabot)
                #calculate F
                tw = tsai_wu(sigmap, R)
                if tw>maxFgivenX:
                    maxFgivenX = tw
                    maxFgivenXlayer = k

            F[i,j] = maxFgivenX
            if Kout:
                K[i,j] = maxFgivenXlayer

    if Kout:
        return F,K
    else:
        return F




def Qmn(m,n,q0):
    """
    Coefficients of Fourier expansion of the distributed load

    Parameters
    -----------

    n, m: int
        the number of terms to keep in the series.
    q0: float
        the external load

    """

    return 8*q0*np.cos(m*np.pi)/(np.pi**2*m*n)

def dmn(m,n,D):
    """
    Special coefficients of Fourier expansion of the displacements.
    See the book by Reddy,page 248

    Parameters
    -----------

    n, m: int
        the number of terms to keep in the series.
    D: nd array
        bending stiffness matrix

    """
    s = b/a
    return (np.pi/b)**4*(D[0,0]*m**4*s**4+2*(D[0,1]+2*D[2,2])*m**2*n**2*s**2+\
                        D[1,1]*n**4)

def moments(x,y,D,q0, n = 31, m = 31):
    """
    Moment stress resultants acting in the plate

    Parameters
    -----------
    x: float
        coordinate along a
    y: float
        coordinate along b
    D: nd array
        bending stiffness matrix
    q0: float
        the external load

    n, m: int
        the number of terms to keep in the series. Defaults to 21.

    Returns
    --------
    M: ndarray
        an array with the moment components Mx,My,Mxy

    """

    Mx = 0
    My = 0
    Mxy = 0
    for i in range(1,n,2):#only odd terms for hydro loading
        for j in range(1,m,2):
            alpha = i * np.pi/a
            beta = j * np.pi/b
            Wmn = Qmn(i,j,q0)/dmn(i,j,D)
            Mx += (D[0,0]*alpha**2+D[0,1]*beta**2)*(Wmn)*\
                    np.sin(alpha*x)*np.sin(beta*y)
            My += (D[0,1]*alpha**2+D[1,1]*beta**2)*(Wmn)*\
                    np.sin(alpha*x)*np.sin(beta*y)
            Mxy += (alpha*beta*D[2,2])*Wmn*\
                    np.cos(alpha*x)*np.cos(beta*y)

    Mxy = -2*Mxy
    return np.stack((Mx,My,Mxy))


def stress(x,y,z,Q,D,q0,n = 31, m = 31):
    """
    Gives the stresses for given M, Q ina layer

    x: float
        coordinate along a

    y: float
        coordinate along b

    z:float
        the position through the thickness

    M:nd array
        the bending moments

    Q:ndarray
        the layer stiffness matrix

    q0: float
        the external load

    Returns
    ---------
    sigma:nd array
        the in plane stress vector

    """
    sx = 0
    sy = 0
    sxy = 0
    for i in range(1,n,2):#only odd terms for hydro loading
        for j in range(1,m,2):
            alpha = i * np.pi/a
            beta = j * np.pi/b
            Wmn = Qmn(i,j,q0)/dmn(i,j,D)
            sx += (Q[0,0]*alpha**2+Q[0,1]*beta**2)*Wmn*\
                    np.sin(alpha*x)*np.sin(beta*y)
            sy += (Q[0,1]*alpha**2+Q[1,1]*beta**2)*Wmn*\
                    np.sin(alpha*x)*np.sin(beta*y)
            sxy += -2*(alpha*beta*Q[2,2])*Wmn*\
                    np.cos(alpha*x)*np.cos(beta*y)

    sigma = np.stack((sx,sy,sxy))
    return z*sigma




def w0(x,y,D,q0, n = 31, m = 31):
    """
    Beam deflection along Z

    Parameters
    -----------
    x: float
        coordinate along a
    y: float
        coordinate along b
    D: nd array
        bending stiffness matrix
    q0: float
        the external load

    n, m: int
        the number of terms to keep in the series. Defaults to 21.
    """

    w = 0
    for i in range(1,n,2):#only odd terms for hydro loading
        for j in range(1,m,2):
            alpha = i * np.pi/a
            beta = j * np.pi/b
            w = w + Qmn(i,j,q0)*np.sin(alpha*x)*np.sin(beta*y)/dmn(i,j,D)
    return w



def main():
    """
    Main program
    """
    #Calculate the steel plate stress stiffness matrix
    Cs = elastic_compliance_2D_pstress(E1s,E2s,v12s,G12s)
    Qs = np.linalg.inv(Cs)
    A, _, D = clt_ABD([hsteel/2.0],Qs,[0])
    #calculate the stresses and the stress resultants on a grid
    X, Y  = np.meshgrid(np.linspace(0,a),np.linspace(0,b))
    MX = np.empty_like(X)
    MY = np.empty_like(X)
    MXY = np.empty_like(X)
    SX = np.empty_like(X)
    SY = np.empty_like(X)
    SXY = np.empty_like(X)
    Smises = np.empty_like(X)
    for i in range(len(X)):
        for j in range(len(Y)):
            ms = moments(X[i,j],Y[i,j],D, q0)
            sigma = stress(X[i,j],Y[i,j],-hsteel/2.0,Qs,D,q0)
            MX[i,j], MY[i,j] , MXY[i,j] = ms
            SX[i,j], SY[i,j] , SXY[i,j] = sigma

    Smises = np.sqrt(SX**2+SY**2-SX*SY+3*SXY**2)
    #the maximum stress is at (a/2,b/2) and is given by .
    ms=stress(a/2,b/2,-hsteel/2.0,Qs,D,q0)
    maxmises = np.sqrt(ms[0]**2+ms[1]**2-ms[0]*ms[1]+3*ms[2]**2)
    ms=stress(0,0,-hsteel/2.0,Qs,D,q0)
    corner_mises = np.sqrt(ms[0]**2+ms[1]**2-ms[0]*ms[1]+3*ms[2]**2)
    max_w = w0(a/2.0,b/2.0,D, q0)
    P = b*a*q0/2
    Khat = P/abs(max_w)
    #evaluate the securiti coeffiecient
    eta = sigmay/maxmises
    print("Data for the steel plate (N,mm,MPa)")
    print(40*'#')
    print("Plate weigth",a*b*hsteel*rhos)
    print('Maximum Mises Stress', maxmises)
    print ('Mises stress at the corners:',corner_mises )
    print("Security coefficient eta: ",eta )
    print('Maximum displacement at the center:',max_w)
    print('Tranverse force resultant', P)
    print('Stiffness F/w_max:',Khat)
    print('Plotting the stress resultant data')
    print(40*'#')
    #prepare plots of the stress resultants to get an idea of the stress state
    mymap=plt.cm.gnuplot
    for data,name in [(MX,'MX'), (MY,'MY'),(MXY,'MXY'),(Smises,'SMISES')]:
        fig = plt.figure(figsize=(5,2))
        plt.contourf(X,Y,data, cmap=mymap)
        plt.colorbar()
        plt.savefig('plate/'+name+'f.pdf',bbox_inches='tight')
        fig = plt.figure(figsize=(5,2))
        plt.contourf(X,Y,data, cmap=mymap)
        plt.xticks([])
        plt.yticks([])
        plt.savefig('plate/'+name+'.pdf',bbox_inches='tight',pad_inches=0)
        plt.close()
    #calculate the steel displacment distribution with less points
    #for pgfplots
    X1, Y1 = np.meshgrid(np.linspace(0,a,25),np.linspace(0,b,25))
    W = np.empty_like(X1)
    for i in range(len(X1)):
        for j in range(len(Y1)):
            W[i,j] = w0(X1[i,j],Y1[i,j],D, q0)

    #export the steel plate displacement data, with position in meters
    out = np.column_stack((X1.reshape(25*25)/1e3,
                           Y1.reshape(25*25)/1e3,
                           W.reshape(25*25)))
    np.savetxt('plate/w_steel.txt', out, delimiter='\t')
    ###########################################################################
    #make the calculations for the case of a monolithic composite
    ###########################################################################
    #points were F is to be evaluated
    xs = (0,a/2.0)
    ys = (0,b/2.0)
    px, py = np.meshgrid(xs, ys)
    #make the calculations for the carbon material
    print("Data for the carbon composite (N,mm,MPa)")
    E1,E2,v12,G12 = 67e3, 66e3, 0.1, 5.8e3
    Cc = elastic_compliance_2D_pstress(E1,E2,v12,G12)
    Qc = np.linalg.inv(Cc)
    Rc =[876.0, 924.0, 800.0, 840.0, 70.0, None]
    rhoc = 1580

    #now let's calculate F and stiffness for various values of n
    ns = np.arange(30,70,dtype='int')
    maxFcarbon = np.empty_like(ns, dtype='float')
    stiffnesscarbon = np.empty_like(ns, dtype='float')
    for index,n in enumerate(ns):
        #tsai-wu failure index
        F = Fmax(n,px,py,Rc,Qc)
        maxFcarbon[index] = np.max(F)*eta
        #stiffness
        hs = [2*cpt,cpt]*n
        thetas = [0,45]*n
        A,B,D = clt_ABD(hs, Qc, thetas)
        stiffnesscarbon[index] = abs(P/w0(a/2.0,b/2.0,D,q0))/Khat



    monolith = plt.figure()
    plt.plot(ns,maxFcarbon,'ko')
    plt.plot(ns,stiffnesscarbon,'ko')
    plt.plot(ns,np.ones(ns.shape),'k')
    plt.xlabel('$n$ [ ]')
    plt.ylabel('$\eta F_{max},\\quad K/\\hat{K}$ [ ]')

    ncarbon = 44
    hcarbon = ncarbon*cpt*3*2
    print("Carbon number of layers n:44")
    print("Carbon lamnate height:", hcarbon)
    print("Carbon mass:",rhoc*a*b*hcarbon/1e9)
    print(40*'#')

    print("Data for the aramid fibre composite (N,mm,MPa)")
    E1,E2,v12,G12 = 35e3, 34e3, 0.15, 5.6e3
    Ca = elastic_compliance_2D_pstress(E1,E2,v12,G12)
    Qa = np.linalg.inv(Ca)
    Ra =[600.0, 150.0, 500.0, 150.0, 44.0, None]
    rhoaramid = 1250


    #now let's calculate F and stiffness for various values of n
    maxFaramid = np.empty_like(ns, dtype='float')
    stiffnessaramid = np.empty_like(ns, dtype='float')
    for index,n in enumerate(ns):
        #tsai-wu failure index
        F = Fmax(n,px,py,Ra,Qa)
        maxFaramid[index] = np.max(F)*eta
        #stiffness
        hs = [2*cpt,cpt]*n
        thetas = [0,45]*n
        A,B,D = clt_ABD(hs, Qa, thetas)
        stiffnessaramid[index] = abs(P/w0(a/2.0,b/2.0,D,q0))/Khat

    plt.plot(ns,maxFaramid,'bo')
    plt.plot(ns,stiffnessaramid,'bo')
    naramid = 61
    haramid = naramid*cpt*3*2
    print("Aramid number of layers n:", naramid)
    print("Aramid lamnate height:", haramid)
    print("Aramid mass:",rhoaramid*a*b*haramid/1e9)
    print(40*'#')
    #calculations for the glass material
    print("Data for the glass fibre composite (N,mm,MPa)")
    E1,E2,v12,G12 = 26e3, 22e3, 0.13, 7.2e3
    Cg = elastic_compliance_2D_pstress(E1,E2,v12,G12)
    Qg = np.linalg.inv(Cg)
    Rg =[520.0, 490.0, 400.0, 340.0, 40.0, None]
    rhoglass =1900

    #now let's calculate F and stiffness for various values of n
    maxFglass = np.empty_like(ns, dtype='float')
    stiffnessglass = np.empty_like(ns, dtype='float')
    for index,n in enumerate(ns):
        #tsai-wu failure index
        F = Fmax(n,px,py,Rg,Qg)
        maxFglass[index] = np.max(F)*eta
        #stiffness
        hs = [2*cpt,cpt]*n
        thetas = [0,45]*n
        A,B,D = clt_ABD(hs, Qg, thetas)
        stiffnessglass[index] = abs(P/w0(a/2.0,b/2.0,D,q0))/Khat

    plt.plot(ns,maxFglass,'ro')
    plt.plot(ns,stiffnessglass,'ro')
    nglas = 53
    hglass = nglas*cpt*3*2
    print("Glass number of layers n:", nglas)
    print("Glass lamnate height:", hglass)
    print("Glass mass:",rhoglass*a*b*hglass/1e9)
    #make some plots to show that calculating the maximum value of F may be non
    #trivial
    #for the case n=1 plot fmax distribution
    maxFdist,layers = Fmax(1,X,Y,Rg,Qg, Kout = True)
    fig = plt.figure(figsize=(5,2))
    plt.contourf(X,Y,maxFdist, cmap=mymap)
    plt.colorbar()
    plt.savefig('plate/Fdistf_n1.pdf',bbox_inches='tight')
    fig = plt.figure(figsize=(5,2))
    plt.contourf(X,Y,maxFdist, cmap=mymap)
    plt.xticks([])
    plt.yticks([])
    plt.savefig('plate/Fdist_n1.pdf',bbox_inches='tight',pad_inches=0)
    #for the case n=10 plot fmax distribution
    maxFdist,layers = Fmax(10,X,Y,Rg,Qg, Kout = True)
    fig = plt.figure(figsize=(5,2))
    plt.contourf(X,Y,maxFdist, cmap=mymap)
    plt.colorbar()
    plt.savefig('plate/Fdistf_n10.pdf',bbox_inches='tight')
    fig = plt.figure(figsize=(5,2))
    plt.contourf(X,Y,maxFdist, cmap=mymap)
    plt.xticks([])
    plt.yticks([])
    plt.savefig('plate/Fdist_n10.pdf',bbox_inches='tight',pad_inches=0)

    #output the relevant data for F and stiffness to a file
    out = np.column_stack((ns,
                           maxFcarbon,stiffnesscarbon,
                           maxFaramid,stiffnessaramid,
                           maxFglass,stiffnessglass,
                           ))

    np.savetxt('./plate/monolithic_design.txt', out)

    plt.figure(num=monolith.number)
    plt.savefig('./plate/monolithic_design.pdf', bbox_inches='tight')

    print(40*'#')
    print('Data for sandwich panels')
    #sandwich properties
    E1,E2,v12,G12 = 0.95, 0.95, 0.035, 0.0


if __name__=="__main__":
    main()
