%kate: default-dictionary en_GB; 
\mode<presentation>{
\begin{frame}
\frametitle{Preliminary design of a composite barrel}
\begin{figure}
    \centering
    \includegraphics[width=0.85\textwidth]{../../images/M256.png}
    \caption*{Scheme of the Rheinmetall 120 L/44}
\end{figure}

Design a replacement barrel for the Rheinmetall 120 L/44 using a composite 
wrapping in order to minimise the barrel mass; the new barrel shall have the 
same bending stiffness of the steel barrel. A steel liner must be included in 
the design. If possible, produce the wrapping by filament winding.
\end{frame}
}

\mode<article>{
\item this is a smooth-bore gun and has been the main gun for most of 
    the western MBTs from the seventies well into the nineties. 
    The steel liner is needed since at the moment no other material can provide 
    the same resistance to abrasion.
    The bending stiffness is crucial for shooting precision.
    We are neglecting buckling, which is important, and we are neglecting 
    pre-tensioning of the composite wrapping which could in principle be 
    exploited to overcome some problems with thermal expansion and to induce a 
    compressive stress state in the steel liner, which is beneficial w.r.t. to 
    the resistance.
}

\mode<presentation>{
\begin{frame}
\frametitle{Analysis of the steel barrel}
The internal diameter of the barrel is $D_i=$\SI{120}{\milli\metre}; the length 
is 44 times the calibre(\SI{5280}{\milli\metre}). 
Weight: $m=$\SI{1190}{\kilogram}.
Steel barrel: approximated to a constant 
cross section cylinder, external diameter $D_e=$\SI{226}{\milli\metre}.
\begin{columns}[T]
    \begin{column}{0.4\textwidth}
        \begin{itemize}
            \item two load cases: projectile leaving the muzzle/tip load
            \item complex internal pressure distribution: 
                linear approximation\cite{Tierney2005}.
            \item complex temperature distribution: assume a 
                jump $\Delta T=$\SI{150}{\degree\kelvin}, constant in space
                \cite{South2005}.
            \item bending stiffness (via FEM): 
                $\mathcal{D}=$\SI{490}{\newton\per\milli\metre}.
        \end{itemize}
    \end{column}
    \begin{column}{0.6\textwidth}
        \centering
        \begin{tikzpicture}
            \begin{axis}[xlabel={$X$ [m]}, 
                         ylabel={$\textcolor{blue}{p}$ [MPa]},
                         scale only axis,
                         width=0.7\linewidth, height = 0.5\linewidth,
                         xmin= -1, xmax=6,
                         ymin=0,
                         xtick={0,5.28},
                         xticklabels={breech,muzzle},
                         xmajorgrids=true
                        ]
                \addplot [blue,nodes near coords, mark=o,
                          point meta=explicit symbolic]
                table[x=a,y=b,meta=label,row sep=\\]
                {
                a       b       label\\
                0.0     620.0   620\\
                5.28    116.0   116\\
                };
            \end{axis}
        \end{tikzpicture}

    \end{column}
\end{columns}

\end{frame}
}

\mode<article>{
\item in actual barrels the cross section is not constant, since the pressure
    profile is not constant and we have a minimum pressure at the projectile 
    position, and so at the muzzle, and a maximum pressure at the breech 
    (culatta).
}

\mode<presentation>{
\begin{frame}
\frametitle{Wrapping scheme}
\begin{figure}
    \centering
    \def\svgwidth{\textwidth}
    \input{../../images/composite_wrapping_barrel.pdf_tex}
    \caption*{Wrapped barrel scheme. Drawing not to scale.}
\end{figure}
\begin{itemize}
    \item Simplified wrapping with three regions.
    \item Main issues: pressure and CTE mismatch.
    \item Need layers at $\pm\phi$ for low $\phi$: bending stiffness.
\end{itemize}

\end{frame}
}

\mode<article>{
\item we fix a lot of variables to make the problem easier and use this very 
simplified scheme in which the wrappegae is made of three regions with 
different thickness, but of course one can devise more complicated schemes.
}
\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{itemize}
    \item Need layers at $90$ for pressure.
    \item Exploit the results for optimal pressure vessels.
    \item $\phi_a=\phi_b$ for easiness, $\phi_c=$\SI{90}{\degree}.
    \item in regions $a$ and $b $, the relationship between the thickness of 
    each layer is         given by  $\frac{h_{90}}{h_{\phi}} = \frac{1}{2} (3 
    \cos^2 \phi -1),         \phi<55^\circ$.
    \item $h_a=h_b$, $h_c=$\SI{10}{\milli\metre}
    \item design space $\mathcal{A}$ for the unknowns 
        $\bm{u}=\lbrace h_s, h_a,\phi_a, X_b \rbrace $:
        \begin{empheq}[box=\fbox]{alignat*=2}
            %line 1
            8 \leq h_s \leq 35 \qquad & 12.5 \leq h_a < 50\\
            %line 2
            15^\circ \leq \phi_a \leq 45^\circ 
            \qquad & 1400 \leq X_b \leq 5000
        \end{empheq}
    \item this a very simplified design exercise to keep limited the number of 
    simulations needed so that it can be reproduced on a laptop overnight, but 
    the technique can be seamlessly extended to every number of design 
variables 
\end{itemize}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{FEM models}
The problem has variable cross-section: complex to solve $\Rightarrow$ use FEM 
to evaluate the elastostatic fields.
\begin{block}{Quarter model for temperature and pressure load}
    \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]
    {../../images/wrapped_cylinder_symmetric_model.png}
    % wrapped_cylinder_symmetric_model.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \end{figure}
    Barrel is buit-in at the breech. Uniform temperature change equal to
    $\Delta T =$\SI{150}{\degreeCelsius}. Neglect residual stresses due to 
    manufacturing.
\end{block}

\end{frame}
}

\mode<article>{
\item We simplify the thermal load  as a uniform temperature jump, but of 
course the temperature variation is of course more complicated during the firing
and the usage of the barrel. Also we neglect thermal stresses arasing due to 
manufacturing, which can be very high especially for metal matrix composites 
and can come into play in a very complicated way.
}


\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{block}{Half model for bending load}
    \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]
    {../../images/wrapped_cylinder_half_model.png}
    % wrapped_cylinder_symmetric_model.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \end{figure}
    Concentrated force at muzzle. Isothermal.
\end{block}

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
    \begin{itemize}
        \item composite region meshed with continuum shell elements: allow 
            easy estimates for the interlaminar stresses.
        \item the model is implemented using the commercial finite element code
         Abaqus\texttrademark{}; click on the pin to get the Abaqus model. 
        \attachfile{barrel.cae}.
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]
        {../../images/wrapped_barrel_quarter_mesh.png}
    % wrapped_barrel_quarter_mesh.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
        \caption*{Quarter model, mesh. Detail near the end of region b.}
    \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{Materials}
The materials shall have service temperatures greater than 
\SI{300}{\degreeCelsius}.
\begin{block}{Thermo-elastic properties}
\tabulinesep=1ex
\begin{center}
\scriptsize
\begin{tabu} to \textwidth {X|XXX}
\firsthline
%first line$
\rowfont{\bfseries}
Property & Maraging Steel & Carbon/PEEK & SiC/Ti~\cite{Chamis20062395}\\
\hline
$\rho$ [\si{\kilogram\per\cubic\metre}] & 7800 & 1580 & 4207\\
$E_1$ [\si{\mega\pascal}]   & 210e3 & 177e3 & 184e3\\
$E_2=E_3$ [\si{\mega\pascal}]& - & 9.4e3 & 123e3\\
$\nu_{21}=\nu_{31}$ & 0.29 & 0.30 & 0.255\\
$\nu_{23}$ & - & 0.25 & 0.171\\
$G_{12}=G_{13}$ [\si{\mega\pascal}] & 81e3 & 4.7e3 & 58e3\\
$G_{23}$ [\si{\mega\pascal}] & - & 3.8e3 & 50e3\\
$\alpha_{11}$ [\si{\per\kelvin}] & 1.12e-5 & -4.06e-6 & 8.24e-6\\
$\alpha_{22}=\alpha_{33}$ [\si{\per\kelvin}] & - & 12e-6 & 1e-5\\
\lasthline
\end{tabu}
\end{center}
\end{block}
\end{frame}
}

\mode<article>{
\item properties for 
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\begin{block}{Failure properties}
\tabulinesep=1ex
\begin{center}
\scriptsize
\begin{tabu} to \textwidth {X|XXX}
\firsthline
%first line$
\rowfont{\bfseries}
Property & Maraging Steel & Carbon/PEEK & SiC/Ti~\cite{Chamis20062395}\\
\hline
$\sigma_y$ [\si{\mega\pascal}] & 1400 & - & -\\
$X^+$ [\si{\mega\pascal}] & - & 2130 & 1344\\
$X^-$ [\si{\mega\pascal}]   & - & 1100 & 1331\\
$Y^+$ [\si{\mega\pascal}]   & - & 80 & 446\\
$Y^-$ [\si{\mega\pascal}]   & - & 200 & 701\\
$S$ [\si{\mega\pascal}]   & - & 160 & 300\\
$Z^+$ [\si{\mega\pascal}]   & - & 30 & 150\\
$I$ [\si{\mega\pascal}]   & - & 35  & 250\\
\lasthline
\end{tabu}
\end{center}
\end{block}
$Z^+$ and $I$ are the interlaminar normal and shear strengths.We are assuming 
the same interlaminar behaviour at both composite/composite interfaces and at 
steel/composite interfaces.
Approximate the SiC/Ti behaviour as linear elastic (very rough).
\end{frame}
}

\mode<article>{
\item while the resistance along the fibre direction is much lower for the 
SiC/Ti composite, it is less anisotropic when it comes to resistance, and this 
can have an influence on the overall behaviour; in addition its thermal 
expansion is closer to that of steel, so from this point of view thermal 
stresses should be less problematic, so in the end it may have a chance in 
spite of the relatively higher density. As to failure, the Ti material of 
course will fail by matrix yielding; here we neglect the associated plasticity 
and assume it has a linear elastic behaviour.
}

\mode<presentation>{
\begin{frame}
\frametitle{Objective and constraint functions approximation}
The objective function for the problem is the barrel weight:
\[
    W(\bm{u}) = \sum_i W_i
      = \sum_i \rho_i \frac{\pi X_i}{4} (D_{e,i}^2-D_{i,i}^2) 
\]
with  $i \in \lbrace s,a,b,c \rbrace$, $X_i$= length along X of the region 
$i$.
The constraint  vector $\bm{g}$ components are:
    \begin{enumerate}
        \item bending stiffness $\mathcal{D}$ constraint:
            \[
                \mathcal{D}(\bm{u}) \geq 490 \Leftrightarrow
                g_1 = 490 - \mathcal{D}(\bm{u}) \leq 0;
            \]
        \item resistance constraint
            \[
                g_2 = \max(F_c, F_s, F_{int}) -1 \leq 0
            \]
            where $F_c$ is the Tsai-Wu failure index for composite regions,
            $F_s$ is the normalised Von-Mises criterion for the steel region
            and $F_{int}$ is the interlaminar failure criterion given by
    \end{enumerate}

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\[
    \frac{\sigma_z}{Z^+}  + \left(\frac{\tau_r}{I}\right)^2 = 1 
\]
Each failure criterion is to be understood as its maximum value at varying the 
spatial position.\\
\end{frame}
}

\mode<article>{
\item these problems are very complicated!
}

\mode<presentation>{
\begin{frame}
\frametitle{}
For each set of design parameters $\bm{u}$ evaluating $W$ and $\bm{g}$ 
requires to \textcolor{red}{solve two elastic problem via FEM}. \\
This can be \textcolor{red}{very cumbersome}, 
involving thousands of simulations, depending on $\mathcal{A}$ size, the 
solution algorithm and the technique for the jacobian $\partial 
W/\partial \bm{u}$ calculation (if needed).\\

\begin{center}
\begin{tikzpicture}
\pgfmathsetmacro{\ns}{46}
\pgfmathsetmacro{\zlow}{-0.8}
\begin{axis}[xlabel= {$x$},ylabel={$y$},zlabel={$z$},
             xtick=\empty,ytick=\empty,ztick=\empty,
             width=0.7\textwidth,
             zmin=\zlow,zmax=0.6,
%              colormap/bluered
             ]
    \addplot3[surf,
              samples=\ns,domain=-2:2,y domain=-2:2]
        {x*exp(-x^2-y^2)};
    \addplot3[contour gnuplot={output point meta=rawz,number=12,labels=false},
              z filter/.code={\def\pgfmathresult{\zlow}},
              samples=\ns,domain=-2:2,y domain=-2:2]
        {x*exp(-x^2-y^2)};
    %these coordinates were generate with the 'TNC' scipy minimize solver
    \addplot3[ForestGreen,thick,smooth] %
        coordinates{
        (0.5, -0.80000000000000004, 0.20532787637617272)
        (0.062500014045719365, -1.5, 0.0065617709585633595)
        (-0.635496071198134, -1.5, -0.044725806062936121)
        (-1.0317051390807546, -1.3214528246776642, -0.062073249138664459)
        (-1.4128459883849387, -0.53628672343047346, -0.14397276416322252)
        (-1.3945454952375225, 0.12682998075488888, -0.19626795619201201)
        (-0.81509683488413853, -0.30482494470678068, -0.38222361802645249)
        (-0.62990608446741958, -0.12643678018037718, -0.41688183042690258)
        (-0.71496987315389293, 0.041243585806172336, -0.42810027412041129)
        (-0.70649431992589717, -0.000509033121889867, -0.42888150950321341)
        (-0.70717402225098625, -0.00022669815982201527, -0.42888191656109242)
        (-0.70712532089123226, -4.2478493705217532e-06, -0.42888194217778602)
        (-0.70710646373210451, -2.0944388467732791e-06, -0.42888194247838557)
        };
    \addplot3[ForestGreen,thick,smooth,%
              z filter/.code={\def\pgfmathresult{\zlow}}] coordinates{
        (0.5, -0.80000000000000004, 0.20532787637617272)
        (0.062500014045719365, -1.5, 0.0065617709585633595)
        (-0.635496071198134, -1.5, -0.044725806062936121)
        (-1.0317051390807546, -1.3214528246776642, -0.062073249138664459)
        (-1.4128459883849387, -0.53628672343047346, -0.14397276416322252)
        (-1.3945454952375225, 0.12682998075488888, -0.19626795619201201)
        (-0.81509683488413853, -0.30482494470678068, -0.38222361802645249)
        (-0.62990608446741958, -0.12643678018037718, -0.41688183042690258)
        (-0.71496987315389293, 0.041243585806172336, -0.42810027412041129)
        (-0.70649431992589717, -0.000509033121889867, -0.42888150950321341)
        (-0.70717402225098625, -0.00022669815982201527, -0.42888191656109242)
        (-0.70712532089123226, -4.2478493705217532e-06, -0.42888194217778602)
        (-0.70710646373210451, -2.0944388467732791e-06, -0.42888194247838557)
        };
\end{axis}
\end{tikzpicture}
\end{center}

\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
\textcolor{red}{Computational cost reduction}:
\textcolor{red}{sample the design space} $\mathcal{A}$ to get 
$\bm{g}$ at a finite number of points $\textcolor{blue}{\bm{u}_k}$ ($W$ can 
be calculated analytically). 
Sampling each design variable at \textbf{seven points} $\Rightarrow$ $k \in 
[1,2401]$ and 4802 simulations.
{\centering
\begin{tikzpicture}[font=\scriptsize]
    \matrix (m) [matrix of nodes, column sep=4ex,%
                 nodes in empty cells=true,%
                 nodes={text width=0.25\textwidth,
                        anchor=center,}
                 ]
    {
    %line 1
    Design space $\mathcal{A}$ hypersection for fixed $h_a,X_b$ 
    \pgfmatrixnextcell
    Tsai-Wu failure index distribution
     \pgfmatrixnextcell\\
    %line 2
     \pgfmatrixnextcell 
     |[draw,text width=0.4\textwidth]|
    \includegraphics[width=\textwidth]
    {../../images/wrapped_barrel_tsaiw.png} 
     \pgfmatrixnextcell %|[draw,text width=3ex]| 
     $F_{c,k}, F_{s,k}, F_{int,k}$\\
    %line 3
     \pgfmatrixnextcell 
     displacement distribution
     \pgfmatrixnextcell\\
    %line 4
     \pgfmatrixnextcell 
     |[draw,text width=0.4\textwidth]|   
     \includegraphics[width=\textwidth]
     {../../images/wrapped_barrel_vdisp.png} 
     \pgfmatrixnextcell $\mathcal{D}_k$\\
    };
\input{design_space_sampling.tex}
\draw[-latex] (n1)--(m-4-2.west);
\draw[-latex] (n1)|-(m-2-2.west);
\draw[-latex] (m-2-2.east)--(m-2-3.west);
\draw[-latex] (m-4-2.east)--(m-4-3.west);
\end{tikzpicture}
}
\end{frame}
}

\mode<article>{
\item nothing
}

\mode<presentation>{
\begin{frame}
\frametitle{}
given $\bm{g}_k$ at $k$ points \textcolor{red}{construct an 
approximate interpolating function},using the radial 
basis function method (e.g.~\cite[\S 3.7.1]{Press2007}), without smoothing.
\begin{center}
\begin{tikzpicture}[font=\scriptsize]
\tikzstyle{every pin}=[fill=white,draw]
    \matrix (m) [matrix of nodes, column sep=6ex,%
                 nodes in empty cells=true,%
                 nodes={anchor=center,}
                 ]
    {
    %line 1
     $F_{c,k}, F_{s,k}, F_{int,k}$
     \pgfmatrixnextcell
     |[text width=0.6\textwidth,draw]|
     {\centering 
      \textcolor{Green}{radial basis} \textcolor{red}{interpolant}
      for function $f$ sampled at \textcolor{blue}{$(\bm{u}_j,f_j)$}
      \[
          f(\bm{u}) \approx \textcolor{red}{f^*}(\bm{u}) = 
          \sum_{j=1}^{n}  \textcolor{Green}{\lambda_j \omega}
           (\lVert \bm{u} - \textcolor{blue}{\bm{u}_j}\rVert)
          \text{ with } \textcolor{Green}{\omega(r)=e^{-r^{\,2}/\epsilon^{\,2}}}
        \]
      }\\
    %line 2
    $\mathcal{D}_k$
    \pgfmatrixnextcell
    |[text width=0.7\textwidth,text height=0.35\textwidth]|
    \\
    };
%radial basis coefficeints and values from 
%https://scholar.sun.ac.za/handle/10019.1/2002
\begin{axis}[at=(m-2-2.center), anchor=center,
             width=0.7\textwidth, height=0.48\textwidth,
             xlabel={$u$}, ylabel={$f(u)$},
             xmin=0,xmax=10
             ]
%initial function
\addplot[domain=0:10,samples=150,black,thick]
{5*(exp(-x)-1)+exp(-x/10)*sin(deg((x^2)/10))+5}%
node[coordinate,pos=0,pin=right:$f(u)$]{};
%sample points
\addplot[mark=o,only marks,blue,thick] coordinates
{(0.20000000000000001, 4.0975745496276916)
(1.5714285714285716, 1.2476321711092169)
(2.9428571428571435, 0.83114155958398772)
(4.3142857142857149, 0.68924222421207215)
(5.6857142857142868, -0.03457485517731218)
(7.0571428571428587, -0.47183187323149678)
(8.4285714285714288, 0.31609857311630662)
(9.8000000000000007, -0.06662724759207439)};
\node (fj) [coordinate,pin={[draw=blue]above:$\textcolor{blue}{f_j}$}] at %
(axis cs:8.4285714285714288, 0.31609857311630662){};
\node[coordinate] (xj) [pin={[draw=blue]above right:$\textcolor{blue}{u_j}$}] %
at (axis cs:8.4285714285714288, 0){};
\draw[thin] (xj.center)--(fj.center);
%interpolated function
\addplot[red,thick] table[col sep=tab]{wrapped_barrel_interp_function.txt}%
node[coordinate,pos=0.45,pin=above:$\textcolor{red}{f^*}$]{};
%basis functions
\addplot[domain=0:10,samples=100, thin, densely dashed,Green]%
{4.01565696008 * exp(-(x-0.2)^2)}% 
node[pos=0.1,pin={[solid, pin edge={solid}]right:$\textcolor{Green}{\lambda_j %
\omega( \lVert  u-u_j\rVert)}$}, coordinate]  {};
\foreach \lam/\xi in %
    {0.534958623695 / 1.57142857143,%
    0.656554274078 / 2.94285714286,0.596063355463 / 4.31428571429,%
    -0.0454310212154 / 5.68571428571,-0.528662612402 / 7.05714285714,%
    0.41652315846 / 8.42857142857,-0.129847025129 / 9.8}
{
    \addplot[domain=0:10,samples=100, thin, densely dashed,Green]%
    {\lam * exp(-(x-\xi)^2)};
}
\end{axis}
%arrow decorations
\draw[-latex] (m-1-1.east)--(m-1-2.west)node[pos=0.5](ni){};
\draw[-latex] (m-2-1.east)--(m-2-1.east-|ni) |- (m-1-2.west);
\draw[latex-] (m-1-1.west)--++(-0.4,0)node(nj){};
\draw[latex-] (m-2-1.west)--(nj|-m-2-1);
\end{tikzpicture}
\end{center}
Procedures like this usually \textcolor{red}{trade accuracy for speed}.
\end{frame}
}

\mode<article>{
\item without smoothing means that the interpolation will pass exactly 
through the nodes. Radial basis are a convenient choice in multiple dimensions,
however some care must be taken in normalising the design space range to cope 
with the very different dimensions involved, and with the interpolation near 
the boundaries, which can present some glitches as you see for example here.
}


\begin{frame}[fragile]
\frametitle{}
The generation of samples should be automatic; with ABAQUS this is easy to do 
using the ABAQUS Python scripting interface~\cite{Systemes2017}.
Attached to the slides there are:
    \begin{itemize}
        \item the script used to generate the FE models and run the analysis 
        (here \attachfile{wrapped_barrel_create_obj.py}). Run with 
         \verb+abaqus cae noGUI=wrapped_barrel_create_obj.py -- <options>+;
        \verb+abaqus cae noGUI=wrapped_barrel_create_obj.py -- -h+ will print
        an help message.
        \item the data files generated by the previous script, as tab separated
        values, for:
        \begin{itemize}
            \item the carbon/PEEK material    
                \attachfile{wrapped_barrel_obj_carbon.txt}.
            \item the SiC/Ti material 
                \attachfile{wrapped_barrel_sic_carbon.txt}.
        \end{itemize}
        \item a jupyter notebook for the Python 2.7 kernel to visualise the
            constraint functions, perform the interpolation and the optimisation
            \attachfile{wrapped_barrel_optimisation.ipynb}. 
    \end{itemize}
so that if you want you can reproduce all the analysis steps.
The optimisation is carried out using a non-sorting genetic algorithm, NSGAII, 
by Deb \textit{et al.}~\cite{ngsa2}.
\end{frame}


\mode<article>{
\item on my home PC, which is pretty good, it takes using a single thread about
6 hours per material to run all the simulations.
}

\mode<presentation>{
\begin{frame}
\frametitle{Results: Carbon/PEEK}
\begin{columns}[T]
    \begin{column}{0.45\textwidth}
        \begin{block}{Solution}
            \begin{itemize}
                \item $h_s=$ 28.8 mm
                \item $h_a=$ 19.4 mm
                \item $\phi=$   23.9
                \item $X_b=$ 2207 mm
                \item Total weight:   671 kg (-44\%)
                \item $\mathcal{D}$:  635 N/mm (+30\%)
                \item Failure index:  1
            \end{itemize}
        \end{block}
    \begin{itemize}
        \item because of interpolation, the solution should be checked 
        \textit{a posteriori}.
        \item the main problem here is $F_s$.
    \end{itemize}

    \end{column}
    \begin{column}{0.55\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width=\textwidth]
    {../../images/wrapped_barrel_optimal_carbon_mises.png}
    % wrapped_barrel_optimal_carbon_mises.png: 0x0 pixel, 300dpi, 0.00x0.00 cm,
    \caption*{Mises stress near the built-in end, pressure load case. The 
    results in the composite are shown for the top layer. Parameters correspond
    to the optimal ones.}
    \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item nothing
}


\mode<presentation>{
\begin{frame}
\frametitle{Results: Sic/Ti}
\begin{columns}[T]
    \begin{column}{0.45\textwidth}
        \begin{block}{Solution}
            \begin{itemize}
                \item $h_s=$ 26.7 mm
                \item $h_a=$ 20.1 mm
                \item $\phi=$   44.9
                \item $X_b=$ 2163 mm
                \item Total weight:   955 kg (-20\%)
                \item $\mathcal{D}$:  876 N/mm (+79\%)
                \item Failure index:  1
            \end{itemize}
        \end{block}
    \begin{itemize}
        \item critical failure index: interlaminar stresses
    \end{itemize}

    \end{column}
    \begin{column}{0.55\textwidth}
    \includegraphics[width=0.8\textwidth]%
    {../../images/wrapped_barrel_sic_Fi_optimal.pdf}\\
    \includegraphics[width=0.8\textwidth]%
    {../../images/wrapped_barrel_optimal_sic_sigmaz.png}\\
    {\scriptsize $\sigma_z$ mostly compressive except
     for small peeling region near the fixed end.}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item the throough the thickness normal stresses are compressive almost 
everywhere except close to the fixed end, where there it seems to be a small 
region with peel stress. In the composite region , $\sigma_z$ is constant 
thorugh the thickness.
Since we have large elements we need to perform a 
better analysis to be sure we are resolving this region right.
}