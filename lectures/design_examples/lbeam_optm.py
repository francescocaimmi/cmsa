#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    Author: Francesco Caimmi francesco dot caimmi at polimi dot it
last modified: 2016-01-11


Solves the optimization problem for the L-beam example
The optmization variables are the angles and the number of layers
The otimization function is the laminate height and the constraints is
Tsai-Hill failure criterion.
We assume a symmetric lamination sequence.
"""
from __future__ import print_function
import numpy as np
import pyOpt

#this stuff imports modules that are used to performe the needed trasformation
#of stresses and strains and to calculate the plate elastic properties
#theese needs to be defined separartely
from lib4g.composites.elastic import mat_trans_2Dstrain_rot
from lib4g.composites.elastic import elastic_compliance_2D_pstress
from lib4g.composites.lamination_theory import clt_ABD
from lib4g.composites.failure import tsai_hill

#Units: N,mm, MPa
#material data
E1,E2,v12,G12 = 140e3, 10e3, 0.03, 5.1e3
X, Y, S = 2100, 75, 90
#geometry
l1, l2 = 10e3, 1e3
B = 100
#load
P = 1000
Mxy = P*l2/B
Mx = P*l1/B
M = np.array([Mx,0.0,Mxy])
#safety factor
eta = 1.4
#cured ply thickness
cpt = 0.3
#layer properties
C = elastic_compliance_2D_pstress(E1,E2,v12,G12)
Q = np.linalg.inv(C)


def objfunc(x):
    """
    This function returns the objective function and the constraints,
    in compliance with the specifications of the package pyopt

    Parameters
    ------------
    x: array
        x[0] the number of layers L (int)
        x[1:]the angles (ints)

    Returns
    -------
    f: float
        the objective function (height)

    g: array
        a one component array containing the value of F, the Tsai-Hill failure
        index

    fail: int
        an evaluation failure index (here we always return 0)

    """

    fail = 0
    #DEFINE THE OBJECTIVE FUNCTION
    L = int(2*x[0])
    t = 15.0*x[1:x[0]+1:1]
    thetas = np.deg2rad(t)
    hs = [cpt for i in range(L/2)]
    hf = hs+hs[::-1]
    f = 2*np.sum(hs)

    #DEFINE THE CONSTRAINT FUNCTION
    #layer interface positions
    Z = np.zeros(L+1)
    Z[0] = -f/2.0
    for j in range(L):
        Z[j+1] = hf[j]+Z[j]

    A,dump,D = clt_ABD(hs,Q,t)#A,B,D matrix
    chi = np.dot(np.linalg.inv(D),M)#generalized deformations   

    #_for each layer_ lets calulate the maximum value of the Tsai-Hill index
    Flayers = np.zeros(len(thetas))

    for k,theta in enumerate(thetas):
        eps_top = Z[k+1]*chi
        eps_bot = Z[k]*chi
        #transform the strains in the PMRS
        Te = mat_trans_2Dstrain_rot(np.deg2rad(theta))
        eps_top12 = np.dot(Te,eps_top)
        eps_bot12 = np.dot(Te,eps_bot)
        #get the stresses
        sigma_top = np.dot(Q,eps_top12)
        sigma_bot = np.dot(Q,eps_bot12)
        #get Tsai-Hill index
        Ftop = tsai_hill(sigma_top,[X,Y,S])
        Fbot = tsai_hill(sigma_bot,[X,Y,S])
        Flayers[k] = np.max([Ftop,Fbot])
    g = [0]
    #calculate the value of the constraint function g(x)
    #inequality constraints are defined as g(x)<=0
    g[0] = np.max(Flayers)-1/eta

    return f, g, fail

def main():
    """
    Main program
    """
    Lmax=50#maximum number of layers allowed
    #define the optimization problem. See the pyopt package documentation
    #for the details http://www.pyopt.org/
    problem = pyOpt.Optimization('L-shaped beam', objfunc)
    #define the objective function
    problem.addObj('f')
    #define the problem variables (of integer type)
    problem.addVar('L',type='i', lower=20, upper=Lmax)
    for i in range(Lmax):
        problem.addVar('t'+str(i+1),type='i', lower=-5, upper=6)
    #add the inequality type Tsai-Hill constraint
    problem.addCon('Tsai-Hill',type='i')

    #define the solver, a particle swarm one in this case
    solver = pyOpt.pyALPSO.ALPSO()

    #set the solver options, see
    #http://www.pyopt.org/reference/optimizers.alpso.html for details

    solver.setOption('SwarmSize',130)
    solver.setOption('maxOuterIter',450)
    solver.setOption('maxInnerIter',9)
    #solve the optimization problem
    [fstr, xstr, inform] = solver(problem)

    #print the results to stdout and to a file
    print(problem.solution(0))
    np.savetxt('lbeam.optimal-ALPSO.txt',xstr)

if __name__=="__main__":
    main()
    
%kate: default-dictionary en_GB;
