%kate: default-dictionary en_GB;
\mode<presentation>{
\begin{frame}
\frametitle{Introduction}
In most cases there is no composite material without a composite structure:
manufacture of the component and material production are carried out at the 
same time: this allows fibres to be placed optimally, but means that 
manufacturing considerations are to be tightly integrated in the mechanical 
design of a composite structure.
Focusing on FRP, the manufacturing technique must be able to:
\begin{columns}
    \begin{column}{0.58\textwidth}
    \begin{itemize}
        \item provide the desired \textcolor{red}{shape}
        \item \textcolor{red}{consolidate} the matrix
        \begin{itemize}
        \item polymerisation (reactive polymers: thermosets and some 
            thermoplastics) 
        \item solidification (most thermoplastics)
        \end{itemize}
        \item control the \textcolor{red}{structure} of the material:
            \begin{itemize}
            \item fibre placement
            \item reinforcement dispersion
            \item material homogeneity
            \end{itemize}
    \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]
            {../../images/woven_roving_laminate.png}
            \caption*{section of laminate reinforced with woven glass 
            reinforcements. From~\cite{Hull1996}}.
        \end{figure}
    \end{column}
\end{columns}


\end{frame}

}

\mode<article>{
    \item In most cases there is no composite material without a composite 
    structure: manufacture of the component and material production are 
    carried out at the same time. This is especially true for advanced 
    composites, while for filled composites there is more room for the 
    production of materials which are shaped in subsequent phases.
    This is an opportunity, since it allows for optimal placement of the 
    reinforcing fibres, but also means that manufacturing  must be integrated
    very tightly in the design of a component, even more tightly than with 
    conventional materials.
    In this classes we focus on FRP, because they are much more frequent in 
    industry; techniques for metallic or ceramic matrices are rather different
    although they share some common principles.
    Since there is this coupling between the component and the material 
    manufacture, the technique adopted must be able to
    \begin{itemize}
    \item provide the desired \textcolor{red}{shape} of the component
    \item \textcolor{red}{consolidate} the matrix; this means
    \begin{itemize}
        \item allow polymerisation of the matrix for the so called reactive 
            polymers: thermosets and some thermoplastics that are polymerised 
            in-situ.
    \item otherwise, for thermoplastics which are used from the beginning as  
        completely reacted polymers, the technique should be able to make the 
        resin formable and then allow it to solidify back again; this is usually
        achieved by heating and cooling down
    \end{itemize}
    \item control the \textcolor{red}{structure} of the material, which means
        \begin{itemize}
        \item the fibre placement: for a composite material to work properly,
            the fibres should be disposed along more than one direction in 
            order to cope with complex loading states; we shall see 
            later more details on this, but usually fibres should be 
            disposed along the directions of maximum stress. The manufacturing
            process must allow one to place the fibers along some given 
            direction and grant that they remain in place during the making of 
            the structure, especially in the first phases when the material is 
            not yet consolidated.
        \item reinforcement dispersion: the reinforcement should be well 
            dispersed without fibres touching each other; they should all be 
            well covered by the matrix. This is the reason why practical fibre
            volume fraction in composites are at most 70\% but usually about
            60\% for the most demanding application
        \item material homogeneity: the material should be on average 
        relatively homogeneous, without resin rich pockets which of course
        limit material strength. In addition to the fact that non-crimped fibres
        are more resistant and efficient as to load transfer, this is why 
        unidirectional reinforcement are preferred in demanding applications, 
        although woven fabrics provide an faster and easier placement.
        \end{itemize}
    \end{itemize}
}


\mode<presentation>{
\begin{frame}
\frametitle{Common principles}
There are four basic steps involved in all composites manufacturing 
processes~\cite{Mazumdar2002}:
    \begin{itemize}
        \item fibre impregnation
        \item fibre-lay up
        \item consolidation
        \item solidification
    \end{itemize}
These steps are accomplished in different ways and at different times for a 
given manufacturing technique.
\end{frame}

}

\mode<article>{
\item The making of a high performance component for structural applications 
usually requires a highly complex placement of the fibres to provide an 
effective answer to the external loads.
These fibres must be impregnated with the resin that, in turn, must be able to 
become solid while the component shape is kept constant. 
There are therefore four basic steps involved in all composites manufacturing 
processes~\cite{Mazumdar2002}
These are 
    \begin{itemize}
        \item fibre impregnation
        \item fibre-lay up
        \item consolidation
        \item solidification
    \end{itemize}
These steps are accomplished in different ways and at different times depending
on the technique. Sometimes there is no clear distinction between the different 
stages, but the basic functions are achieved almost simultaneously. 
We will see some example in what follows.
We will now see a panorama of many different techniques that can be used to 
perform these different steps. 
Sometimes these technique are used together in definite groups to create a 
composite part. 
For instance with the name ``hand lay-up'', which should rigorously refer only 
to fibre lay-up, actually oftentimes a whole process is meant, which includes 
manual lay up of the reinforcement, manual impregnation and consolidation with 
rollers and cure --- solidification --- with vacuum bag or in autoclave. 
Anyway, these process can be combined in very different ways and there is a 
great number of different ways to produce composite structures, with different
techniques combined in a single process. 
}


\subsection{Fibre impregnation}
\mode<presentation>{\begin{frame}
\frametitle{Fibre impregnation}
Correct impregnation of the fibres by the matrix is paramount to get a 
performing composite material.
Requires:
\begin{itemize}
    \item fibre wetting by the matrix
    \item matrix flow around the fibre reinforcements (takes time!)
\end{itemize}
\begin{columns}[T]
    \begin{column}{0.6\textwidth}
        It can be done:
        \begin{itemize}
            \item \textcolor{red}{on line} (i.e. during component forming)
            \item during a previous stage $\Rightarrow$ by making a
            \textcolor{red}{pre-preg} tape
            \begin{itemize}
                \item sometimes offer better control on the impregnation process
                \item reactive polymers prepregs usually:
                    \begin{itemize}
                    \item  have a shelf-life and must be conserved at low
                    temperature
                    \item are tacky
                    \end{itemize}
                \item thermoplastic prepregs:
                    \begin{itemize}
                        \item no shelf life
                        \item no tack
                        \item can be reshaped and recycled
                    \end{itemize}
            \end{itemize}
        \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=0.8\textwidth]
            {../../images/carbon_fibre_prepreg.jpg}
            % carbon_fibre_prepreg.jpg: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
            \caption*{a carbon fibre prepreg with protective coating}
        \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item As we already mentioned, proper impregnation is paramount in order to get 
a performing composite material. 
For impregnation to take place, there are two requirements, mainly due to the 
processes taking place a the interface between the fibre and the matrix. 
The first is a thermodynamic one: the matrix must be able to wet the fibres. 
This happens if the fibre surface tension exceeds the liquid matrix surface 
tension plus the interface surface tension. Generally speaking polymeric 
matrices wet easily carbon, glass and metal fibres, while there can be some 
problems with other polymeric fibres; in this cases compatibility is ensured via 
proper sizing of the fibres by chemical coupling agents.
The second requirements is basically a time one: the matrix must be able to 
flow through the 3D reinforcement structure completely filling it.
This requires some time because the matrix penetrate by capillarity in the 
interstices between the fibre; often the application of an external pressure 
to promote the flow and an increase of temperature to lower the resin viscosity
are needed. 
To get the feeling of some values coming into play in this process: for a 
volume fraction of about 60\%, the surface area which is to be covered for a 
panel 1x1x0.02 meters is about \SI{5000}{\square\metre}.
Reactive matrices usually have very low viscosity (in the range 
\SIrange{0.1}{100}{\pascal\second}~\cite{Mazumdar2002}) and the impregnation 
is relatively easy: in many cases it can be done at room temperature without 
external pressure. 
For thermoplastic matrices it is much more complicated and requires melting of 
the polymer and high pressures.

Fibre impregnation can be done on line, i.e. 
during the forming of component immediately before or after placing the fibre 
reinforcement. 
Otherwise it can be done at an earlier stage, creating the so called 
prep-pregs, rolls of fibre reinforcement which are already impregnated with the 
matrix.
For some thermoplastic matrices, one can create yarns containing 
filaments made both with the fibres and with the matrix material, which are 
called comingled yarn. Fabrics made with both reinforcing and matrix yarns are 
also used.
These expedients lower the flow length needed to impregnate the fibers, thus 
easing processing.

Since they are tacky, thermoset prepregs are usually protected with a layer of 
something with a very low surface tension in order to avoid self-adhesion of 
the material when it is rolled up. 
}

\subsection{Fibre lay-up}
\mode<presentation>{\begin{frame}
\frametitle{Fibre lay-up}
Composite laminates are formed by placing fibres or prepregs at the desired 
angles and at places where they are needed. The required thickness is obtained 
by stacking more layers.
In the initial stages, before solidification, stability of the lay-up must be 
granted (flow, gravity).
\begin{columns}[T]
    \begin{column}{0.4\textwidth}
        \begin{itemize}
            \item process can be automatic or manual
            \item placement on a surface\ldots{}
            \begin{itemize}
                \item tape laying on a mould is the inverse of milling
                \item tape winding on a mandrel is the inverse of turning
            \end{itemize}
            \item \ldots{} or in 3D using pre-forms
        \end{itemize}
    \end{column}
    \begin{column}{0.3\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{../../images/pre-preg-layup.jpg}
    % pre-preg-layup.jpg: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \caption*{Manual lay-up of a prepreg on a flat mould.}
    \end{figure}
    \end{column}
    \begin{column}{0.3\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../../images/T-profile-3Dpreform.jpg}
    % pre-preg-layup.jpg: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \caption*{A 3D preform for T-beams}
    \end{figure}
    \end{column}
\end{columns}
\vspace{0.5ex}
Composites: made by additive manufacturing well before it was cool
\end{frame}
}

\mode<article>{
\item Stability of the lay-up must be assured: the layers must be able to 
stay 
in place without significant displacement of the fibres bundles under the 
actions of the loads coming from the self-weight of the laminate and those due 
to flow of the matrix.
}

\mode<presentation>{\begin{frame}
\frametitle{Hand lay-up}
It's the simplest and possibly the most widely used lay-up technique
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item reinforcements are cut to proper size and manually placed 
            in an open mould
            \item if needed resin is added and compacted with a roll
            \item developable mould surfaces are better!
            \item good surface finishing can be achieved only on the mould side
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{center}
        \includegraphics[width=\textwidth]{../../images/handlayup.png}
        % handlayup.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \end{center}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item 
The addition of resin may be done now or later: for instance in some 
processes,such as  RTM which we are going to see later, impregnation is done at 
a later stage.
This is a general rule for composite manufacturing. 
Since most composite reinforcements are actually planar, it is much easier to 
lay them up if the surface upon which they are laid is a developable surface, 
i.e. a surface with zero gaussian curvature that can be obtained by folding a 
plan.
Draping a non developable surface is a much more complicated job: to adapt to 
the surface in fact wrinkles or changes in the orientation of the fibres are 
required; these are generally difficult to predict and requires the use of 
simulation software.
Good surface finishing can be achieved only on the mould side, unless after the 
lay up a male mould is also used, which actually may happens in some processes 
such as RTM
}

\mode<presentation>{\begin{frame}
\frametitle{Filament winding}
\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]
    {../../images/fiilament_winding_scheme.png}
    % fiilament_winding_scheme.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \caption*{a very simple two axis filament winding machine with a bath for 
    resin impregnation}
\end{figure}
\begin{itemize}
    \item fibres are deposed automatically on an axisymmetric mandrel rotating
        around its axis
    \item there is at least a second linear axis moving a carriage. The ratio 
        $\omega/v$ controls the angle $\alpha$ that can vary in the range
        \SIrange{5}{90}{\degree}~\cite{barbero2011introduction}
    \item the simplest mandrels are cylindrical, but more complicated mandrels 
    are possible
\end{itemize}

\end{frame}
}


\mode<article>{
\item  
Several back-and-forth travels of the carriage are needed to complete a 
single lamina.
You cannot lay fibres along the symmetry axis.
You can lay fibres with angles in the 
range \SIrange{5}{90}{\degree}~\cite{barbero2011introduction}, but angles lower 
than \SI{15}{\degree} are very difficult~\cite{Mazumdar2002}.
\textcolor{red}{manda il video}
}

\mode<presentation>{\begin{frame}
\frametitle{}
\begin{columns}[T]
    \begin{column}{0.6\textwidth}
        \begin{itemize}
            \item machines with more axes can work faster and make more complex 
                shapes
            \item broadly speaking, the fibres tend to follow 
                \textcolor{red}{geodesic} paths: e.g. on cylinder they are 
                helices, circles and lines parallel to the axis
            \item to draw  fibres along paths which are not geodesic, path 
                stability must be assured, mostly by friction: anyway 
                limited by the processing 
                conditions~\cite{barbero2011introduction}
            \item mandrels:
            \begin{itemize}
                \item sacrificial/liner
                \item collapsible/inflatable
                \item soluble
            \end{itemize}
            \item can be used with prepregs: placement and consolidation in one 
                single step
            \item can make cost-effective, high performance composite parts 
                (max volume fraction about 60\%)
        \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
        \begin{figure}
        \centering
        \includegraphics[width=\textwidth]
        {../../images/gedesic_winding_vessel.png}
        \caption*{Geodesic winding of a pressure vessel.}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}
}

\mode<article>{
\item There are various type of mandrels that can be used in this process. A 
common solution is to use a sacrificial steel mandrel that can be left inside 
the component; sometimes, when a liner is needed such as in some pressure 
vessels, this is done intentionally. Otherwise one can use an inflatable 
mandrel or a collapsible one, made of many pieces that can be disassembled; 
these generally are expensive and they use requires more operations, thus 
reducing the process throughput~\cite{barbero2011introduction}.
There are also soluble mandrels made with sand and poly-vinyl-alcohol, that can 
be washed away with water after solidification. 
Release agents may be required on the mandrel if it is to be removed.
The technique can also be used with prepregs, both thermoset and thermoplastic: 
of course thermoplastic prepregs require a more complex head able to heat the 
material. 
Using pre-pregs: placement and consolidation take place at the same 
time, exploiting the tension applied by the machine. Thermoplastic prepregs
usually require a method to apply heat in order to soften the material and make 
it take the desired shape. Similarly, ultraviolet curing can be implemented by 
adding a UV source on the head.
For very thick laminates it may be necessary to stop the process and allow 
partial curing (up to gelation) to avoid overheating 
problems~\cite{barbero2011introduction,Mazumdar2002}.
It can be used to get cost-effective, high performance composite parts 
although the maximum achievable fibre volume fraction is about 
60\%~\cite{Mazumdar2002}.
The surface finish of the outer surface cannot be controlled, unless after 
laying up the mandrel is placed in a mould, something which is sometimes done
if autoclave or mould curing is to follow deposition. Otherwise, to control the 
external finishing additional machining is needed.
}

\mode<presentation>{\begin{frame}
\frametitle{Automated tape laying and fibre placement}
Automated tape laying (ATL) and fibre placement 
(AFP)~\cite{Lukaszewicz2012997} are the automated version of manual lay-up, with
tape cutting incorporated in the process.
\begin{itemize}
    \item ATL uses large tapes (high throughput, less versatility), while AFP 
        simultaneously lays a set of much smaller tapes (more complex head, 
        more versatile)
    \item high tooling cost: usually highly demanding applications
\end{itemize}
\begin{columns}[b]
\begin{column}{0.33\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{../../images/ATL_vertical.png}
        % ATL_vertical.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
        \caption*{ATL machine laying tape on a vertical tool. 
        From~\cite{Lukaszewicz2012997}.}
    \end{figure}
\end{column}
\begin{column}{0.33\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{../../images/AFP_steer.png}
        % ATL_vertical.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
        \caption*{AFP laying steered tows. 
                From~\cite{Lukaszewicz2012997}.}
    \end{figure}
\end{column}
\begin{column}{0.33\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../../images/AFP_defects.png}
    % ATL_vertical.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \caption*{Typical lap and gaps problem with AFP. 
            From~\cite{Lukaszewicz2012997}.}
    \end{figure}
\end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item \textcolor{red}{manda il video} 
These techniques have exceptionally high tooling cost and therefore are 
used mostly for very demanding applications in the aerospace industry, both 
with thermoplastic and thermoset prepregs. 
They can significantly reduce trim waste with respect to manual lay-up and can 
achieve very high production rates
}


\subsection{Consolidation}
\mode<presentation>{\begin{frame}
\frametitle{Consolidation}
Provides intimate contact between each lamina~\cite{Mazumdar2002} (autohesion)
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item requires pressure to be applied
            \item poor consolidation $\Rightarrow$ parts with voids and/or dry 
                spots
            \item grants that air entrapped between layers, solvents and excess 
                resin are removed
            \item involves resin flow and elastic fibre deformation
            \item may take place almost immediately after lay-up or in a 
                subsequent phase
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \begin{center}
            \includegraphics[width=\textwidth]
            {../../images/thermoplastic_consolidation.png}
            \caption*{Thermoplastic prepreg consolidation process immediately
                      after lay-up. From~\cite{Advani2003}.}
        \end{center}
    \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item Consolidation is the processing step that brings very close the different
layers, ideally allowing the creation of monolithic material. It actually 
often happens that between the different laminae a resin rich layer remains, 
the laminae retaining some of their individuality.
}
\subsection{Solidification}
\mode<presentation>{\begin{frame}
\frametitle{Solidification}
Involves curing for reactive matrices or cool-down for thermoplastics.
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item it may take less than a minute for some thermoplastics or up 
                to some hours for some thermosets
            \item for thermosets often heat must be supplied 
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    \begin{figure}
        \begin{center}
            \includegraphics[width=\textwidth]
            {../../images/ttt-diagram.png}
            \caption*{Typical TTT diagram for a thermoset polymer. 
                After~\cite{enns1983time}.}
        \end{center}
    \end{figure}
    \end{column}
\end{columns}
\end{frame}
}

\mode<article>{
\item solidification. The time needed for the process to complete can be 
estimated using the so called TTT (time - temperature -transformation) 
graphs,which show the physical state of a polymer during isothermal curing at 
varying temperatures. Here for instance we have such a graph for an epoxy resin 
showing what happens after 
}